<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newsType extends Model
{
	protected $table = 'news_type';
	protected $primaryKey = 'news_type_id';

	protected $fillable = [
	'news_type_description',
	];
	public $timestamps = false;

}
