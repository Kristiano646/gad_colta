<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class senso1 extends Model
{
	protected $table = 'infocenso';
	protected $primaryKey = 'infocenso_id';

	protected $fillable = [
	'infocenso_id_Tipos',
	'infocenso_description',
	];

	const CREATED_AT = 'infocenso_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'infocenso_update';

    protected $date =[
    'infocenso_create',
    'infocenso_update',
    ];

}
