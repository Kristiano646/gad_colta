<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyectosType extends Model
{
	protected $table = 'proyectos_type';
	protected $primaryKey = 'proyectos_type_id';

	protected $fillable = [
	'proyectos_type_description',
	];
	public $timestamps = false;

}
