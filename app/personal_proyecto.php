<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personal_proyecto extends Model
{
	protected $table = 'personal_proyecto';
	protected $primaryKey = 'personal_proyecto_id';

	protected $fillable = [
        'personal_id',
        'proyecto_id',
      
	];

	const CREATED_AT = 'personal_proyecto_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'personal_proyecto_update';

    protected $date =[
    'personal_proyecto_create',
    'personal_proyecto_update',
    ];
    public $timestamps = false;

}
