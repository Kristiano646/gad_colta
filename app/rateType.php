<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rateType extends Model
{
    protected $table = 'type_rate';
	protected $primaryKey = 'type_rate_id';

	protected $fillable = [
	'type_rate_description',
	];
	public $timestamps = false;
}
