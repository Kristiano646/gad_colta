<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comunidad_proyecto extends Model
{
	protected $table = 'comunidad_proyecto';
	protected $primaryKey = 'comunidad_proyecto_id';

	protected $fillable = [
        'comunidad_id',
        'proyecto_id',
      
	];
	public $timestamps = false;


}
