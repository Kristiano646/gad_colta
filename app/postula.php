<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class postula extends Model
{
    protected $table = 'postulant';
	protected $primaryKey = 'postulant_id';

	protected $fillable = [
    'postulant_document_number',
    'postulant_genre',
    'postulant_id_document',
    'postulant_last_name',
    'postulant_name',
    'postulant_mail',
    'postulant_nationality',
    'postulant_phone',
    'postulant_program',
	];
	public $timestamps = false;
}
