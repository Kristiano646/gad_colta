<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class authorityEstado extends Model
{
	protected $table = 'user_estado';
	protected $primaryKey = 'user_estado_id';

	protected $fillable = [
	'user_estado_description',
	];

	public $timestamps = false;

}
