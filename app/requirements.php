<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class requirements extends Model
{
    protected $table = 'requirements_program';
	protected $primaryKey = 'requirements_program_id';

	protected $fillable = [
    'requirements_program_description',
    'requirements_program_program',
	];
	public $timestamps = false;
}
