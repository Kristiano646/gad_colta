<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class miembro extends Model
{
	protected $table = 'miembro';
	protected $primaryKey = 'miembro_id';

	protected $fillable = [
        'miembro_directiva_id',
        'miembro_nombres',
        'miembro_cargo',
        'miembro_sexo',
        'miembro_directiva_fechaini',
        'miembro_directiva_fechafin',
	];
	const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';

    protected $date =[
    'updated_at',
    'created_at',
    ];


}
