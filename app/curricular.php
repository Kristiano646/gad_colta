<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class curricular extends Model
{
    protected $table = 'curriculum_program';
	protected $primaryKey = 'curriculum_program_id';

	protected $fillable = [
    'curriculum_program_description',
    'curriculum_program_module',
	];
	public $timestamps = false;
}
