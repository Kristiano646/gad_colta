<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tiposgarantia extends Model
{
    
	protected $table = 'tipos_garantia';
	protected $primaryKey = 'tipos_garantia_id';

	protected $fillable = [
    'tipos_garantia_description',
];
}