<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class programType extends Model
{
    protected $table = 'type_program';
	protected $primaryKey = 'type_program_id';

	protected $fillable = [
	'type_program_description',
	];

	public $timestamps = false;
}
