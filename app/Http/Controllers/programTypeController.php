<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class programTypeController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\programType::All();
		return view ('admin.programType')
		->withManagement($managementArea)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'programDescription' => 'required|max:100|unique:type_program,type_program_description',

			]);

		\App\programType::create([
			'type_program_description' => $request['programDescription'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\programType::find($request['categoryId']);

		$this->validate($request,[

			'programDescription' => 'required|max:100|unique:type_program,type_program_description,'.$category->type_program_id.',type_program_id',

			]);
		$category->type_program_description   = $request['programDescription'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}
}
