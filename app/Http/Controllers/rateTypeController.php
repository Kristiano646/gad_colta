<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class rateTypeController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\rateType::All();
		return view ('admin.rateType')
		->withManagement($managementArea)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'rateTypeDescription' => 'required|max:100|unique:type_rate,type_rate_description',

			]);

		\App\rateType::create([
			'type_rate_description' => $request['rateTypeDescription'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\rateType::find($request['categoryId']);

		$this->validate($request,[

			'rateTypeDescription' => 'required|max:100|unique:type_rate,type_rate_description,'.$category->type_rate_id.',type_rate_id',

			]);
		$category->type_rate_description   = $request['rateTypeDescription'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}
}
