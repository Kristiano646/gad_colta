<?php

namespace App\Http\Controllers;

use Auth;
use App\news;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class newsController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$userId = Auth::user()->user_id;
		$userType = Auth::user()->user_type;
		if ($userId!=1){
			if($userType==4){
				$newsTable = news::where('news_user',$userId)->orderBy('news_update', 'desc')->get();
			}else{
				$newsTable = news::orderBy('news_update', 'desc')->get();
			}
		}else{
			$newsTable = news::orderBy('news_update', 'desc')->get();
		}
		if($userType!=4){
			$newsTypeTable = \App\newsType::All();
		}else {
			$newsTypeTable = \App\newsType::where('news_type_id',3)->get();
		}
		$multimediaTable = \App\multimediaType::All();
		return view ('admin.news')
		->withManagement($managementArea)
		->withNews($newsTable)
		->withMultimedia($multimediaTable)
		->withTypes($newsTypeTable);
	}

	public function showData($newsId)
	{
		$managementArea = \App\managementArea::firstOrFail();
		$news =  \App\news::find($newsId);
		$multimedia = \App\multimedia::where('multimedia_news',$newsId)
		->orderBy('multimedia_type', 'asc')
		->orderBy('multimedia_create','desc')
		->get();

		return view ('admin.newsData')
		->withManagement($managementArea)
		->withNews($news)
		->withMultimedia($multimedia);
	}

	public function store(Request $request)
	{
		
		$this->validate($request,[

			'newsTitle' => 'required|unique:news,news_title|max:100',
			//'newsAlias' => 'unique:news,news_alias|max:100',
			'newsDescription' => 'required',
			//'newsType' => 'required',
			]);
        $time = time();
		$news = new news();
		/*Controlar segun tipo de usuario*/
		$userId = Auth::user()->user_id;
		$userType = Auth::user()->user_type;
		if($userType==4){
			$news->news_user=$userId;
			$news->news_state = 0;
		}else {
			$news->news_state = 1;
		}
		$news->news_title = ucwords($request['newsTitle']);
		$news->news_content = $request['newsDescription'];
		$news->news_type = $request['newsType'];
		$news->news_management_area = 1;
		$news->news_user = Auth::user()->user_id;
		
    	$news->news_alias = "noticia-gadcolta-".date("Y-m-d-H-i-s", $time);

		if ($request->hasFile('newsPhoto')) {
			$imagen = $news->news_photo;
			$news->news_photo= $request->file('newsPhoto')->getClientOriginalName();
			$path = 'img/noticias';
			$file = $request->file('newsPhoto');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			\File::delete($path . '/' . $imagen);
		}

		$news->save();
		unset($request);
		unset($news);

		return back()->withMensaje('Operación Exitosa');

	}

	public function delete (Request $request){
		try{
			$news = news::find($request['newsId']);
			$news->news_state = 0;
			$imagen = $news->user_photo;
			$path = 'img/noticias';
			$news->delete();
			\File::delete($path . '/' . $imagen);
			unset($request);
			unset($news);
			return back()->withMensaje('Operación Exitosa');

		}catch(Exception $e){
			return back()->withMensaje('Error en la operación');

		}

	}

	public function update (Request $request){

		$news = news::find($request['newsId']);

		$this->validate($request,[

			'newsTitle' => 'required|max:100'.$news->news_id.',news_id',
			//'newsAlias'=> 'required|max:100|unique:news,news_alias,'.$news->news_id.',news_id',
			'newsDescription' => 'required',
			'newsDate' => 'date|date_format:Y-m-d',

			]);
		$news->news_title = ucwords($request['newsTitle']);
		$news->news_content = $request['newsDescription'];
		
		if (isset($request->newsState)){
			$news->news_state = $request['newsState'];
		}
		$news->news_create = $request['newsDate'];
		
		
		if ($request->hasFile('newsPhoto')) {
			$imagen = $news->news_photo;
			$news->news_photo= $request->file('newsPhoto')->getClientOriginalName();
			$path = 'img/noticias';
			$file = $request->file('newsPhoto');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			\File::delete($path . '/' . $imagen);
		}
		$news->save();
		unset($request);
		unset($news);
		return back()->withMensaje('Operación Exitosa');

	}


	public function addResource(Request $request){

		$newsId = $request->newsId;

		$this->validate($request,[

			'multimediaType' => 'required',
			'foto.*'=>'mimes:jpg,jpeg,png|image',
			'archivo.*'=>'mimes:pdf,docx|file',
			'enlace.*'=>'url',
			]);

		if (isset($request->foto)) {

			foreach ( $request->foto as $fotografia) {

				\App\multimedia::create([
					'multimedia_news'=> $newsId,
					'multimedia_name'=> str_replace(" ","_",strtolower ($fotografia->getClientOriginalName())),
					'multimedia_type'=> 1,
					]);

				$path = 'img/noticias';
				$filename = str_replace(" ","_",strtolower ($fotografia->getClientOriginalName()));
				$fotografia->move($path, $filename);
			}
		}

		if (isset($request->enlace)) {
			foreach ( $request->enlace as $enlace) {
				\App\multimedia::create(
					[
					'multimedia_news'=> $newsId,
					'multimedia_name'=> $enlace,
					'multimedia_url' => $enlace,
					'multimedia_type'=> 3,
					]
					);
			}
		}

		if (isset($request->archivo)) {
			foreach ( $request->archivo as $file) {

				\App\multimedia::create(
					[
					'multimedia_news'=> $newsId,
					'multimedia_name'=> $file->getClientOriginalName(),
					'multimedia_type'=> 2,
					]
					);

				$path = 'docs/noticias';
				$filename = $file->getClientOriginalName();
				$file->move($path, $filename);
			}
		}
		unset($request);
		return redirect('admin/newsData/'.$newsId);


	}

	public function deleteResource(Request $request){

		$news = \App\news::find($request->newsId);
		if ($request->hasFile('newsPhoto')) {
			$imagen = $news->news_photo;
			$news->news_photo= $request->file('newsPhoto')->getClientOriginalName();
			$path = 'img/noticias';
			$file = $request->file('newsPhoto');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			\File::delete($path . '/' . $imagen);
		}

		$news->delete();
		unset($newsData);
		unset($request);
		return back();
	}

}
