<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class sliderController extends Controller
{
    public function show()
    {
        $managementArea = \App\managementArea::firstOrFail();
        $userId = Auth::user()->user_id;
        if ($userId != 1) {
            $sliderTable = \App\slider::all(); 
        } else {
            $sliderTable = \App\slider::orderBy('slider_update', 'desc')->get();
        }

        return view('admin.slider')
            ->withManagement($managementArea)
            ->withSocial($sliderTable);
    }

    public function showData($sliderId)
    {

        $managementArea = \App\managementArea::firstOrFail();
        $slider =  \App\slider::find($sliderId);

        return view('admin.sliderData')
            ->withManagement($managementArea)
            ->withSocial($slider);
    }

    public function store(Request $request)
    {

        $this->validate($request, [

            'sliderName' => 'required|max:100|',
            'sliderImage' => 'required|mimes:svg,jpg,jpeg,png|max:10000',

        ]);

        $image = $request->file('sliderImage');
        
        \App\slider::create([
            'slider_name' => $request['sliderName'],
            'slider_description' => $request['sliderDescription'],
            'slider_image' => $image->getClientOriginalName(),
        ]);

        $path ='img/slider';
        $filename = $image->getClientOriginalName();
        $image->move($path, $filename);
        unset($request);
        return back()->withMensaje('Operación Exitosa');
    }

    public function delete(Request $request)
    {

        $slider = \App\slider::find($request['sliderId']);
        $imagen=$slider->slider_image;
        $path ='img/slider';
        //dd($imagen,$path);
        $slider->delete();
        \File::delete($path . '/' . $imagen);
        return back()->withMensaje('Operación Exitosa');
    }

    public function update(Request $request)
    {

        $slider = \App\slider::find($request['sliderId']);


        $this->validate($request, [

            'sliderName' => 'required|max:100|',
            'sliderImage' => 'mimes:jpeg,png,svg|image|max:10000',

        ]);

        $slider->slider_name   = $request['sliderName'];
        $slider->slider_description   = $request['sliderDescription'];
        if ($request->hasFile('sliderImage')) {
            $imagen = $slider->slider_image;
            $slider->slider_image = $request->file('sliderImage')->getClientOriginalName();
            $path ='img/slider';
            $file = $request->file('sliderImage');
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);
            \File::delete($path . '/' . $imagen);
        }
      
        $slider->save();
        unset($request);
        unset($slider);
        return back()->withMensaje('Operación Exitosa');
    }
}
