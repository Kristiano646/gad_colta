<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authorityTypeController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\authorityType::All();
		$authorityCargo = \App\cargo::All();
		$authorityEstado =\App\authorityEstado::All(); 
		return view ('admin.authorityType')
		->withManagement($managementArea)
		->withCargo($authorityCargo)
		->withEstado($authorityEstado)
		->withCategory($category);
	}

	public function store(Request $request)
	{

		

		\App\cargo::create([
			'user_cargo_description' =>ucwords($request['authorityDescription']),
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');

	}

	public function update (Request $request){

		
		$authorityCargo = \App\cargo::find($request['categoryId']);
		

		$authorityCargo->user_cargo_description = ucwords($request['authorityDescription']);
		$authorityCargo->save();	
		unset($request);
		unset($authorityCargo);

		return back()->withMensaje('Operación Exitosa');

	}
	public function delete (Request $request){
		try{
			$category=\App\cargo::find($request['categoryId']);
			$category->delete();
			unset($request);
			unset($news);
			return back()->withMensaje('Operación Exitosa');

		}catch(Exception $e){
			return back()->withMensaje('Error en la operación');

		}

	}

}
