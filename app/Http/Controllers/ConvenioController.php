<?php

namespace App\Http\Controllers;

use Auth;
use App\news;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ConvenioController extends Controller
{
    public function show()

    {
        $tipgarantia = \App\tiposgarantia::All();
        $managementArea = \App\managementArea::firstOrFail();
        $encargado = \App\encargados::All();
        $persproy = \App\personal_proyecto::All();
        return view('admin.garantiaType')
            ->withPerproy($persproy)
            ->withEncarg($encargado)
            ->withManagement($managementArea)
            ->withTipog($tipgarantia);
    }


    public function addgarantia(Request $request)

    {
        \App\tiposgarantia::create([
            'tipos_garantia_description' => $request['TypeDescription'],
        ]);
        unset($request);

        return back()->withMensaje('Operación Exitosa');
    }

    public function uptgarantia(Request $request)

    {
        $category = \App\tiposgarantia::find($request['garantiaId']);

        $category->tipos_garantia_description = $request['TypeDescription'];
        $category->save();
        unset($request);
        unset($category);

        return back()->withMensaje('Operación Exitosa');
    }
    public function dltgarantia(Request $request)

    {
        try {
            $category = \App\tiposgarantia::find($request['garantiaId']);
            $category->delete();
            unset($request);
            return back()->withMensaje('Operación Exitosa');
        } catch (Exception $e) {
            return back()->withMensaje('Error en la operación');
        }
    }

    public function showConvenio(Request $request)

    {
        $convenios = \App\convenio::All();
        $financiamiento = \App\financiamiento_proyecto::All();


        foreach ($financiamiento as $dataFi);
        $monto = $dataFi->financiamiento_id;

        $costo = DB::table('presupuesto')
            ->leftJoin('financiamiento_proyecto', 'financiamiento_id', '=', 'presupuesto_id')
            ->where('financiamiento_proyecto.proyecto_id',$request['ProId'])
            ->get();

        if (isset($convenios)) {
            $conv = 0;
            $v2 = 0;
        } else {
            foreach ($costo as $cst);
            foreach ($convenios as $conv);
            $v1 = $cst->presupuesto_monto;
            $valaux = $conv->convenios_sal;
            $v2 = $v1 - $valaux;
        }
        $idpro = $request['ProId'];
        $proy = DB::table('proyectos')
            ->leftJoin('convenios', 'convenios_id_proyecto', '=', 'proyectos.proyectos_id')
            ->where('proyectos.proyectos_id', $request['ProId'])
            ->get();

        $dataproy = DB::table('proyectos')
            ->select('proyectos.proyectos_id', 'proyectos.proyectos_name', 'proyectos.proyectos_objetivo')
            ->where('proyectos.proyectos_id', $request['ProId'])
            ->get();
        $datapers = DB::table('personal_proyecto')
            ->select('personal_proyecto.personal_proyecto_id', 'personal_proyecto.personal_id', 'personal_proyecto.proyecto_id')
            ->where('personal_proyecto.proyecto_id', $request['ProId'])
            ->get();

        $encargado = \App\encargados::All();
        $persproy = \App\personal_proyecto::All();

        $tipgarantia = \App\tiposgarantia::All();
        $managementArea = \App\managementArea::firstOrFail();
        $proyectos = \App\proyectos::All();
        $encproy = DB::table('personal_proyecto')
            ->leftJoin('personal', 'personal.personals_id', '=', 'personal_proyecto.personal_id')
            ->where('personal_proyecto.proyecto_id', $request['ProId'])
            ->get();

        return view('admin.convenio')
            ->withDataper($datapers)
            ->withData($dataproy)
            ->withSaldo($v2)
            ->withPerproy($persproy)
            ->withCosto($costo)
            ->withEncarg($encargado)
            ->withManagement($managementArea)
            ->withProye($proy)
            ->withEncpro($encproy)
            ->withProyec($proyectos)
            ->withTipog($tipgarantia)
            ->withConvenios($convenios);
    }

    public function addConvenio(Request $request)

    {       
        $costo = DB::table('presupuesto')
        ->leftJoin('financiamiento_proyecto', 'financiamiento_id', '=',  'presupuesto.presupuesto_id')
        ->where('financiamiento_proyecto.proyecto_id',$request['ProId'])
        ->get();
        
        foreach($costo as $cst)
        $auxcos=$cst->presupuesto_monto;
        $auxcos1=0;
        $sal=$request['saldo'];
        if(!isset($sal)){
            $auxcos1=$auxcos;
            $val1=$request['aporteProde'];
            $val2=$request['aporteCom'];
            
        $saldo=$auxcos1-$val1-$val2;
              
        }else {
            $saldo=$request['saldo'];
        }
        
        \App\convenio::create([
            'convenios_id_proyecto' => $request['ProId'],
            'convenios_fech_ini' => $request['fechaini'],
            'convenios_fech_final' => $request['fechafin'],
            'convenios_aporte_municipio' => $request['aporteProde'],
            'convenios_aporte_comunidad' => $request['aporteCom'],
            'convenios_sal' => $saldo,
            'convenios_id_encargado' => $request['PersId'],
            'convenios_management_area' => '1',
            'convenios_create' => $request['finia'],
            'convenios_update' => $request['finia'],

        ]);
       
        \App\desembolsos::create([
         
            'fecha_desembolsos' => $request['finia'],
            'monto_desembolsos'=> $request['desemb'],
            'acumulado_desembolsos'=>$request['acum'],

        ]);

        $idd=DB::table('desembolsos')
        ->where('fecha_desembolsos','=',$request['finia'])
        ->get();
        $idc=DB::table('convenios')
        ->where('convenios_create','=',$request['finia'])
        ->get();
        foreach ($idd as $ids)
        
        $idss=$ids->id_desembolsos;
        foreach ($idc as $iddc)
        $idcs=$iddc->convenios_id;
        

        \App\convenio_desembolsos::create([
            'id_desembolso' => $idss,
            'id_convenios'=> $idcs,
           
        ]);

        \App\garantia::create([
            'garantia_monto' => $request['monto'],
            'garantia_id_tipo' => $request['tipgarantia'],
            'garantia_fechven' => $request['fechven'],
            'garantia_observacion' => $request['observ'],
            'garantia_proyecto_id' => $request['ProId'],
        ]);
        unset($request);

        return back()->withMensaje('Operación Exitosa');
    }
}
