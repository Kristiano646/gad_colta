<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class requirementsController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\requirements::All();
		$curricularModule= \App\program::All();
		return view ('admin.requirements')
		->withManagement($managementArea)
		->withOption($curricularModule)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'requirementTitle' => 'required|max:100|:requirements_program,requirements_program_description',
			'requirementProgram' => 'required:requirements_program,requirements_program_program',
		]);

		\App\requirements::create([
			'requirements_program_description' => $request['requirementTitle'],
			'requirements_program_program' => $request['requirementProgram'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\requirements::find($request['categoryId']);

	/*	$this->validate($request,[

			'coordinatorName' => 'required|max:100|unique:coordinator_program,coordinator_program_name,'.$category->coordinator_program_id.',coordinator_program_id',
			'coordinatorLastName' => 'required|max:100|unique:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|unique:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);*/
		$category->requirements_program_description = $request['requirementTitle'];
		$category->requirements_program_program = $request['requirementProgram'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa, coordinador ingresado con éxito.');

	}}