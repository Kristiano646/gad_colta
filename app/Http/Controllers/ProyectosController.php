<?php

namespace App\Http\Controllers;

use Auth;
use App\news;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;


class ProyectosController extends Controller
{
	public function showProyectos()

	{
		$beneficiados = DB::table('comunidad_proyecto')->get();
		$countbeneficiados = DB::table('comunidad_proyecto')->count();

		$encargados = \App\encargados::All();

		$managementArea = \App\managementArea::firstOrFail();
		$proyectos = \App\proyectos::All();
		$typeproy = \App\proyectosType::All();

		$avancenum = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->count();

		$proy = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->get();

		$presu = DB::table('proyectos')
			->leftJoin('financiamiento_proyecto', 'proyecto_id', '=', 'proyectos.proyectos_id')
			->leftJoin('presupuesto', 'presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->get();

		$comu = DB::table('comunidad')
			->get();

		foreach ($proyectos as $pro)

			if ($avancenum <= $pro->proyectos_numInf) {

				$porcavance = ($pro->proyectos_numInf);
				$percent = $avancenum * 100 / $porcavance;
				$percent = intval($percent);
			} else {
				$percent = 100;
				$percent = intval($percent);
			}
		if ($avancenum == 0) {
			$percent = 0;
		}

		foreach ($proyectos as $pro)
			$date1 = new DateTime($pro->proyectos_fechaini);
		$date2 = new DateTime($pro->proyectos_fechafin);
		$difference = $date1->diff($date2);
		$diffInMonths  = $difference->m;
		$avance = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->get();

		$presupuesto = DB::table('proyectos')
			->leftJoin('financiamiento_proyecto', 'proyecto_id', '=', 'proyectos.proyectos_id')
			->leftJoin('presupuesto', 'presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->get();



		$encproy = DB::table('personal')
			->leftJoin('personal_proyecto', 'personal_id', '=', 'personal.personals_id')
			->leftJoin('proyectos', 'proyectos_id', '=', 'personal_proyecto.proyecto_id')
			->get();

		if (isset($countbeneficiados)) {
			$valbeneficiado = 0;
		} else {
			$valbeneficiado = 1;
		}
		// esto para fechas entre Reservation::whereBetween('reservation_from', [$from, $to])->get();
		return view('admin.proyectos')
			->withComu($comu)
			->withMonths($diffInMonths)
			->withAvance($avance)
			->withPres($presu)
			->withPresupuesto($presupuesto)
			->withProy($proy)
			->withEncargados($encargados)
			->withAvncen($avancenum)
			->withEncpro($encproy)
			->withPorcentaje($percent)
			->withManagement($managementArea)
			->withBeneficiados($beneficiados)
			->withCountben($valbeneficiado)
			->withProyec($proyectos)
			->withTypepro($typeproy);
	}


	public function addProyectos(Request $request)
	{


		try {
		$variable=\App\proyectos::create([

				'proyectos_name' =>  ucwords($request['NombrePro']),
				'proyectos_fechaini' => $request['fechaini'],
				'proyectos_fechafin' => $request['fechafin'],
				'proyectos_objetivo' => ucwords(($request['ObjetivoPro'])),
				'proyectos_nmeses' => $request['valueMeses'],
				'proyectos_numInf' => $request['numinf'],
				'proyectos_Cumrequisitos' => ucwords($request['chkcumple']),
				'proyectos_estado' => ucwords($request['estadoreq']),
				'type_id'=>ucwords($request['typepro']),

			]);

			\App\presupuesto::create([

				'presupuesto_monto' => $request['montodinero'],
				'presupuesto_acumulado' => $request['acumdinero'],
				'presupuesto_observacion' => ucwords($request['observaciondinero']),
				'presupuesto_estado' => ucwords($request['estadodinero']),
			]);


			$aux1 = DB::table('proyectos')
				->where('proyectos.proyectos_name', '=', ucwords($request['NombrePro']))
				->get();
			$aux2 = DB::table('presupuesto')
				->where('presupuesto.presupuesto_observacion', '=', ucwords($request['observaciondinero']))
				->get();
			foreach ($aux1 as $ax)
				foreach ($aux2 as $ax2)
					$idpro = $ax->proyectos_id;
			$idpre = $ax2->presupuesto_id;
			$comunidadesafec = $request['seleccom'];
			foreach ($comunidadesafec as $comaf) {
				\App\comunidad_proyecto::create([

					'comunidad_id' => $comaf,
					'proyecto_id' => $variable->proyectos_id,

				]);
			}
			\App\financiamiento_proyecto::create([
				'financiamiento_id' => $idpre,
				'proyecto_id' => $idpro,
			]);

			\App\personal_proyecto::create([
				'personal_id' => $request['idencargados'],
				'proyecto_id' => $idpro,
			]);



			unset($request);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}


	public function deleteProyectos(Request $request)
	{
		try {
			$var = $request['ProId'];
			$avance = DB::table('avance')->where('avance.avance_proyecto_id', $var);
			$comunidad_proyecto = DB::table('comunidad_proyecto')->where('comunidad_proyecto.proyecto_id', $var);
			$convenios = DB::table('convenios')->where('convenios.convenios_id_proyecto',  $var);
			$proyecto=DB::table('proyectos')->where('proyectos.proyectos_id',  $var);
			$comunidad_proyecto = DB::table('comunidad_proyecto')->where('comunidad_proyecto.proyecto_id', $var);
			$encproy = DB::table('personal_proyecto')->where('personal_proyecto.proyecto_id', $var);
			$auxcon=DB::table('convenios')->where('convenios.convenios_id_proyecto',  $var)->get();
			$convenios_desembolso=DB::table('convenios_desembolso')->where('convenios_desembolso.id_convenios',$auxcon);
			$avance->delete();
			$comunidad_proyecto->delete();
			$convenios->delete();
			$proyecto->delete();
			$comunidad_proyecto->delete();
			$encproy->delete();

			unset($request);
			unset($avance);
			unset($comunidad_proyecto);
			unset($convenios);
			unset($proyecto);
			unset($comunidad_proyecto);
			unset($encproy);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Error en la operación');
		}
	}

	public function updateProyectos(Request $request)
	{

		$pro = \App\proyectos::find($request['ProId']);

		$pro->proyectos_name = ucwords($request['NombrePro']);
		$pro->proyectos_fechaini = $request['fechaini'];
		$pro->proyectos_fechafin = $request['fechafin'];
		$pro->proyectos_objetivo = ucwords(($request['ObjetivoPro']));
		$pro->proyectos_nmeses = $request['valueMeses'];
		$pro->proyectos_Cumrequisitos = ucwords($request['chkcumple']);
		$pro->proyectos_estado = ucwords($request['estadoreq']);

		$encproy = DB::table('personal_proyecto')
			->leftJoin('personal', 'personal.personals_id', '=', 'personal_proyecto.personal_id')
			->leftJoin('proyectos', 'proyectos_id', '=', 'personal_proyecto.proyecto_id')
			->where('personal_proyecto.proyecto_id', $request['ProId'])
			->get();

		foreach ($encproy as $enp)

			$perproy = \App\personal_proyecto::find($enp->personal_proyecto_id);
		$perproy->personal_id = $request['idencargados'];
		$perproy->proyecto_id = $request['ProId'];



		//$presu = news::find($request['ProId']); esta reemplazar cuando tengas tabla presupuesto
		$value = DB::table('proyectos')
			->leftJoin('financiamiento_proyecto', 'proyecto_id', '=', 'proyectos.proyectos_id')
			->leftJoin('presupuesto', 'presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();


		foreach ($value as $val)
			$auxval = $val->presupuesto_id;

		$presu = \App\presupuesto::find($auxval);

		$presu->presupuesto_monto = $request['montodinero'];
		$presu->presupuesto_acumulado = $request['acumdinero'];
		$presu->presupuesto_observacion = ucwords($request['observaciondinero']);
		$presu->presupuesto_estado = ucwords($request['estadodinero']);

		$comunidadesafec = $request['seleccom'];

		foreach ($comunidadesafec as $comaf) {


			//$comu = \App\comunidad_proyecto::where("proyecto_id", "=", $request['ProId']);
			$comu = DB::table('comunidad_proyecto')
				->where('comunidad_proyecto.proyecto_id', '=', ucwords($request['ProId']))
				->get();
			foreach ($comu as $cm)

				$comuna = \App\comunidad_proyecto::find($cm->comunidad_proyecto_id);
			$comuna->comunidad_id = $comaf;
			$comuna->proyecto_id = $request['ProId'];
		}



		$pro->save();
		$presu->save();
		$perproy->save();
		$comuna->save();
		unset($request);
		unset($pro);
		unset($presu);
		unset($perproy);
		unset($comu);
		return back()->withMensaje('Operación Exitosa');
	}

	public function avance(Request $request)
	{

		$idpro = $request['ProId'];
		$managementArea = \App\managementArea::firstOrFail();
		$proyectos = \App\proyectos::All();

		$proy = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		foreach ($proy as $idavc)

			$idav = $idavc->avance_id;

		$avance = \App\avance::All();

		$avancenum = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->count();
		$avancenumaux = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		$proy = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		foreach ($proy as $pro);
		$porcavance = ($pro->proyectos_numInf);
		foreach ($avancenumaux as $avaux);


		if ($avancenum <= $porcavance) {
			if ($avaux->avance_id == null) {
				$percent = 0;
			} else {

				$percent = ($avancenum * 100) / ($porcavance);
				$percent = intval($percent);
			}
		} else {
			$percent = 100;
			$percent = intval($percent);
		}

		return view('admin.avance')
			->withId($idpro)
			->withProy($proy)
			->withPorcentaje($percent)
			->withAvance($avance)
			->withProyec($proyectos)
			->withManagement($managementArea);
	}

	public function addavance(Request $request)
	{


		try {
			\App\avance::create([
				'avance_proyecto_id' => $request['ProId'],
				'avance_doc' => $request->file('Infav')->getClientOriginalName(),
				'avance_fechainforme' => $request['finia'],
				'avance_observacion' => ucwords($request['observacionavance']),
				'avance_estado' => ucwords($request['porcentaje']),

			]);
			$nm = $request['ProId'];
			$var1 = 'docs/proyectos/';
			$var2 = 'avance/';
			$path = $var1 . $nm . '/' . $var2;
			$file = $request->file('Infav');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			unset($request);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}

	public function actavance(Request $request)
	{

		$avance = \App\avance::find($request['avcId']);

		$avance->avance_doc = $request->file('Infav')->getClientOriginalName();
		$avance->avance_fechainforme = $request['finia'];
		$avance->avance_observacion = ucwords($request['observacionavance']);
		$avance->avance_estado = ucwords($request['porcentaje']);



		//$docant = $avance->avance_doc;

		$nm = $request['ProId'];
		$var1 = 'docs/proyectos/';
		$var2 = 'avance/';
		$path = $var1 . $nm . '/' . $var2;
		$file = $request->file('Infav');
		$filename = $file->getClientOriginalName();
		$file->move($path, $filename);
		//\File::delete($path . '/' . $docant);



		$avance->save();
		unset($request);
		unset($avance);
		return back()->withMensaje('Operación Exitosa');
	}

	public function deleteavance(Request $request)
	{
		try {
			$avance = \App\avance::find($request['avcId']);
			$doc = $avance->avance_doc;
			$nm = $request['ProId'];
			$var1 = 'docs/proyectos/';
			$var2 = 'avance/';
			$path = $var1 . $nm . '/' . $var2;
			\File::delete($path . '/' . $doc);

			$avance->delete();

			unset($request);
			unset($avance);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Error en la operación');
		}
	}


	public function showEncargados(Request $request)
	{
		$encargados = \App\encargados::All();
		$managementArea = \App\managementArea::firstOrFail();
		return view('admin.encargados')
			->withManagement($managementArea)
			->withEncargados($encargados);
	}

	public function addEncargados(Request $request)
	{
		try {
			\App\encargados::create([
				'personal_name' => ucwords($request['NombreEnc']),
				'personal_last_name' => ucwords($request['ApellidoEnc']),
				'personal_cv' => $request['CvEnc']->getClientOriginalName(),
				'personal_cargo' => ucwords($request['CargoEnc']),
				'personal_ci' => $request['CiEnc'],
				'personal_direccion' => ucwords($request['DirEnc']),
				'personal_tlf' => $request['FonoEnc'],
				'personal_email' => $request['emailEnc'],
			]);

			$var1 = 'docs/encargados/';

			$path = $var1;
			$file = $request->file('CvEnc');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			unset($request);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}

	public function dltEncargados(Request $request)
	{
		try {
			$encargados = \App\encargados::find($request['encargadoId']);

			$encargados->delete();

			unset($request);
			unset($encargados);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Error en la operación');
		}
	}

	public function actEncargados(Request $request)
	{

		$encargados = \App\encargados::find($request['encargadoId']);

		$encargados->personal_name = ucwords($request['NombreEnc']);
		$encargados->personal_last_name = ucwords($request['ApellidoEnc']);

		$encargados->personal_cargo = ucwords($request['CargoEnc']);
		$encargados->personal_ci = $request['CiEnc'];
		$encargados->personal_direccion = ucwords($request['DirEnc']);
		$encargados->personal_tlf = $request['FonoEnc'];
		$encargados->personal_email = $request['emailEnc'];
		$encargados->save();
		unset($request);
		unset($encargados);
		return back()->withMensaje('Operación Exitosa');
	}

	public function download()
	{

		$encargados = \App\encargados::All();

		$managementArea = \App\managementArea::firstOrFail();
		$proyectos = \App\proyectos::All();

		$avancenum = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->count();

		$proyec = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->get();

		$presu = DB::table('proyectos')
			->leftJoin('financiamiento_proyecto', 'proyecto_id', '=', 'proyectos.proyectos_id')
			->leftJoin('presupuesto', 'presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->get();

		foreach ($proyectos as $pro)

			if ($avancenum <= $pro->proyectos_numInf) {

				$porcavance = ($pro->proyectos_numInf);
				$percent = $avancenum * 100 / $porcavance;
				$percent = intval($percent);
			} else {
				$percent = 100;
				$percent = intval($percent);
			}
		if ($avancenum == 0) {
			$percent = 0;
		}

		foreach ($proyectos as $pro)
			$date1 = new DateTime($pro->proyectos_fechaini);
		$date2 = new DateTime($pro->proyectos_fechafin);
		$difference = $date1->diff($date2);
		$diffInMonths  = $difference->m;
		$avance = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->get();

		$presupuesto = DB::table('proyectos')
			->leftJoin('financiamiento_proyecto', 'proyecto_id', '=', 'proyectos.proyectos_id')
			->leftJoin('presupuesto', 'presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->get();



		$encproye = DB::table('personal')
			->leftJoin('personal_proyecto', 'personal_id', '=', 'personal.personals_id')
			->leftJoin('proyectos', 'proyectos_id', '=', 'personal_proyecto.proyecto_id')
			->get();


		$pdf = PDF::loadView('admin.pdfproyectos', compact('diffInMonths', 'avance', 'presu', 'presupuesto', 'proyec', 'encargados', 'avancenum', 'encproye', 'percent', 'managementArea', 'proyectos'));
		$pdf->setPaper('a4', 'landscape');
		return $pdf->download('Proyecto.pdf');
	}

	public function downloadid(Request $request)
	{
		$pro = \App\proyectos::find($request['ProId']);

		$encproy = DB::table('personal_proyecto')
			->leftJoin('personal', 'personal.personals_id', '=', 'personal_proyecto.personal_id')
			->leftJoin('proyectos', 'proyectos_id', '=', 'personal_proyecto.proyecto_id')
			->where('personal_proyecto.proyecto_id', $request['ProId'])
			->get();
		$presuproy = DB::table('financiamiento_proyecto')
			->leftJoin('presupuesto', 'presupuesto.presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->leftJoin('proyectos', 'proyectos_id', '=', 'financiamiento_proyecto.proyecto_id')
			->where('financiamiento_proyecto.proyecto_id', $request['ProId'])
			->get();

		foreach ($encproy as $enp)

			$perproy = \App\personal_proyecto::find($enp->personal_proyecto_id);




		//$presu = news::find($request['ProId']); esta reemplazar cuando tengas tabla presupuesto
		$value = DB::table('proyectos')
			->leftJoin('financiamiento_proyecto', 'proyecto_id', '=', 'proyectos.proyectos_id')
			->leftJoin('presupuesto', 'presupuesto_id', '=', 'financiamiento_proyecto.financiamiento_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();

		$validp = $pro->proyectos_name;

		foreach ($value as $val)
			$auxval = $val->presupuesto_id;

		$presu = \App\presupuesto::find($auxval);

		//Avnace

		$id = $request['ProId'];
		$managementArea = \App\managementArea::firstOrFail();
		$proyectos = \App\proyectos::All();

		$proy = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		foreach ($proy as $idavc)

			$idav = $idavc->avance_id;

		$avance = \App\avance::All();

		$avancenum = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->count();
		$avancenumaux = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		$proy = DB::table('proyectos')
			->leftJoin('avance', 'avance_proyecto_id', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		foreach ($proy as $pro);
		$porcavance = ($pro->proyectos_numInf);
		foreach ($avancenumaux as $avaux);


		if ($avancenum <= $porcavance) {
			if ($avaux->avance_id == null) {
				$percent = 0;
			} else {

				$percent = ($avancenum * 100) / ($porcavance);
				$percent = intval($percent);
			}
		} else {
			$percent = 100;
			$percent = intval($percent);
		}



		//fin avance

		//convenio


		$convenios = \App\convenio::All();
		$financiamiento = \App\financiamiento_proyecto::All();


		foreach ($financiamiento as $dataFi);
		$monto = $dataFi->financiamiento_id;

		$costo = DB::table('presupuesto')
			->leftJoin('financiamiento_proyecto', 'financiamiento_id', '=', 'presupuesto_id')
			->where('financiamiento_proyecto.proyecto_id', $request['ProId'])
			->get();

		if (isset($convenios)) {
			$conv = 0;
			$v2 = 0;
		} else {
			foreach ($costo as $cst);
			foreach ($convenios as $conv);
			$v1 = $cst->presupuesto_monto;
			$valaux = $conv->convenios_sal;
			$v2 = $v1 - $valaux;
		}




		$idpro = $request['ProId'];
		$proy = DB::table('proyectos')
			->leftJoin('convenios', 'convenios_id_proyecto', '=', 'proyectos.proyectos_id')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();

		$dataproy = DB::table('proyectos')
			->select('proyectos.proyectos_id', 'proyectos.proyectos_name', 'proyectos.proyectos_objetivo')
			->where('proyectos.proyectos_id', $request['ProId'])
			->get();
		$datapers = DB::table('personal_proyecto')
			->select('personal_proyecto.personal_proyecto_id', 'personal_proyecto.personal_id', 'personal_proyecto.proyecto_id')
			->where('personal_proyecto.proyecto_id', $request['ProId'])
			->get();

		$encargado = \App\encargados::All();
		$persproy = \App\personal_proyecto::All();

		$tipgarantia = \App\tiposgarantia::All();
		$managementArea = \App\managementArea::firstOrFail();
		$proyectos = \App\proyectos::All();
		$encproy = DB::table('personal_proyecto')
			->leftJoin('personal', 'personal.personals_id', '=', 'personal_proyecto.personal_id')
			->where('personal_proyecto.proyecto_id', $request['ProId'])
			->get();

			$garantia= DB::table('garantia')
			->leftJoin('proyectos', 'proyectos.proyectos_id', '=', 'garantia.garantia_proyecto_id')
			->where('garantia.garantia_proyecto_id', $request['ProId'])
			->get();
			$garantia=\App\garantia::All();



		//finconvenio



		$pdf = PDF::loadView('admin.pdfproyectosid', compact(
			'presu',
			'perproy',
			'pro',
			'encproy',
			'presuproy',
			'id',
			'proy',
			'percent',
			'avance',
			'proyectos',
			'managementArea',
			'datapers',
			'dataproy',
			'v2',
			'persproy',
			'costo',
			'encargado',
			'proy',
			'proyectos',
			'convenios',
			'garantia'

		));
		$pdf->setPaper('a4', 'portrait');
		return $pdf->download('Proyecto_' . $validp . '.pdf');
	}
}
