<?php

namespace App\Http\Controllers;

use Auth;
use App\authority;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;

class authorityController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$authorityTable = \App\user::orderBy('user_update', 'desc')->get();
		$authorityType = \App\authorityType::All();
		$authorityCargo = \App\cargo::All();
		$authorityEstado = \App\authorityEstado::All();
		return view('admin.authority')
			->withManagement($managementArea)
			->withTypes($authorityType)
			->withCargo($authorityCargo)
			->withEstado($authorityEstado)
			->withAuthority($authorityTable);
	}

	public function showInsert()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$authorityTable = \App\user::orderBy('user_update', 'desc')->get();
		$authorityType = \App\authorityType::All();
		$authorityCargo =\App\cargo::All();
		$authorityEstado = \App\authorityEstado::All();
		return view('admin.authorityNew')
			->withManagement($managementArea)
			->withTypes($authorityType)
			->withCargo($authorityCargo)
			->withEstado($authorityEstado)
			->withAuthority($authorityTable);
	}
	public function store(Request $request)
	{
		
		try {
			
			$docs =$request->file('authorityCv');
			$photo = $request->file('authorityPhoto');

			\App\user::create([
				'user_name' => ucwords($request->authorityName),
				'user_last_name' => ucwords($request->authorityLastName),
				'user_cv' => $request->file('authorityCv')->getClientOriginalName(),
				'user_management_area' => '1',
				'user_cargo_a' => $request->authorityCargo,
				'user_type' => $request->authorityType,
				'user_estado' => $request->authorityEstado,
				'user_photo' => $request->file('authorityPhoto')->getClientOriginalName(),
				'user_ci' => $request->authorityCi,
				'user_direccion' => $request->authorityDireccion,
				'user_telefono' => $request->authorityTelefono,
				'user_mail' => $request->authorityEmail,
				'user_observaciones' => $request->authorityObservaciones,
				'password'=> bcrypt($request->authorityCi),		
			]);

			$path1 = 'img/authority';
			$path2 = 'docs/authority';
			$filename1 =$photo->getClientOriginalName();
			$filename2 = $docs->getClientOriginalName();
			$photo->move($path1, $filename1);
			$docs->move($path2, $filename2);
			unset($request);
			
			return redirect('admin/authority');
		} catch (Exception $e) {
			return back($e);
		}
	}

	public function update(Request $request)
	{
	
		$this->validate($request, [

			'authorityName' => 'required|max:100',
			'authorityLastName' => 'required|max:100',
			'authorityType' => 'required',
			'authorityPhoto' => 'mimes:jpg,jpeg,png|image',
			'authorityCv' => 'mimes:pdf,doc,docx|file',
			'authorityEstado' => 'required',
		]);


		$authority = authority::find($request->authorityId);
		$authority->user_name = ucwords($request->authorityName);
		$authority->user_last_name = ucwords($request->authorityLastName);
		$authority->user_cargo_a = $request->authorityCargo;
		$authority->user_type = $request->authorityType;
		$authority->user_estado = $request->authorityEstado;
		$authority->user_ci = $request->authorityCi;
		$authority->user_direccion = $request->authorityDireccion;
		$authority->user_telefono = $request->authorityTelefono;
		$authority->user_mail = $request->authorityEmail;
		$authority->user_observaciones = $request->authorityObservaciones;
		

		if ($request->hasFile('authorityPhoto')) {
			$imagen = $authority->user_photo;
			$authority->user_photo = $request->file('authorityPhoto')->getClientOriginalName();
			$path = 'img/authority';
			$file = $request->file('authorityPhoto');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			\File::delete($path . '/' . $imagen);
		}

		if ($request->hasFile('authorityCv')) {
			$docs = $authority->user_cv;
			$authority->user_cv = $request->file('authorityCv')->getClientOriginalName();
			$path = 'docs/authority';
			$file = $request->file('authorityCv');
			$filename = $file->getClientOriginalName();
			$file->move($path, $filename);
			\File::delete($path . '/' . $docs);
		}

		$authority->save();

		unset($request);
		unset($authority);
		return back()->withMensaje('Operación exitosa');
	}


	public function delete(Request $request)
	{
		try {
			$authority = \App\user::find($request['autorityId']);
			$imagen = $authority->user_photo;
			$path = 'img/authority';
			$docs = $authority->user_cv;
			$path2 = 'docs/authority';
			//dd($imagen,$path);
			$authority->delete();
			unset($request);
			\File::delete($path . '/' . $imagen);
			\File::delete($path2 . '/' . $docs);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Error en la operación');
		}
	}
}
