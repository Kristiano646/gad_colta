<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class scheduleController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\schedule::All();
		$curricularModule= \App\program::All();
		return view ('admin.schedule')
		->withManagement($managementArea)
		->withOption($curricularModule)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'scheduleDay' => 'required|max:100|:schedule_program,schedule_program_day',
			'scheduleBegin' => 'required:schedule_program,schedule_program_hour_begin',
    		'scheduleEnd' => 'required:schedule_program,schedule_program_hour_end',
    		'scheduleProgram' => 'required:schedule_program,schedule_program_program',   
            ]);

		\App\schedule::create([
			'schedule_program_day' => $request['scheduleDay'],
			'schedule_program_hour_begin' => $request['scheduleBegin'],
            'schedule_program_hour_end' => $request['scheduleEnd'],
			'schedule_program_program' => $request['scheduleProgram'],
            ]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\schedule::find($request['categoryId']);

	/*	$this->validate($request,[

			'coordinatorName' => 'required|max:100|unique:coordinator_program,coordinator_program_name,'.$category->coordinator_program_id.',coordinator_program_id',
			'coordinatorLastName' => 'required|max:100|unique:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|unique:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);*/
		$category->schedule_program_day = $request['scheduleDay'];
		$category->schedule_program_hour_begin = $request['scheduleBegin'];
        $category->schedule_program_hour_end = $request['scheduleEnd'];
		$category->schedule_program_program = $request['scheduleProgram'];
        $category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa, coordinador ingresado con éxito.');

	}}