<?php

namespace App\Http\Controllers;

use App\garantia;
use App\miembro;
use Auth;
use Hamcrest\Core\HasToString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;
use DateTime;
use Yajra\DataTables\DataTables; //for Laravel DataTable JS...
use Barryvdh\DomPDF\Facade as PDF;

class comunidadController extends Controller
{
	public function show()
	{ 	
		$managementArea = \App\managementArea::firstOrFail();
		$userId = Auth::user()->user_id;
		$comunidadTable = \App\comunidad::orderBy('comunidad_update', 'desc')->get();

		return view('admin.comunidad')
			->withManagement($managementArea)
			->withCategory($comunidadTable);
	}

	public function store(Request $request)
	{

		try {
			$comunidadId = $request->ComunidadId;


			\App\comunidad::create([

				'comunidad_description' => $request['comunidadDescription'],
				'comunidad_state' => 1,
				'comunidad_name' => ucwords($request['comunidadName']),
				'comunidad_ubicacion' => $request['comunidadUbicacion'],
				'comunidad_type_sociedad' => ucwords($request['comunidadTipo']),
				'comunidad_type_organizacion' => ucwords($request['comunidadTipoOrg']),
				'comunidad_type_suborganizacion' => ucwords($request['comunidadSubmenut1']),
				'comunidad_cont_name' => ucwords($request['ComunidadConNom']),
				'comunidad_cont_direccion' => $request['ComunidadConDir'],
				'comunidad_cont_tel' => $request['ComunidadConTel'],
				'comunidad_cont_email' => $request['ComunidadConEmail'],
				'comunidad_num_hom' => $request['ComunidadNhombres'],
				'comunidad_num_muj' => $request['ComunidadNmujeres'],
				'comunidad_num_poblacion' => $request['ComunidadPbase'],
				'comunidad_num_familias' => $request['ComunidadNfamilias'],
				'comunidad_num_mestizos' => $request['ComunidadMestizos'],
				'comunidad_num_indigenas' => $request['ComunidadIndigenas'],
				'comunidad_ruc' => $request['ComunidadRuc'],
				'comunidad_name_banco' => ucwords($request['ComunidadNBanco']),
				'comunidad_num_cuenta' => $request['ComunidadNumCuen'],
				'comunidad_type_cuenta' => $request['comunidadtypeCuen'],
				'comunidad_observaciones' => $request['ComunidadObs'],


			]);
			unset($request);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}

	public function delete(Request $request)
	{
		try {

			$comunidad = \App\comunidad::find($request['comunidadId']);
			$comunidad->delete();
			unset($request);
			unset($comunidad);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Error en la operación');
		}
	}

	public function update(Request $request)
	{

		$comunidad = \App\comunidad::find($request['comunidadId']);


		$comunidad->comunidad_description = $request['comunidadDescription'];
		$comunidad->comunidad_state = 1;
		$comunidad->comunidad_name = ucwords($request['comunidadName']);
		$comunidad->comunidad_ubicacion = $request['comunidadUbicacion'];
		$comunidad->comunidad_type_sociedad = ucwords($request['comunidadTipo']);
		$comunidad->comunidad_type_organizacion = ucwords($request['comunidadTipoOrg']);
		$comunidad->comunidad_type_suborganizacion = ucwords($request['comunidadSubmenut']);
		$comunidad->comunidad_cont_name = ucwords($request['ComunidadConNom']);
		$comunidad->comunidad_cont_direccion = $request['ComunidadConDir'];
		$comunidad->comunidad_cont_tel = $request['ComunidadConTel'];
		$comunidad->comunidad_cont_email = $request['ComunidadConEmail'];
		$comunidad->comunidad_num_hom = $request['ComunidadNhombres'];
		$comunidad->comunidad_num_muj = $request['ComunidadNmujeres'];
		$comunidad->comunidad_num_poblacion = $request['ComunidadPbase'];
		$comunidad->comunidad_num_familias = $request['ComunidadNfamilias'];
		$comunidad->comunidad_num_mestizos = $request['ComunidadMestizos'];
		$comunidad->comunidad_num_indigenas = $request['ComunidadIndigenas'];
		$comunidad->comunidad_ruc = $request['ComunidadRuc'];
		$comunidad->comunidad_name_banco = ucwords($request['ComunidadNBanco']);
		$comunidad->comunidad_num_cuenta = $request['ComunidadNumCuen'];
		$comunidad->comunidad_type_cuenta = $request['comunidadtypeCuen'];
		$comunidad->comunidad_observaciones = $request['ComunidadObs'];

		$comunidad->save();
		unset($request);
		unset($comunidad);
		return back()->withMensaje('Operación Exitosa');
	}




	public function showDirectivas(Request $request)
	{

		$most = '2';
		$from = new DateTime($request['start_date']);
		$to = new DateTime($request['end_date']); //date($request['newsDateFin']);

		//$to=$request['newsDateFin'];
		$directiva = DB::table('miembro')
			->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
			->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
			->where('comunidad_id', $request['status_id'])
			->whereDate('miembro_directiva_fechaini', '>=', $from)
			->whereDate('miembro_directiva_fechafin', '<=', $to)
			->get();

		$member = \App\miembro::orderBy('miembro_id', 'ASC')
			->when($request->miembroId, function ($query) use ($request) {
				$query->where("miembroId", "LIKE", "%");
			})

			->when($request->start_date, function ($query) use ($request) {
				$dateS = date("Y-m-d", strtotime($request->start_date));
				$query->whereDate("miembro_directiva_fechaini", ">=", $dateS);
			})
			->when($request->newsDateFin, function ($query) use ($request) {
				$dateE = date("Y-m-d", strtotime($request->newsDateFin));
				$query->whereDate("miembro_directiva_fechafin", "<=", $dateE);
			})
			->latest()
			->paginate(15);
		$nombreComunidad = DB::table('miembro')
			->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
			->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
			->select('comunidad.comunidad_name', 'comunidad.comunidad_id', 'miembro.miembro_id', 'miembro_directiva_id', 'miembro.miembro_nombres', 'miembro.miembro_cargo', 'miembro.miembro_sexo', 'miembro.miembro_directiva_fechaini', 'miembro.miembro_directiva_fechafin')
			->get();
		$dire = \App\miembro::All();
		//::whereBetween('miembro_directiva_fechaini', [$from, $to])
		//$comunidadID=\App\miembro::find($request['comunidadId']);
		$managementArea = \App\managementArea::firstOrFail();
		$comunidades = \App\comunidad::All();
		$directivas = \App\directivas::All();
		$miembro = \App\miembro::All();

		return view('admin.ComunidadesDirectiva')
			->withDire($directiva)   //problema aqui en retorno del controlador con variable consultada
			->withDirectiva($directiva)
			->withDirectivas($directivas)
			->withManagement($managementArea)
			->withMiembro($miembro)
			->withCategory($comunidades)
			->withData($nombreComunidad)
			->withTo($to)
			->withMember($member)
			->withMost($most)
			->withFrom($from);
	}




	public function storeDirectivas(Request $request)
	{

		try {


			\App\directivas::create([

				'directiva_comunidad_id' => $request['comunidadId'],
				'directiva_descripcion' => ucwords($request['Desc']),
				'directiva_estado' => $request['EstadoD']



			]);


			unset($request);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}


	public function storeMiembros(Request $request)
	{

		try {


			$tenerid = DB::table('directiva')
				->where('directiva_descripcion', $request['DescM'])
				->get();
			foreach ($tenerid as $id) {
				//dd(($id->directiva_id));

			}
			\App\miembro::create([

				'miembro_directiva_id' => $id->directiva_id,
				'miembro_nombres' => ucwords($request['NomApp']),
				'miembro_cargo' => ucwords($request['CargoM']),
				'miembro_sexo' => ucwords($request['SexM']),
				'miembro_directiva_fechaini' => $request['fechaini'],
				'miembro_directiva_fechafin' => $request['fechafin'],

			]);


			unset($request);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}

	public function updateDirectivas(Request $request)
	{

		$time = time();
		try {
			$directivas = \App\directivas::find($request['directivaId']);
			$miembrod = \App\miembro::find($request['miembroId']);
			$directivas->directiva_comunidad_id = $request['comunidadId'];
			$miembrod->miembro_nombres = ucwords($request['NombreM']);
			$miembrod->miembro_cargo = ucwords($request['CargoM']);
			$miembrod->miembro_directiva_fechaini = $request['fechaini'];
			$miembrod->miembro_directiva_fechafin = $request['fechafin'];
			$miembrod->save();
			$directivas->save();
			unset($request);
			unset($miembrod);
			unset($directivas);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Falló Operación');
		}
	}

	public function deleteDirectivas(Request $request)
	{
		try {


			$directivas = \App\directivas::find($request['directivaId']);
			$directivas->delete();
			unset($request);
			unset($directivas);
			return back()->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return back()->withMensaje('Error en la operación');
		}
	}

	public function showdatatable(Request $request)
	{
		$most = ($request['mostar']);
		$from = new DateTime($request['start_date']);
		$to = new DateTime($request['end_date']);
		$miembro = \App\miembro::All();
		foreach ($miembro as $fechm)
		$bdfi=new DateTime($fechm->miembro_directiva_fechaini);
		$bdff=new DateTime($fechm->miembro_directiva_fechafin);
		
        $idv= $request['status_id'];
		if ($from != '1970-01-01' && $to != '1970-01-01' && $idv != null) {
			$directiva = DB::table('miembro')
				->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
				->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
				->where('comunidad_id', $request['status_id'])
				->where('miembro.miembro_directiva_fechaini', '>=', $from)
				->where('miembro.miembro_directiva_fechafin', '<=', $to)
				->get();
		} else {
			$directiva = DB::table('miembro')
				->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
				->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
				->where('miembro.miembro_directiva_fechaini', '>=', $from)
				->where('miembro.miembro_directiva_fechafin', '<=', $to)
				->get();
		}
		if ( $from == '1970-01-01' && $to == '1970-01-01' && $idv != null) {
			$directiva = DB::table('miembro')
				->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
				->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
				->where('comunidad_id', $request['status_id'])
				->get();
		}
		$member = \App\miembro::orderBy('miembro_id', 'ASC')
			->when($request->miembroId, function ($query) use ($request) {
				$query->where("miembroId", "LIKE", "%");
			})

			->when($request->start_date, function ($query) use ($request) {
				$dateS = $request->start_date;
				$query->whereDate("miembro_directiva_fechaini", ">=", $dateS);
			})
			->when($request->newsDateFin, function ($query) use ($request) {
				$dateE = $request->newsDateFin;
				$query->whereDate("miembro_directiva_fechafin", "<=", $dateE);
			})
			->latest()
			->paginate(15);
		$nombreComunidad = DB::table('miembro')
			->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
			->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
			->select('comunidad.comunidad_name', 'comunidad.comunidad_id', 'miembro.miembro_id', 'miembro_directiva_id', 'miembro.miembro_nombres', 'miembro.miembro_cargo', 'miembro.miembro_sexo', 'miembro.miembro_directiva_fechaini', 'miembro.miembro_directiva_fechafin')
			->get();
		$dire = \App\miembro::All();
		//::whereBetween('miembro_directiva_fechaini', [$from, $to])
		//$comunidadID=\App\miembro::find($request['comunidadId']);
		$managementArea = \App\managementArea::firstOrFail();
		$comunidades = \App\comunidad::All();
		$directivas = \App\directivas::All();
		

		return view('admin.ComunidadesDirectiva')
			->withDire($directiva)   //problema aqui en retorno del controlador con variable consultada
			->withDirectiva($directiva)
			->withDirectivas($directivas)
			->withManagement($managementArea)
			->withMiembro($miembro)
			->withCategory($comunidades)
			->withData($nombreComunidad)
			->withTo($to)
			->withMember($member)
			->withMost($most)
			->withFrom($from);
	}
	public function download(Request $request)
{
	$com = \App\comunidad::All();
	$directivas = \App\directivas::All();
	$management = \App\managementArea::firstOrFail();
	$category = \App\comunidad::orderBy('comunidad_update', 'desc')->get();
    $garantia= \App\garantia::All();
	$pdf = PDF::loadView('admin.pdf', compact('com','directivas','management','category','garantia'));
	

    return $pdf->download('comunidad.pdf');
}

public function downloaddirectiva(Request $request)
{
	$most = '2';
		$from = new DateTime($request['start_date']);
		$to = new DateTime($request['end_date']); //date($request['newsDateFin']);

		//$to=$request['newsDateFin'];
		$directiva = DB::table('miembro')
			->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
			->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
			->where('comunidad_id', $request['status_id'])
			->whereDate('miembro_directiva_fechaini', '>=', $from)
			->whereDate('miembro_directiva_fechafin', '<=', $to)
			->get();

		$member = \App\miembro::orderBy('miembro_id', 'ASC')
			->when($request->miembroId, function ($query) use ($request) {
				$query->where("miembroId", "LIKE", "%");
			})

			->when($request->start_date, function ($query) use ($request) {
				$dateS = date("Y-m-d", strtotime($request->start_date));
				$query->whereDate("miembro_directiva_fechaini", ">=", $dateS);
			})
			->when($request->newsDateFin, function ($query) use ($request) {
				$dateE = date("Y-m-d", strtotime($request->newsDateFin));
				$query->whereDate("miembro_directiva_fechafin", "<=", $dateE);
			})
			->latest()
			->paginate(15);
		$nombreComunidad = DB::table('miembro')
			->leftJoin('directiva', 'directiva_id', '=', 'miembro.miembro_directiva_id')
			->leftJoin('comunidad', 'comunidad_id', '=', 'directiva.directiva_comunidad_id')
			->select('comunidad.comunidad_name', 'comunidad.comunidad_id', 'miembro.miembro_id', 'miembro_directiva_id', 'miembro.miembro_nombres', 'miembro.miembro_cargo', 'miembro.miembro_sexo', 'miembro.miembro_directiva_fechaini', 'miembro.miembro_directiva_fechafin')
			->get();
		$dire = \App\miembro::All();
		//::whereBetween('miembro_directiva_fechaini', [$from, $to])
		//$comunidadID=\App\miembro::find($request['comunidadId']);
		$managementArea = \App\managementArea::firstOrFail();
		$comunidades = \App\comunidad::All();
		$directivas = \App\directivas::All();
		$miembro = \App\miembro::All();

		
    
	$pdf = PDF::loadView('admin.pdfdirectivas', compact('directiva','nombreComunidad','directivas','managementArea','miembro','comunidades','member','most'));
	foreach($nombreComunidad as $name);
	$namc=$name->comunidad_name;

   // return $pdf->download('directiva_'.$namc.'.pdf');
   return $pdf->download('resumen_general_directivas.pdf');
}



}
