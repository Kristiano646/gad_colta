<?php

namespace App\Http\Controllers;

use Auth;
use App\senso1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SensosController extends Controller
{
	public function showSenso1(Request $request)

	{
		$tipocen = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['Datatiposen'])
			->get();
		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();
		return view('admin.senso1')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withTipsenso($tipocen)
			->withSenso($senso);
	}
	


	public function showtipo1(Request $request)
	{
		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();
		$tipocen = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['Datatiposen'])
			->get();


		return view('admin.senso1')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withSenso($senso)
			->withTipsenso($tipocen);
	}

	public function Sensoadd1(Request $request)
	{
		\App\senso1::create([

			'infocenso_id_Tipos' => $request['idtipo'],
			'infocenso_description' => ucwords($request['sensoDescription']),
		]);

		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();
		$tipocen = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['Datatiposen'])
			->get();

		unset($request);
		return view('admin.senso1')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withSenso($senso)
			->withTipsenso($tipocen)
			->withMensaje('Operación Exitosa');
	}

	public function Sensoupd1(Request $request)
	{
		$sensou = senso1::find($request->SensoId);
		$sensou->infocenso_id_Tipos = $request->idtipo;
		$sensou->infocenso_description = ucwords($request->sensoDescription);
		$sensou->save();
		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();
		$tipocen = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['Datatiposen'])
			->get();

		unset($sensou);
		unset($request);
		return view('admin.senso1')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withSenso($senso)
			->withTipsenso($tipocen)
			->withMensaje('Operación Exitosa');
	}


	public function SensoDlt1(Request $request)
	{
		try {

			$sensou = senso1::find($request->SensoId);
			$sensou->delete();
			$managementArea = \App\managementArea::firstOrFail();
			$senso = \App\senso1::All();
			$tipo = \App\tiposenso::All();
			$tipocen = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['Datatiposen'])
			->get();
			unset($sensou);
			unset($request);
			return view('admin.senso1')
				->withManagement($managementArea)
				->withTiposen($tipo)
				->withSenso($senso)
				->withTipsenso($tipocen)
				->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return view('admin.senso1')->withMensaje('Error en la operación');
		}
	}

	//senso 2


	public function showSenso2(Request $request)
	{
		$tipocen = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['Datatiposen'])
			->get();
		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso2::All();
		$tipo = \App\tiposenso2::All();
		return view('admin.senso2')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withTipsenso($tipocen)
			->withSenso($senso);

		
	}
	public function Sensoadd2(Request $request)
	{
		\App\senso2::create([

			'infocenso2_id_Tipos' => $request['idtipo'],
			'infocenso2_description' => ucwords($request['sensoDescription']),
		]);

		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso2::All();
		$tipo = \App\tiposenso2::All();
		$tipocen = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['Datatiposen'])
			->get();

		unset($request);
		return view('admin.senso2')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withSenso($senso)
			->withTipsenso($tipocen)
			->withMensaje('Operación Exitosa');
	}

	public function showtipo2(Request $request)
	{
		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso2::All();
		$tipocen = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['Datatiposen'])
			->get();

		return view('admin.senso2')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withSenso($senso)
			->withTipsenso($tipocen);
	}


	public function Sensoupd2(Request $request)
	{
		$sensou = senso2::find($request->SensoId);
		$sensou->infocenso_id_Tipos = $request->idtipo;
		$sensou->infocenso_description = ucwords($request->sensoDescription);
		$sensou->save();
		$managementArea = \App\managementArea::firstOrFail();
		$senso = \App\senso2::All();
		$tipo = \App\tiposenso2::All();
		$tipocen = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['Datatiposen'])
			->get();


		unset($sensou);
		unset($request);
		return view('admin.senso2')
			->withManagement($managementArea)
			->withTiposen($tipo)
			->withSenso($senso)
			->withTipsenso($tipocen)
			->withMensaje('Operación Exitosa');
	}


	public function SensoDlt2(Request $request)
	{
		try {

			$sensou = senso2::find($request->SensoId);
			$sensou->delete();
			$managementArea = \App\managementArea::firstOrFail();
			$senso = \App\senso2::All();
			$tipo = \App\tiposenso2::All();
			$tipocen = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['Datatiposen'])
			->get();

			unset($sensou);
			unset($request);
			return view('admin.senso2')
				->withManagement($managementArea)
				->withTiposen($tipo)
				->withSenso($senso)
				->withTipsenso($tipocen)
				->withMensaje('Operación Exitosa');
		} catch (Exception $e) {
			return view('admin.senso2')->withMensaje('Error en la operación');
		}
	}
}
