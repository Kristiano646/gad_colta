<?php

namespace App\Http\Controllers;

use Auth;
use App\senso1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class infosensalController extends Controller
{
	public function showinc1(Request $request)

	{

		$aux = 0;
		$verify = DB::table('infoc1_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->count();
		$infocom = DB::table('comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->get();
		$idtipoinf = $request['tipoinfo'];
		$managementArea = \App\managementArea::firstOrFail();
		$tipoinfo = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['tipoinfo'])
			->get();
		$comunidad = \App\comunidad::All();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();
		$info = \App\senso1::All();
		$inf = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();

		$seninf = DB::table('infoc1_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			// ->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			// ->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			//->and('infocenso', 'infocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();
		$tiposinfc = DB::table('infoc1_comunidad')
			//->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			->get();
		return view('admin.infocenso1')
			->withInfc($tiposinfc)
			->withSenin($seninf)
			->withAuxi($aux)
			->withSenso($info)
			->withIdtipoinf($idtipoinf)
			->withInfc($inf)
			->withManagement($managementArea)
			->withComunidad($comunidad)
			->withTipo($tipoinfo)
			->withTiposen($tipo)
			->withSenso($senso)
			->withInfoco($infocom);
	}

	public function addinfosen1(Request $request)

	{

		$aux = 1;
		$verify = DB::table('infoc1_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->count();
		$infocom = DB::table('comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->get();
		$idtipoinf = $request['tipoinfo'];
		$managementArea = \App\managementArea::firstOrFail();

		$tipoinfo = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', $request['tipoinfo'])
			->get();


		$inf = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();

		$seninf = DB::table('infoc1_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			// ->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			// ->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			//->and('infocenso', 'infocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();
		$tiposinfc = DB::table('infoc1_comunidad')
			//->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			->get();

		$comunidad = \App\comunidad::All();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();
		return view('admin.infocenso1')
			->withInfc($tiposinfc)
			->withInfc($inf)
			->withSenin($seninf)
			->withAuxi($aux)
			->withIdtipoinf($idtipoinf)
			->withManagement($managementArea)
			->withComunidad($comunidad)
			->withTipo($tipoinfo)
			->withTiposen($tipo)
			->withSenso($senso)
			->withInfoco($infocom);
	}
	public function enviarBD(Request $request)
	{
		$aux = 0;
		$verify = DB::table('infoc1_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->count();
		$infocom = DB::table('comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->get();
		$idtipoinf = $request['tipoinfo'];
		$managementArea = \App\managementArea::firstOrFail();
		$tipoinfo = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->where('infocenso.infocenso_id_Tipos', 1)
			->get();
		$inf = DB::table('infocenso')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();

		$seninf = DB::table('infoc1_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			// ->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			// ->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			//->and('infocenso', 'infocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();
		$tiposinfc = DB::table('infoc1_comunidad')
			//->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			->get();
		$comunidad = \App\comunidad::All();
		$senso = \App\senso1::All();
		$tipo = \App\tiposenso::All();

		\App\infoc1_comunidad::create([

			'infoc1_id_comunidad' => $request['idComunidad'],
			'infoc1_id_tipoinfcensal' => $request['idTipo'],
			'infoc1_infocensalparam' => $request['idparam'],
			'infoc1_valor' => $request['valoring'],
		]);
		unset($request);
		return redirect('admin/infosenso1')
			->withInfc($tiposinfc)
			->withInfc($inf)
			->withSenin($seninf)
			->withAuxi($aux)
			->withIdtipoinf($idtipoinf)
			->withManagement($managementArea)
			->withComunidad($comunidad)
			->withTipo($tipoinfo)
			->withTiposen($tipo)
			->withSenso($senso)
			->withInfoco($infocom)
			->withMensaje('Operación Exitosa');
	}

	//para infocensal2 

	public function showinc2(Request $request)

	{
		$aux = 0;
		$verify = DB::table('infoc2_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc2_comunidad.infoc2_id_comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->count();
		$infocom = DB::table('comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->get();
		$idtipoinf = $request['tipoinfo'];
		$managementArea = \App\managementArea::firstOrFail();
		$tipoinfo = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['tipoinfo'])
			->get();
		$inf = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->get();

		$seninf = DB::table('infoc2_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc2_comunidad.infoc2_id_comunidad')
			// ->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			// ->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			//->and('infocenso', 'infocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();
		$tiposinfc = DB::table('infoc2_comunidad')
			//->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infoc2_comunidad.infoc2_id_tipoinfcensal')
			->leftJoin('infocenso2', 'infocenso2_id_Tipos', '=', 'tiposinfocenso2.Tiposinfocenso_id')
			->get();
		$comunidad = \App\comunidad::All();
		$senso = \App\senso2::All();
		$tipo = \App\tiposenso2::All();
		return view('admin.infocenso2')
			->withInfc($tiposinfc)
			->withInfc($inf)
			->withSenin($seninf)
			->withAuxi($aux)
			->withIdtipoinf($idtipoinf)
			->withManagement($managementArea)
			->withComunidad($comunidad)
			->withTipo($tipoinfo)
			->withTiposen($tipo)
			->withSenso($senso)
			->withInfoco($infocom);
	}

	public function addinfosen2(Request $request)

	{
		$aux = 1;
		$verify = DB::table('infoc2_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc2_comunidad.infoc2_id_comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->count();
		$infocom = DB::table('comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->get();
		$idtipoinf = $request['tipoinfo'];
		$managementArea = \App\managementArea::firstOrFail();
		$tipoinfo = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', $request['tipoinfo'])
			->get();

		$inf = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->get();

		$seninf = DB::table('infoc2_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc2_comunidad.infoc2_id_comunidad')
			// ->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			// ->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			//->and('infocenso', 'infocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();
		$tiposinfc = DB::table('infoc2_comunidad')
			//->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infoc2_comunidad.infoc2_id_tipoinfcensal')
			->leftJoin('infocenso2', 'infocenso2_id_Tipos', '=', 'tiposinfocenso2.Tiposinfocenso_id')
			->get();


		$comunidad = \App\comunidad::All();
		$senso = \App\senso2::All();
		$tipo = \App\tiposenso2::All();
		return view('admin.infocenso2')
			->withInfc($tiposinfc)
			->withInfc($inf)
			->withSenin($seninf)
			->withAuxi($aux)
			->withIdtipoinf($idtipoinf)
			->withManagement($managementArea)
			->withComunidad($comunidad)
			->withTipo($tipoinfo)
			->withTiposen($tipo)
			->withSenso($senso)
			->withInfoco($infocom);
	}
	public function enviarBD2(Request $request)
	{
		$aux = 0;
		$verify = DB::table('infoc2_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc2_comunidad.infoc2_id_comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->count();
		$infocom = DB::table('comunidad')
			->where('comunidad.comunidad_id', $request['idcomunidad'])
			->get();
		$idtipoinf = $request['tipoinfo'];
		$managementArea = \App\managementArea::firstOrFail();
		$tipoinfo = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->where('infocenso2.infocenso2_id_Tipos', 1)
			->get();
		$inf = DB::table('infocenso2')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infocenso2.infocenso2_id_Tipos')
			->get();

		$seninf = DB::table('infoc2_comunidad')
			->leftJoin('comunidad', 'comunidad_id', '=', 'infoc2_comunidad.infoc2_id_comunidad')
			// ->leftJoin('tiposinfocenso1', 'Tiposinfocenso_id', '=', 'infoc1_comunidad.infoc1_id_tipoinfcensal')
			// ->leftJoin('infocenso', 'infocenso_id_Tipos', '=', 'tiposinfocenso1.Tiposinfocenso_id')
			//->and('infocenso', 'infocenso_id', '=', 'infocenso.infocenso_id_Tipos')
			->get();
		$tiposinfc = DB::table('infoc2_comunidad')
			//->leftJoin('comunidad', 'comunidad_id', '=', 'infoc1_comunidad.infoc1_id_comunidad')
			->leftJoin('tiposinfocenso2', 'Tiposinfocenso_id', '=', 'infoc2_comunidad.infoc2_id_tipoinfcensal')
			->leftJoin('infocenso2', 'infocenso2_id_Tipos', '=', 'tiposinfocenso2.Tiposinfocenso_id')
			->get();
		$comunidad = \App\comunidad::All();
		$senso = \App\senso2::All();
		$tipo = \App\tiposenso2::All();

		\App\infoc2_comunidad::create([

			'infoc2_id_comunidad' => $request['idComunidad'],
			'infoc2_id_tipoinfcensal' => $request['idTipo'],
			'infoc2_infocensalparam' => $request['idparam'],
			'infoc2_valor' => $request['valoring'],
		]);
		unset($request);
		return redirect('admin/infosenso2')
		
			->withInfc($tiposinfc)
			->withInfc($inf)
			->withSenin($seninf)
			->withAuxi($aux)
			->withIdtipoinf($idtipoinf)
			->withManagement($managementArea)
			->withComunidad($comunidad)
			->withTipo($tipoinfo)
			->withTiposen($tipo)
			->withSenso($senso)
			->withInfoco($infocom)
			->withMensaje('Operación Exitosa');
	}
}
