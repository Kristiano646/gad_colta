<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class moduleController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\module::All();
		return view ('admin.module')
		->withManagement($managementArea)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'moduleTitle' => 'required|max:100|:module_curricular_pensum,module_curricular_pensum_number',
            'moduleDescription' => 'required|max:100|:module_curricular_pensum,module_curricular_pensum_name',
			]);

		\App\module::create([
			'module_curricular_pensum_number' => $request['moduleTitle'],
			'module_curricular_pensum_name' => $request['moduleDescription'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\module::find($request['categoryId']);

		$this->validate($request,[

			'moduleTitle' => 'required|max:100|:module_curricular_pensum,module_curricular_pensum_number,'.$category->module_curricular_pensum_id.',module_curricular_pensum_id',
            'moduleDescription' => 'required|max:100|:module_curricular_pensum,module_curricular_pensum_name',
			]);
		$category->module_curricular_pensum_number   = $request['moduleTitle'];
		$category->module_curricular_pensum_name   = $request['moduleDescription'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}
}
