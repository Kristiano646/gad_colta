<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class curricularController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\curricular::All();
		$curricularModule= \App\module::All();
		return view ('admin.curricular')
		->withManagement($managementArea)
		->withOption($curricularModule)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'curricularTitle' => 'required|max:100|:curriculum_program,curriculum_program_description',
			'curricularModule' => 'required:curriculum_program,curriculum_program_module',
		]);

		\App\curricular::create([
			'curriculum_program_description' => $request['curricularTitle'],
			'curriculum_program_module' => $request['curricularModule'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\curricular::find($request['categoryId']);

	/*	$this->validate($request,[

			'coordinatorName' => 'required|max:100|unique:coordinator_program,coordinator_program_name,'.$category->coordinator_program_id.',coordinator_program_id',
			'coordinatorLastName' => 'required|max:100|unique:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|unique:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);*/
		$category->curriculum_program_description = $request['curricularTitle'];
		$category->curriculum_program_module = $request['curricularModule'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa, coordinador ingresado con éxito.');

	}}