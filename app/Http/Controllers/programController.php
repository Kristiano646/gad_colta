<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class programController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\program::All();
        $curricular= \App\curricular::All();
        $coordinator= \App\coordinator::All();
        $catprog= \App\categoryProgram::All();
        $type= \App\programType::All();
		return view ('admin.program')
		->withManagement($managementArea)
		->withOption1($curricular)
		->withOption2($coordinator)
		->withOption3($catprog)
		->withOption4($type)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'programDescription' => 'required',
            'programType' => 'required',
			'programCategory' => 'required',
			'programObjective' => 'required',
			'programVersion' => 'required',
			'programDuration' => 'required',
			'programBegin' => 'required',
			'programEnd' => 'required',
			'programCoordinator' => 'required',
			'programCurriculum' => 'required',
		]);

		\App\program::create([
			'program_description' => $request['programDescription'],
            'program_type_program' => $request['programType'],
            'program_category_program' => $request['programCategory'],
            'program_objective' => $request['programObjective'],
            'program_version' => $request['programVersion'],
            'program_duration' => $request['programDuration'],
            'program_date_begin' => $request['programBegin'],
            'program_date_end' => $request['programEnd'],
            'program_coordinator_program' => $request['programCoordinator'],
			'program_curriculum_program' => $request['programCurriculum'],	
            'program_state' => $request['programState'],		
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\program::find($request['categoryId']);

	/*	$this->validate($request,[

			'coordinatorName' => 'required|max:100|unique:coordinator_program,coordinator_program_name,'.$category->coordinator_program_id.',coordinator_program_id',
			'coordinatorLastName' => 'required|max:100|unique:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|unique:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);*/
		$category->program_description = $request['programDescription'];
		$category->program_type_program = $request['programType'];
		$category->program_category_program = $request['programCategory'];
		$category->program_objective = $request['programObjective'];
		$category->program_version = $request['programVersion'];
		$category->program_duration = $request['programDuration'];
		$category->program_date_begin = $request['programBegin'];
		$category->program_date_end = $request['programEnd'];
		$category->program_coordinator_program = $request['programCoordinator'];
		$category->program_curriculum_program = $request['programCurriculum'];
		$category->program_state = $request['programState@'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa, coordinador ingresado con éxito.');

	}}