<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class coordinatorController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\coordinator::All();
		return view ('admin.coordinator')
		->withManagement($managementArea)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'coordinatorName' => 'required|max:100|:coordinator_program,coordinator_program_name',
			'coordinatorLastName' => 'required|max:100|:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);

		\App\coordinator::create([
			'coordinator_program_name' => $request['coordinatorName'],
			'coordinator_program_last_name' => $request['coordinatorLastName'],
			'coordinator_program_title' => $request['coordinatorTitle'],
			'coordinator_program_mail' => $request['coordinatorMail'],
			'coordinator_program_cellphone' => $request['coordinatorCellPhone'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\coordinator::find($request['categoryId']);

	/*	$this->validate($request,[

			'coordinatorName' => 'required|max:100|unique:coordinator_program,coordinator_program_name,'.$category->coordinator_program_id.',coordinator_program_id',
			'coordinatorLastName' => 'required|max:100|unique:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|unique:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);*/
		$category->coordinator_program_name = $request['coordinatorName'];
		$category->coordinator_program_last_name = $request['coordinatorLastName'];
		$category->coordinator_program_title = $request['coordinatorTitle'];
		$category->coordinator_program_mail = $request['coordinatorMail'];
		$category->coordinator_program_cellphone = $request['coordinatorCellPhone'];
		$category->coordinator_program_name = $request['coordinatorName'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa, coordinador ingresado con éxito.');

	}}