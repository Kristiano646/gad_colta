<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProyectosTypeController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\proyectosType::All();
		return view ('admin.ProyectosType')
		->withManagement($managementArea)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		// $this->validate($request,[

		// 	'proyectos_type_description' => 'required|max:100|unique:news_type,proyectos_type_description',

		// 	]);

		\App\proyectosType::create([
			'proyectos_type_description' => $request['proyectos_type_description'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\proyectosType::find($request['categoryId']);

		// $this->validate($request,[

		// 	'proyectos_type_description' => 'required|max:100|unique:news_type,proyectos_type_description,'.$category->proyectos_type_id.',proyectos_type_id',

		// 	]);
		$category->proyectos_type_description   = $request['proyectos_type_description'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}
}
