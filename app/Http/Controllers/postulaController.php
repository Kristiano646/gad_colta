<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class postulaController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
        $category = \App\postula::All();
        $program= \App\program::All();
        $socialNetwork = \App\socialNetworks::All();
	    $link = \App\link::All();
        
		return view ('/postula')
		->withManagement($managementArea)
        ->withCategory($category)
        ->withSocial($socialNetwork)
		->withOption2($program);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'postulantName' => 'required',
			'postulantProgram' => 'required',
			'postulantDocumentNumber' => 'required',
			'postulantIdDocument' => 'required',
			'postulantLastName' => 'required',
			'postulantAddress' => 'required',
			'postulantPhone' => 'required',
			'postulantMail' => 'required',
			'postulantNationality' => 'required',
			'postulantGenre' => 'required',
            ]);
    

		\App\postula::create([
			'postulant_document_number' => $request['postulantDocumentNumber'],
            'postulant_genre' => $request['postulantGenre'],
            'postulant_id_document' => $request['postulantIdDocument'],
            'postulant_name' => $request['postulantName'],
            'postulant_last_name' => $request['postulantLastName'],
            'postulant_mail' => $request['postulantMail'],
            'postulant_nationality' => $request['postulantNationality'],
            'postulant_phone' => $request['postulantPhone'],
			'postulant_program' => $request['postulantProgram'],
			'postulant_address' => $request['postulantAddress'],
            ]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\postula::find($request['categoryId']);
        /*
		$this->validate($request,[

			'rateTypeDescription' => 'required|max:100|unique:type_rate,type_rate_description,'.$category->type_rate_id.',type_rate_id',

            ]);*/
        
		$category ->postulant_document_number = $request['postulantDocumentNumber'];
        $category ->postulant_genre = $request['postulantGenre'];
        $category ->postulant_id_document= $request['postulantIdDocument'];
        $category ->postulant_name= $request['postulantName'];
        $category ->postulant_last_name= $request['postulantLastName'];
        $category ->postulant_mail= $request['postulantMail'];
        $category ->postulant_nationality= $request['postulantNationality'];
        $category ->postulant_phone= $request['postulantPhone'];
        $category ->postulant_program= $request['postulantProgram'];
        $category ->postulant_address= $request['postulantAddress'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}
}
