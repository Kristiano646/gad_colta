<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\news;
use App\multimedia;
use App\NewsTranslation;

class NewsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $urlServerNews = url()->current() . '/img/noticias/';
    $urlalias = url()->current() . '/noticia/';
    $newss = News::select('news_id', 'news_title', 'news_content', 'news_create', 'news_update')->where('news_type', '2')->where('news_state', 1)->get(); //->load('projects');
    foreach ($newss as $news) {
      $news->{'urlImage'} = $urlServerNews . Multimedia::where('multimedia_news', $news->news_id)->where('multimedia_type', 1)->value('multimedia_name');
      $news->{'url'} = $urlalias . News::where('news_id', $news->news_id)->value('news_id');
    }
    return response()->json($newss);
  }
}
