<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

class authorityEstadoController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$authorityTable = \App\authority::orderBy('authority_update','desc')->get();
		$authorityCargo =\App\cargo::All();
		$authorityType =\App\authorityType::All();
		$authorityEstado =\App\authorityEstado::All(); 
		return view ('admin.authority')
		->withManagement($managementArea)
		->withTypes($authorityType)
		->withCargo($authorityCargo)
		->withEstado($authorityEstado)
		->withAuthority($authorityTable);
	}

	public function store(Request $request)
	{

		

		\App\authorityEstado::create([
			'user_estado_description' => ucwords($request['authorityDescription']),
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');

	}

	public function update(Request $request){
		
		$category=\App\authorityEstado::find($request['categoryId']);
	
		$category->user_estado_description =ucwords($request['authorityDescription']); 
		

		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}

	public function delete (Request $request){
		try{
			$category=\App\authorityEstado::find($request['categoryId']);
			$category->delete();
			unset($request);
			unset($news);
			return back()->withMensaje('Operación Exitosa');

		}catch(Exception $e){
			return back()->withMensaje('Error en la operación');

		}

	}

}
