<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class categoryProgramController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\categoryProgram::All();
		return view ('admin.categoryProgram')
		->withManagement($managementArea)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'categoryProgramDescription' => 'required|max:100|unique:category_program,category_program_description',

			]);

		\App\categoryProgram::create([
			'category_program_description' => $request['categoryProgramDescription'],
			]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\categoryProgram::find($request['categoryId']);

		$this->validate($request,[

			'categoryProgramDescription' => 'required|max:100|unique:category_program,category_program_description,'.$category->category_program_id.',category_program_id',

			]);
		$category->category_program_description   = $request['categoryProgramDescription'];
		$category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa');

	}
}
