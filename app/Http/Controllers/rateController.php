<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class rateController extends Controller
{
	public function show()
	{
		$managementArea = \App\managementArea::firstOrFail();
		$category = \App\rate::All();
        $curricularModule= \App\program::All();
        $type= \App\rateType::All();
		return view ('admin.rate')
		->withManagement($managementArea)
        ->withOption1($curricularModule)
        ->withOption2($type)
		->withCategory($category);
	}

	public function store(Request $request)
	{
		$this->validate($request,[

			'rateDescription' => 'required|max:100|:rate_program,rate_program_description',
			'rateValue' => 'required:rate_program,rate_program_value',
    		'rateProgram' => 'required:rate_program,rate_program_program',
    		'rateType' => 'required:rate_program,rate_program_type_rate',   
            ]);

		\App\rate::create([
			'rate_program_description' => $request['rateDescription'],
			'rate_program_value' => $request['rateValue'],
            'rate_program_program' => $request['rateProgram'],
			'rate_program_type_rate' => $request['rateType'],
            ]);
		unset($request);

		return back()->withMensaje('Operación Exitosa');
	}

	public function update (Request $request){

		$category= \App\rate::find($request['categoryId']);

	/*	$this->validate($request,[

			'coordinatorName' => 'required|max:100|unique:coordinator_program,coordinator_program_name,'.$category->coordinator_program_id.',coordinator_program_id',
			'coordinatorLastName' => 'required|max:100|unique:coordinator_program,coordinator_program_last_name',
			'coordinatorTitle' => 'required|max:100|unique:coordinator_program,coordinator_program_title',
			'coordinatorMail' => 'required|max:100|unique:coordinator_program,coordinator_program_mail',
			'coordinatorCellPhone' => 'required|max:100|unique:coordinator_program,coordinator_program_cellphone',
			]);*/
		$category->rate_program_description = $request['rateDescription'];
		$category->rate_program_value = $request['rateValue'];
        $category->rate_program_program = $request['rateProgram'];
		$category->rate_program_type_rate = $request['rateType'];
        $category->save();	
		unset($request);
		unset($category);

		return back()->withMensaje('Operación Exitosa, coordinador ingresado con éxito.');

	}}