<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class rate extends Model
{
    protected $table = 'rate_program';
	protected $primaryKey = 'rate_program_id';

	protected $fillable = [
    'rate_program_description',
    'rate_program_value',
    'rate_program_program',
    'rate_program_type_rate',
	];
	public $timestamps = false;
}
