<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class desembolsos extends Model
{
    
	protected $table = 'desembolsos';
	protected $primaryKey = 'id_desembolsos';

	protected $fillable = [
    'fecha_desembolsos',
    'monto_desembolsos',
    'acumulado_desembolsos',
];
}