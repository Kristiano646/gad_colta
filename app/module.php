<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class module extends Model
{
    protected $table = 'module_curricular_pensum';
	protected $primaryKey = 'module_curricular_pensum_id';

	protected $fillable = [
    'module_curricular_pensum_name',
    'module_curricular_pensum_number',
	];

	public $timestamps = false;
}
