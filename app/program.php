<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class program extends Model
{
    protected $table = 'program';
	protected $primaryKey = 'program_id';

	protected $fillable = [
    'program_description',
    'program_type_program',
    'program_category_program',
    'program_objective',
    'program_version',
    'program_duration',
    'program_date_end',
    'program_date_begin',
    'program_coordinator_program',
    'program_curriculum_program',
    'program_state',
	];
	public $timestamps = false;
}
