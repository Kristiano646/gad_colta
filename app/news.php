<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news extends Model
{
	protected $table = 'news';
	protected $primaryKey = 'news_id';

	protected $fillable = [
	'news_title',
	'news_content',
	'news_state',
	'news_user',
	'news_alias',
	'news_create',
	'news_type',
	'news_management_area',
	'news_photo',
	];

	const CREATED_AT = 'news_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'news_update';

    protected $date =[
    'news_create',
    'news_update',
    ];

}
