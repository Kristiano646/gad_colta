<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comunidad extends Model
{
    
	protected $table = 'comunidad';
	protected $primaryKey = 'comunidad_id';

	protected $fillable = [

        'comunidad_id',
        'comunidad_name',
        'comunidad_ubicacion',
        'comunidad_type_sociedad',
        'comunidad_type_organizacion',
        'comunidad_type_suborganizacion',
        'comunidad_num_hom',
        'comunidad_num_muj',
        'comunidad_num_poblacion',
        'comunidad_num_familias',
        'comunidad_num_mestizos',
        'comunidad_num_indigenas',
        'comunidad_cont_name',
        'comunidad_cont_direccion',
        'comunidad_cont_tel',
        'comunidad_cont_email',
        'comunidad_ruc',
        'comunidad_name_banco',
        'comunidad_num_cuenta',
        'comunidad_type_cuenta',
        'comunidad_observaciones',
        'comunidad_create',
        'comunidad_update',
        'comunidad_description',
        'comunidad_state',
        'comunidad_parent'
	];
    

	const CREATED_AT = 'comunidad_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'comunidad_update';

    protected $date =[
    'comunidad_create',
    'comunidad_update',
    ];
}
