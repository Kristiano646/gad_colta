<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class convenio_desembolsos extends Model
{
    
	protected $table = 'convenios_desembolso';
	protected $primaryKey = 'id_convenios_desembolso';

	protected $fillable = [
    'id_convenios',
    'id_desembolso',
    
];
}