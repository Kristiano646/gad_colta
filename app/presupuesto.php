<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class presupuesto extends Model
{
	protected $table = 'presupuesto';
	protected $primaryKey = 'presupuesto_id';

	protected $fillable = [
	
    'presupuesto_monto',
    'presupuesto_acumulado',
    'presupuesto_observacion', 
    'presupuesto_estado',
	];

	const CREATED_AT = 'presupuesto_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'presupuesto_update';

    public $timestamps = false;


}
