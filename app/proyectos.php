<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyectos extends Model
{
	protected $table = 'proyectos';
	protected $primaryKey = 'proyectos_id';

	protected $fillable = [
	
	'proyectos_name',
	'proyectos_fechaini',
	'proyectos_fechafin',
	'proyectos_objetivo',
	'proyectos_numInf',
	'proyectos_nmeses',
	'proyectos_Cumrequisitos',
	'proyectos_estado',
    'type_id',
	];

	const CREATED_AT = 'proyectos_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'proyectos_update';

    protected $date =[
    'proyectos_create',
    'proyectos_update',
    ];
	public $timestamps = false;

}
