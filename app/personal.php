<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personal extends Model
{
	protected $table = 'personal';
	protected $primaryKey = 'personal_id';

	protected $fillable = [
	    'personal_name',
        'personal_cargo',
        'personal_tlf',
       
	];

	const CREATED_AT = 'personal_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'personal_update';

    protected $date =[
    'personal_create',
    'personal_update',
    ];

}
