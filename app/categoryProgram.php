<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoryProgram extends Model
{
    protected $table = 'category_program';
	protected $primaryKey = 'category_program_id';

	protected $fillable = [
	'category_program_description',
	];

	public $timestamps = false;
}
