<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class coordinator extends Model
{
    protected $table = 'coordinator_program';
	protected $primaryKey = 'coordinator_program_id';

	protected $fillable = [
    'coordinator_program_cellphone',
    'coordinator_program_last_name',
    'coordinator_program_mail',
    'coordinator_program_name',
    'coordinator_program_title',
	];
	public $timestamps = false;
}
