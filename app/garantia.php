<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class garantia extends Model
{
    protected $table = 'garantia';
	protected $primaryKey = 'garantia_id';

	protected $fillable = [

	'garantia_monto',
	'garantia_id_tipo',
	'garantia_fechven',
	'garantia_observacion',
	'garantia_proyecto_id',
	];

}
