<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class infoc2_comunidad extends Model
{
	protected $table = 'infoc2_comunidad';
	protected $primaryKey = 'infoc2_id';

	protected $fillable = [
	

    'infoc2_id_comunidad', 
	'infoc2_id_tipoinfcensal', 
    'infoc2_infocensalparam', 
    'infoc2_valor', 
            

    ];

}
