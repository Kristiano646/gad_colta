<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class infoc1_comunidad extends Model
{
	protected $table = 'infoc1_comunidad';
	protected $primaryKey = 'infoc1_id';

	protected $fillable = [
	

    'infoc1_id_comunidad', 
	'infoc1_id_tipoinfcensal', 
    'infoc1_infocensalparam', 
    'infoc1_valor', 
            

    ];

}
