<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class financiamiento_proyecto extends Model
{
	protected $table = 'financiamiento_proyecto';
	protected $primaryKey = 'financiamiento_proyecto_id';

	protected $fillable = [
        'financiamiento_id',
        'proyecto_id',
      
	];

	const CREATED_AT = 'financiamiento_proyecto_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'financiamiento_proyecto_update';

    protected $date =[
    'financiamiento_proyecto_create',
    'financiamiento_proyecto_update',
    ];
    public $timestamps = false;

}
