<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class encargados extends Model
{
    protected $table = 'personal';
	protected $primaryKey = 'personal_id';

	protected $fillable = [

	'personal_name',
	'personal_last_name',
	'personal_cv',
	'personal_cargo',
	'personal_ci',
    'personal_direccion',
    'personal_tlf',
    'personal_email',
	];

}
