<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class senso2 extends Model
{
	protected $table = 'infocenso2';
	protected $primaryKey = 'infocenso2_id';

	protected $fillable = [
	'infocenso2_id_Tipos',
	'infocenso2_description',
	];

	const CREATED_AT = 'infocenso2_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'infocenso2_update';

    protected $date =[
    'infocenso2_create',
    'infocenso2_update',
    ];

}
