<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;

class user extends Authenticatable
{
	protected $table = 'user';
	protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    	'user_name',
        'user_last_name',
        'user_cv',
        'user_photo',
        'user_create',
        'user_update',
        'user_type', 
        'user_estado', 
        'user_cargo_a',
        'user_management_area', 
        'user_ci',
        'user_direccion',
        'user_telefono',
        'user_mail',
        'user_observaciones',
        'remember_token',
        'password', 
    ];
    const CREATED_AT = 'user_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'user_update';

    protected $date =[
    'user_create',
    'user_update',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 
    'remember_token',
    ];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}