<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class convenio extends Model
{
    
	protected $table = 'convenios';
	protected $primaryKey = 'convenios_id';

	protected $fillable = [
    'convenios_id_proyecto',
    'convenios_fech_ini',
    'convenios_fech_final',
    'convenios_aporte_municipio',
    'convenios_aporte_comunidad',
    'convenios_sal',
    'convenios_id_encargado', 
    'convenios_management_area',
    'convenios_create',
    'convenios_update'
];
}