<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cargo extends Model
{
	protected $table = 'user_cargo';
	protected $primaryKey = 'user_cargo_id';

	protected $fillable = [
	'user_cargo_description',
	];

	public $timestamps = false;

}
