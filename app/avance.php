<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class avance extends Model
{
	protected $table = 'avance';
	protected $primaryKey = 'avance_id';

	protected $fillable = [
    'avance_proyecto_id',
	'avance_doc',
	'avance_fechainforme',
	'avance_observacion',
	'avance_estado',  
	];

	const CREATED_AT = 'avance_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'avance_update';

    protected $date =[
    'avance_create',
    'avance_update',
    ];

}
