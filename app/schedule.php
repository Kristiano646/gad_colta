<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class schedule extends Model
{
    protected $table = 'schedule_program';
	protected $primaryKey = 'schedule_program_id';

	protected $fillable = [
    'schedule_program_day',
    'schedule_program_hour_begin',
    'schedule_program_hour_end',
    'schedule_program_program',
	];
	public $timestamps = false;
}
