<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table = 'slider';
    protected $primaryKey = 'slider_id';

    protected $fillable = [
        'slider_id',
        'slider_name',
        'slider_description',
        'slider_image',
    ];


    const CREATED_AT = 'slider_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'slider_update';

    protected $date = [
        'slider_create',
        'slider_update',
    ];
}
