<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tiposenso extends Model
{
	protected $table = 'tiposinfocenso1';
	protected $primaryKey = 'tiposinfocenso';

	protected $fillable = [
	'Tiposinfocenso_description',
	];

	const CREATED_AT = 'tiposinfocenso_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'tiposinfocenso_update';

    protected $date =[
    'tiposinfocenso_create',
    'tiposinfocenso_update',
    ];

}
