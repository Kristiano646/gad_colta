<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class directivas extends Model
{
	protected $table = 'directiva';
	protected $primaryKey = 'directiva_id';

	protected $fillable = [
        'directiva_comunidad_id',
        'directiva_descripcion',
        'directiva_estado',

	
	];
	const CREATED_AT = 'directivas_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'directivas_update';

	public $timestamps = false;

}
