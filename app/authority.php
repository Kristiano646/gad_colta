<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class authority extends Model
{
    protected $table = 'user';
	protected $primaryKey = 'user_id';

	protected $fillable = [

	'user_name',
	'user_last_name',
	'user_cv',
	'user_management_area',
	'user_type',
    'user_estado',
	'user_photo',
    'user_ci',
    'user_direccion',
    'user_telefono',
    'user_email',
    'user_observaciones'
	];

	const CREATED_AT = 'user_create';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'user_update';

    protected $date =[
    'user_create',
    'user_update',
    ];
}
