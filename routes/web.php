<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//guest routes
//creacion grupo de rutas, con middleware por peticion, para instalación,

use App\Http\Controllers\comunidadController;

Route::group(['middleware' => ['guest', 'install']], function () {

	Route::get('acceso', ['as' => 'login', 'uses' => 'Auth\loginController@showLoginForm']);
	Route::post('acceso', ['as' => 'login', 'uses' => 'Auth\loginController@login']);
});

Route::group(['middleware' => 'register'], function () {

	Route::get('register', ['as' => 'register', 'uses' => 'Auth\loginController@showRegisterForm']);
	Route::post('register', ['as' => 'register', 'uses' => 'userController@store']);
});
//end guest routes

//routes for all users

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

	Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\loginController@logout']);




	//news routes
	Route::get('news', ['as' => 'news', 'uses' => 'newsController@show']);
	Route::get('newsData/{news}', ['as' => 'newsData', 'uses' => 'newsController@showData']);
	Route::Post('news', ['as' => 'news', 'uses' => 'newsController@store']);
	Route::post('deleteNews', ['as' => 'deleteNews', 'uses' => 'newsController@delete']);
	Route::post('updateNews', ['as' => 'updateNews', 'uses' => 'newsController@update']);
	Route::post('addResourcesNews', ['as' => 'addResourcesNews', 'uses' => 'newsController@addResource']);
	Route::post('deleteResourcesNews', ['as' => 'deleteResourcesNews', 'uses' => 'newsController@deleteResource']);
	//end news routes

	//gallery routes
	Route::get('gallery', ['as' => 'gallery', 'uses' => 'galleryController@show']);
	Route::get('galleryData/{gallery}', ['as' => 'galleryData', 'uses' => 'galleryController@showData']);
	Route::Post('gallery', ['as' => 'gallery', 'uses' => 'galleryController@store']);
	Route::post('deleteGallery', ['as' => 'deleteGallery', 'uses' => 'galleryController@delete']);
	Route::post('updateGallery', ['as' => 'updateGallery', 'uses' => 'galleryController@update']);
	Route::post('addResourcesGallery', ['as' => 'addResourcesGallery', 'uses' => 'galleryController@addResource']);
	Route::post('deleteResourcesGallery', ['as' => 'deleteResourcesGallery', 'uses' => 'galleryController@deleteResource']);
	//end gallery routes

	//authority routes
	Route::get('user', ['as' => 'user', 'uses' => 'userController@show']);
	Route::get('newUser', ['as' => 'newUser', 'uses' => 'userController@showInsertForm']);
	Route::post('insertUser', ['as' => 'insertUser', 'uses' => 'userController@create']);
	Route::post('updateUser', ['as' => 'updateUser', 'uses' => 'userController@update']);
	Route::post('updatePassword', ['as' => 'updatePassword', 'uses' => 'userController@updatePassword']);
	//end authority routes

});

//slider routes
Route::get('slider', ['as' => 'slider', 'uses' => 'sliderController@show']);
Route::get('sliderData/{slider}', ['as' => 'sliderData', 'uses' => 'sliderController@showData']);
Route::Post('slider', ['as' => 'slider', 'uses' => 'sliderController@store']);
Route::post('deleteSlider', ['as' => 'deleteSlider', 'uses' => 'sliderController@delete']);
Route::post('updateSlider', ['as' => 'updateSlider', 'uses' => 'sliderController@update']);

//en slider routes
// end routes for all users

//routes for user cimogsys and vinculacion

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'vin']], function () {

	//comunidad routes
	Route::get('comunidad', ['as' => 'comunidad', 'uses' => 'comunidadController@show']);
	Route::Post('comunidad', ['as' => 'comunidad', 'uses' => 'comunidadController@store']);
	Route::post('deleteComunidad', ['as' => 'deleteComunidad', 'uses' => 'comunidadController@delete']);
	Route::post('updateComunidad', ['as' => 'updateComunidad', 'uses' => 'comunidadController@update']);

	//downloads
	Route::get('downloadComunidad', ['as' => 'downloadComunidad', 'uses' => 'comunidadController@download']);
	Route::get('downloaddirec', ['as' => 'downloaddirec', 'uses' => 'comunidadController@downloaddirectiva']);
	Route::get('downloadProyectos', ['as' => 'downloadProyectos', 'uses' => 'ProyectosController@download']);
	Route::get('downloadProyectosid', ['as' => 'downloadProyectosid', 'uses' => 'ProyectosController@downloadid']);
	//end comunidad routes

	//link routes
	Route::get('link', ['as' => 'link', 'uses' => 'linkController@show']);
	Route::Post('link', ['as' => 'link', 'uses' => 'linkController@store']);
	Route::post('deleteLink', ['as' => 'deleteLink', 'uses' => 'linkController@delete']);
	Route::post('updateLink', ['as' => 'updateLink', 'uses' => 'linkController@update']);
	//end link routes

	//faculty routes
	Route::get('faculty', ['as' => 'axes', 'uses' => 'axesController@showFaculty']);
	Route::Post('faculty', ['as' => 'faculty', 'uses' => 'axesController@storeFaculty']);
	Route::post('deleteFaculty', ['as' => 'deleteFaculty', 'uses' => 'axesController@deleteFaculty']);
	Route::post('updateFaculty', ['as' => 'updateFaculty', 'uses' => 'axesController@updateFaculty']);

	//end faculty routes

	//conventions routes
	Route::get('conventions', ['as' => 'conventions', 'uses' => 'axesController@showConventions']);
	Route::Post('conventions', ['as' => 'conventions', 'uses' => 'axesController@storeConventions']);
	Route::post('deleteConventions', ['as' => 'deleteConventions', 'uses' => 'axesController@deleteConventions']);
	Route::post('updateConventions', ['as' => 'updateConventions', 'uses' => 'axesController@updateConventions']);

	//end conventions routes

	//facultyNews routes
	Route::get('facultyNews', ['as' => 'facultyNews', 'uses' => 'axesController@showFacultyNews']);
	Route::Post('facultyNews', ['as' => 'facultyNews', 'uses' => 'axesController@storeFacultyNews']);
	Route::post('deleteFacultyNews', ['as' => 'deleteFacultyNews', 'uses' => 'axesController@deleteFacultyNews']);
	Route::post('updateFacultyNews', ['as' => 'updateFacultyNews', 'uses' => 'axesController@updateFacultyNews']);
	Route::get('facultyData/{faculty}', ['as' => 'facultyData', 'uses' => 'axesController@showData']);

	//end facultyNews routes

	// culturalManagement routes
	Route::get('culturalManagement', ['as' => 'culturalManagement', 'uses' => 'culturalManagementController@show']);
	Route::get('culturalManagementData/{culturalManagement}', ['as' => 'culturalManagementData', 'uses' => 'culturalManagementController@showData']);
	Route::Post('culturalManagement', ['as' => 'culturalManagement', 'uses' => 'culturalManagementController@store']);
	Route::post('deleteCulturalManagement', ['as' => 'deleteCulturalManagement', 'uses' => 'culturalManagementController@delete']);
	Route::post('updateCulturalManagement', ['as' => 'updateCulturalManagement', 'uses' => 'culturalManagementController@update']);
	//end culturalManagement routes

	// culturalManagementTypes routes
	Route::get('culturalManagementTypes', ['as' => 'culturalManagementTypes', 'uses' => 'culturalManagementTypesController@show']);
	Route::get('culturalManagementTypesData/{culturalManagement}', ['as' => 'culturalManagementTypesData', 'uses' => 'culturalManagementTypesController@showData']);
	Route::Post('culturalManagementTypes', ['as' => 'culturalManagementTypes', 'uses' => 'culturalManagementTypesController@store']);
	Route::post('deleteCulturalManagementTypes', ['as' => 'deleteCulturalManagementTypes', 'uses' => 'culturalManagementTypesController@delete']);
	Route::post('updateCulturalManagementTypes', ['as' => 'updateCulturalManagementTypes', 'uses' => 'culturalManagementTypesController@update']);
	Route::post('addResourcesCulturalManagementTypes', ['as' => 'addResourcesCulturalManagementTypes', 'uses' => 'culturalManagementTypesController@addResource']);
	Route::post('deleteResourcesCulturalManagementTypes', ['as' => 'deleteResourcesCulturalManagementTypes', 'uses' => 'culturalManagementTypesController@deleteResource']);
	//end culturalManagement routes

	//download routes
	Route::get('download', ['as' => 'download', 'uses' => 'downloadController@show']);
	Route::get('downloadData/{download}', ['as' => 'downloadData', 'uses' => 'downloadController@showData']);
	Route::Post('download', ['as' => 'download', 'uses' => 'downloadController@store']);
	Route::post('deleteDownload', ['as' => 'deleteDownload', 'uses' => 'downloadController@delete']);
	Route::post('updateDownload', ['as' => 'updateDownload', 'uses' => 'downloadController@update']);
	Route::post('addResourceDownload', ['as' => 'addResourceDownload', 'uses' => 'downloadController@addResource']);
	Route::post('deleteResourcesDownload', ['as' => 'deleteResourcesDownload', 'uses' => 'downloadController@deleteResource']);
	//end download routes

	//socialNetworks routes
	Route::get('socialNetwork', ['as' => 'socialNetwork', 'uses' => 'socialNetworkController@show']);
	Route::get('socialNetworkData/{socialNetwork}', ['as' => 'socialNetworkData', 'uses' => 'socialNetworkController@showData']);
	Route::Post('socialNetwork', ['as' => 'socialNetwork', 'uses' => 'socialNetworkController@store']);
	Route::post('deleteSocialNetwork', ['as' => 'deleteSocialNetwork', 'uses' => 'socialNetworkController@delete']);
	Route::post('updateSocialNetwork', ['as' => 'updateSocialNetwork', 'uses' => 'socialNetworkController@update']);

	//magazine routes
	Route::get('magazines', ['as' => 'magazines', 'uses' => 'magazineController@show']);
	Route::get('magazineData/{magazine}', ['as' => 'magazineData', 'uses' => 'magazineController@showData']);
	Route::Post('magazine', ['as' => 'magazine', 'uses' => 'magazineController@store']);
	Route::post('deleteMagazine', ['as' => 'deleteMagazine', 'uses' => 'magazineController@delete']);
	Route::post('updateMagazine', ['as' => 'updateMagazine', 'uses' => 'magazineController@update']);
	//end magazine routes

	//authority routes
	Route::get('authority', ['as' => 'authority', 'uses' => 'authorityController@show']);
	Route::get('newAuthority', ['as' => 'newAuthority', 'uses' => 'authorityController@showInsert']);
	Route::post('insertAuthority', ['as' => 'insertAuthority', 'uses' => 'authorityController@store']);
	Route::post('updateAuthority', ['as' => 'updateAuthority', 'uses' => 'authorityController@update']);
	Route::post('deleteAuthority', ['as' => 'deleteAuthority', 'uses' => 'authorityController@delete']);
	//end authority routes

	//managementArea routes
	Route::get('mission', ['as' => 'mission', 'uses' => 'adminController@showMission']);
	Route::post('mission', ['as' => 'mission', 'uses' => 'adminController@updateMission']);
	Route::get('objective', ['as' => 'objective', 'uses' => 'adminController@showObjective']);
	Route::post('objective', ['as' => 'objective', 'uses' => 'adminController@updateObjective']);
	Route::get('direction', ['as' => 'direction', 'uses' => 'adminController@showDirection']);
	Route::post('direction', ['as' => 'direction', 'uses' => 'adminController@updateDirection']);
	Route::get('functions', ['as' => 'functions', 'uses' => 'adminController@showFunctions']);
	Route::post('functions', ['as' => 'functions', 'uses' => 'adminController@updateFunctions']);
	//end managementArea routes



});
//end routes for user cimogsys and vinculacion

//routes for user cimogsys

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'root']], function () {

	//home routes
	Route::get('inicio', ['as' => 'inicio', 'uses' => 'adminController@show']);
	Route::post('inicio', ['as' => 'inicio', 'uses' => 'adminController@update']);
	Route::get('parameterization', ['as' => 'parameterization', 'uses' => 'adminController@showParameterization']);
	Route::get('encargados', ['as' => 'encargados', 'uses' => 'ProyectosController@showEncargados']);
	Route::post('deleteEncargado', ['as' => 'deleteEncargado', 'uses' => 'ProyectosController@dltEncargados']);
	Route::post('encargadosadd', ['as' => 'encargadosadd', 'uses' => 'ProyectosController@addEncargados']);
	Route::post('encargadosact', ['as' => 'encargadosact', 'uses' => 'ProyectosController@actEncargados']);
	Route::get('avance', ['as' => 'avance', 'uses' => 'ProyectosController@avance']);
	Route::post('addavance', ['as' => 'addavance', 'uses' => 'ProyectosController@addavance']);
	Route::post('dltavance', ['as' => 'dltavance', 'uses' => 'ProyectosController@deleteavance']);
	Route::post('actavance', ['as' => 'actavance', 'uses' => 'ProyectosController@actavance']);
	Route::get('convenio', ['as' => 'convenio', 'uses' => 'ConvenioController@showConvenio']);
	Route::post('addconvenio', ['as' => 'addconvenio', 'uses' => 'ConvenioController@addConvenio']);
	Route::get('convenioType', ['as' => 'convenioType', 'uses' => 'ConvenioController@show']);
	Route::post('addgarantiaType', ['as' => 'addgarantiaType', 'uses' => 'ConvenioController@addgarantia']);
	Route::post('deleteGarantiaType', ['as' => 'deleteGarantiaType', 'uses' => 'ConvenioController@dltgarantia']);
	Route::post('updateGType', ['as' => 'updateGType', 'uses' => 'ConvenioController@uptgarantia']);
	
	Route::get('proyectos', ['as' => 'proyectos', 'uses' => 'ProyectosController@showProyectos']);
	Route::post('proyectosadd', ['as' => 'proyectosadd', 'uses' => 'ProyectosController@addProyectos']);
	Route::post('proyectosupd', ['as' => 'proyectosupd', 'uses' => 'ProyectosController@updateProyectos']);
	Route::post('proyectosdlt', ['as' => 'proyectosdlt', 'uses' => 'ProyectosController@deleteProyectos']);
    Route::get('senso1', ['as' => 'senso1', 'uses' => 'SensosController@showSenso1']);
	Route::get('senso2', ['as' => 'senso2', 'uses' => 'SensosController@showSenso2']);
	Route::post('sensoing1', ['as' => 'sensoing1', 'uses' => 'SensosController@Sensoadd1']);
	Route::post('sensoupd1', ['as' => 'sensoupd1', 'uses' => 'SensosController@Sensoupd1']);
	Route::post('sensodlt1', ['as' => 'sensodlt1', 'uses' => 'SensosController@SensoDlt1']);
	Route::post('sensoing2', ['as' => 'sensoing2', 'uses' => 'SensosController@Sensoadd2']);
	Route::post('sensoupd2', ['as' => 'sensoupd2', 'uses' => 'SensosController@Sensoupd2']);
	Route::post('sensodlt2', ['as' => 'sensodlt2', 'uses' => 'SensosController@SensoDlt2']);
	Route::post('tipo1',['as' => 'tipo1', 'uses' => 'SensosController@showtipo1']);
	Route::post('tipo2',['as' => 'tipo2', 'uses' => 'SensosController@showtipo2']);
	 
	//end home routes


	//parameterization routes

	//comunidad rutas

	
	Route::post('miembrosing', ['as' => 'miembrosing', 'uses' => 'comunidadController@storeMiembros']);
	Route::post('directivasing', ['as' => 'directivasing', 'uses' => 'comunidadController@storeDirectivas']);
	Route::post('directivasupd', ['as' => 'directivasupd', 'uses' => 'comunidadController@updateDirectivas']);
	Route::post('directivasdel', ['as' => 'directivasdel', 'uses' => 'comunidadController@deleteDirectivas']);
	

	
	Route::get('directivas', ['as' => 'directivas', 'uses' => 'comunidadController@showDirectivas']);
	Route::post('datatable', ['as' => 'datatable', 'uses' => 'comunidadController@showdatatable']);
	Route::get('infosenso1', ['as' => 'infosenso1', 'uses' => 'infosensalController@showinc1']);
	Route::post('addinfosen1', ['as' => 'addinfosen1', 'uses' => 'infosensalController@addinfosen1']);
	Route::post('enviarinf1', ['as' => 'enviarinf1', 'uses' => 'infosensalController@enviarBD']);
	Route::get('datacensos', ['as' => 'datacensos', 'uses' => 'infosensalController@showdata']);
	//infocensal2
	Route::get('infosenso2', ['as' => 'infosenso2', 'uses' => 'infosensalController@showinc2']);
	Route::post('addinfosen2', ['as' => 'addinfosen2', 'uses' => 'infosensalController@addinfosen2']);
	Route::post('enviarinf2', ['as' => 'enviarinf2', 'uses' => 'infosensalController@enviarBD2']);
	Route::get('datacensos', ['as' => 'datacensos', 'uses' => 'infosensalController@showdata']);
	//banco de Datos
	Route::get('bcodatos', ['as' => 'bcodatos', 'uses' => 'comunidadController@showbd']);

	//fin

	Route::get('userType', ['as' => 'userType', 'uses' => 'userTypeController@show']);
	Route::post('userType', ['as' => 'userType', 'uses' => 'userTypeController@store']);
	Route::post('updateUserType', ['as' => 'updateUserType', 'uses' => 'userTypeController@update']);

	Route::get('authorityType', ['as' => 'authorityType', 'uses' => 'authorityTypeController@show']);
	Route::post('authorityType', ['as' => 'authorityType', 'uses' => 'authorityTypeController@store']);
	Route::post('updateAuthorityType', ['as' => 'updateAuthorityType', 'uses' => 'authorityTypeController@update']);
	Route::post('deleteAuthorityType', ['as' => 'deleteAuthorityType', 'uses' => 'authorityTypeController@delete']);

	Route::get('authorityEstado', ['as' => 'authorityEstado', 'uses' => 'authorityEstadoController@show']);
	Route::post('authorityEstado', ['as' => 'authorityEstado', 'uses' => 'authorityEstadoController@store']);
	Route::post('updateAuthorityEstado', ['as' => 'updateAuthorityEstado', 'uses' => 'authorityEstadoController@update']);
	Route::post('deleteAuthorityEstado', ['as' => 'deleteAuthorityEstado', 'uses' => 'authorityEstadoController@delete']);

	Route::get('multimediaType', ['as' => 'multimediaType', 'uses' => 'multimediaTypeController@show']);
	Route::post('multimediaType', ['as' => 'multimediaType', 'uses' => 'multimediaTypeController@store']);
	Route::post('updateMultimediaType', ['as' => 'updateMultimediaType', 'uses' => 'multimediaTypeController@update']);


	Route::get('newsType', ['as' => 'newsType', 'uses' => 'newsTypeController@show']);
	Route::post('newsType', ['as' => 'newsType', 'uses' => 'newsTypeController@store']);
	Route::post('updateNewsType', ['as' => 'updateNewsType', 'uses' => 'newsTypeController@update']);

	Route::get('programType', ['as' => 'programType', 'uses' => 'programTypeController@show']);
	Route::post('programType', ['as' => 'programType', 'uses' => 'programTypeController@store']);
	Route::post('updateProgramType', ['as' => 'updateProgramType', 'uses' => 'programTypeController@update']);

	Route::get('categoryProgram', ['as' => 'categoryProgram', 'uses' => 'categoryProgramController@show']);
	Route::post('categoryProgram', ['as' => 'categoryProgram', 'uses' => 'categoryProgramController@store']);
	Route::post('updateCategoryProgram', ['as' => 'updateCategoryProgram', 'uses' => 'categoryProgramController@update']);

	Route::get('rateType', ['as' => 'rateType', 'uses' => 'rateTypeController@show']);
	Route::post('rateType', ['as' => 'rateType', 'uses' => 'rateTypeController@store']);
	Route::post('updateRateType', ['as' => 'updateRateType', 'uses' => 'updateRateTypeController@update']);

	Route::get('proyectosType', ['as' => 'proyectosType', 'uses' => 'ProyectosTypeController@show']);
	Route::post('addproyectosType', ['as' => 'addproyectosType', 'uses' => 'ProyectosTypeController@store']);
	Route::post('updateproyectosType', ['as' => 'updateproyectosType', 'uses' => 'ProyectosTypeController@update']);
	Route::post('deleteproyectosType', ['as' => 'deleteproyectosType', 'uses' => 'ProyectosTypeController@delete']);


	//routes for coordinator
	Route::get('coordinator', ['as' => 'coordinator', 'uses' => 'coordinatorController@show']);
	Route::post('coordinator', ['as' => 'coordinator', 'uses' => 'coordinatorController@store']);
	Route::post('updateCoordinator', ['as' => 'updateCoordinator', 'uses' => 'coordinatorController@update']);

	//routes for modules/curricular
	Route::get('module', ['as' => 'module', 'uses' => 'moduleController@show']);
	Route::post('module', ['as' => 'module', 'uses' => 'moduleController@store']);
	Route::post('updateModule', ['as' => 'updateModule', 'uses' => 'moduleController@update']);

	Route::get('curricular', ['as' => 'curricular', 'uses' => 'curricularController@show']);
	Route::post('curricular', ['as' => 'curricular', 'uses' => 'curricularController@store']);
	Route::post('updateCurricular', ['as' => 'updateCurricular', 'uses' => 'curricularController@update']);


	Route::get('program', ['as' => 'program', 'uses' => 'programController@show']);
	Route::post('program', ['as' => 'program', 'uses' => 'programController@store']);
	Route::post('updateProgram', ['as' => 'updateProgram', 'uses' => 'programController@update']);

	Route::get('requirements', ['as' => 'requirements', 'uses' => 'requirementsController@show']);
	Route::post('requirements', ['as' => 'requirements', 'uses' => 'requirementsController@store']);
	Route::post('updateRequirements', ['as' => 'updateRequirements', 'uses' => 'requirementsController@update']);

	Route::get('schedule', ['as' => 'schedule', 'uses' => 'scheduleController@show']);
	Route::post('schedule', ['as' => 'schedule', 'uses' => 'scheduleController@store']);
	Route::post('updateSchedule', ['as' => 'updateSchedule', 'uses' => 'scheduleController@update']);

	Route::get('rate', ['as' => 'rate', 'uses' => 'rateController@show']);
	Route::post('rate', ['as' => 'rate', 'uses' => 'rateController@store']);
	Route::post('updateRate', ['as' => 'updateRate', 'uses' => 'rateController@update']);

	//end parameterization routes

});
//end routes for user cimogsys


Route::get('/', function () {
	//cambie a que se muestren noticias, no eventos
	$managementArea = DB::table('management_area')
		->first();

	$news = DB::table('news')
		->leftJoin('multimedia', 'news.news_id', '=', 'multimedia.multimedia_news')
		->select('news.news_id', 'news.news_state', 'news.news_title', 'news.news_content', 'multimedia.multimedia_name', 'news.news_alias')
		->where('news.news_state', 1)
		->where('news.news_type', 2)
		->groupBy('multimedia.multimedia_news')
		->orderBy('news.news_id', 'desc')
		->limit(2)
		->get();

	$authoritys = DB::table('user')
		->leftJoin('user_type', 'user.user_type', '=', 'user_type.user_type_id')
		->select('user.user_id', 'user.user_name', 'user.user_last_name', 'user.user_cv', 'user.user_photo', 'user_type.user_type_description')
		->orderBy('user.user_id', 'asc')
		->get();

	$downloads = App\download::where('download_state', 1)
		->get();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();

	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	$slider = App\slider::all();

	return view('inicio')
		->withManagement($managementArea)
		->withAuthoritys($authoritys)
		->withDownloads($downloads)
		->withSocial($social)
		->withCategory($comunidad)
		->withSlider($slider)
		->withNews($news);
});

Route::get('/mision', function () {

	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	return view('mision')
		->withSocial($social)
		->withCategory($comunidad)
		->withManagement($managementArea);
});



Route::get('/facultad', function () {
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();

	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	$faculty = DB::table('faculty')
		->where('faculty_state', 1)
		->get();

	return view('facultades')
		->withSocial($social)
		->withFaculty($faculty)
		->withCategory($comunidad)
		->withManagement($managementArea);
});

Route::get('/facultad/{id}', function ($id) {


	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();

	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	$facultyNews = App\facultyNews::where('faculty_news_faculty', $id)
		->where('faculty_news_state', 1)
		->paginate(5);

	$faculty = DB::table('faculty')
		->where('faculty_id', $id)
		->get();

	return view('facultadProyectos')
		->withSocial($social)
		->withNews($facultyNews)
		->withFaculty($faculty)
		->withCategory($comunidad)
		->withManagement($managementArea);
});

Route::get('/facultad/proyecto/{id}', function ($id) {

	if (isset($id)) {
		$managementArea = DB::table('management_area')
			->first();

		$social = DB::table('social_network')
			->where('social_network_state', 1)
			->get();

		$comunidad = DB::table('link')
			->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
			->where('link_state', 1)
			->orderBy('link.link_id', 'asc')
			->get();


		$facultyNews = App\facultyNews::where('faculty_news_id', $id)
			->where('faculty_news_state', 1)
			->first();

		return view('facultadProyecto')
			->withSocial($social)
			->withNews($facultyNews)
			->withCategory($comunidad)
			->withManagement($managementArea);
	} else {
		abort(404);
	}
});

Route::get('/informes/anuales', function () {
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	$conventions = App\conventions::where('conventions_state', 1)
		->where('conventions_type', '=', 'Informe Anual')
		->get();


	return view('informesAnuales')
		->withSocial($social)
		->withCategory($comunidad)
		->withConventios($conventions)
		->withManagement($managementArea);
});

Route::get('/informes/convenios', function () {
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();

	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	$conventions = App\conventions::where('conventions_state', 1)
		->where('conventions_type', '=', 'Convenio')
		->paginate(8);


	return view('informesConvenios')
		->withSocial($social)
		->withCategory($comunidad)
		->withConventios($conventions)
		->withManagement($managementArea);
});

Route::get('/reglamentos-formatos', function () {
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();

	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	$conventions = App\conventions::where('conventions_state', 1)
		->where('conventions_type', '=', 'Reglamento y Formato')
		->get();


	return view('reglamentosFormatos')
		->withSocial($social)
		->withCategory($comunidad)
		->withConventios($conventions)
		->withManagement($managementArea);
});



Route::get('/objetivos', function () {

	$managementArea = DB::table('management_area')
		->first();
	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	return view('objetivos')
		->withSocial($social)->withCategory($comunidad)

		->withManagement($managementArea);
});

Route::get('/funciones', function () {

	$managementArea = DB::table('management_area')
		->first();
	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	return view('funciones')
		->withSocial($social)->withCategory($comunidad)

		->withManagement($managementArea);
});

Route::get('/proyectos', function () {
	//aqui realice cambio 1 es notiia y 2 es eventos
	//se ordene descendentemente
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	$news = DB::table('news')
		->leftJoin('multimedia', 'news.news_id', '=', 'multimedia.multimedia_news')
		->select('news.news_id', 'news.news_state', 'news.news_title', 'news.news_content', 'multimedia.multimedia_name', 'news.news_alias')
		->where('news.news_state', 1)
		->where('news.news_type', 1)
		->groupBy('multimedia.multimedia_news')
		->orderBy('news.news_id', 'desc')
		->paginate(2);

	return view('proyecto')
		->withManagement($managementArea)
		->withSocial($social)->withCategory($comunidad)

		->withProjects($news);
});

Route::get('/proyecto/{id}', function () {
	//se muestre descendentemente
	if (isset($id)) {
		$projects = App\projects::where('projects_id', $id)->first();

		$managementArea = DB::table('management_area')
			->first();
		$comunidad = DB::table('link')
			->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
			->where('link_state', 1)
			->orderBy('link.link_id', 'asc')
			->get();
		$social = DB::table('social_network')
			->where('social_network_state', 1)
			->get();

		return view('proyecto')
			->withGallery($projects)
			->withSocial($social)->withCategory($comunidad)

			->withManagement($managementArea);
	} else {
		abort(404);
	}
});

Route::get('/formacion-grupos', function () {
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	$culturalManagement = DB::table('cultural_management')
		->where('cultural_management_state', 1)
		->get();
	return view('formacion')
		->withManagement($managementArea)
		->withSocial($social)->withCategory($comunidad)

		->withCultural($culturalManagement);
});
Route::get('/quienes-somos', function () {
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	$culturalManagement = DB::table('cultural_management')
		->where('cultural_management_state', 1)
		->get();
	return view('quienesSomos')
		->withManagement($managementArea)
		->withSocial($social)
		->withCategory($comunidad)
		->withCultural($culturalManagement);
});

Route::get('/formacion/{id}', function ($id) {

	if (isset($id)) {

		$culturalManagement = DB::table('cultural_management_types')
			->leftJoin('multimedia', 'cultural_management_types.cultural_management_types_id', '=', 'multimedia.multimedia_cultural_management_types')
			->select('cultural_management_types.cultural_management_types_id', 'cultural_management_types.cultural_management_types_state', 'cultural_management_types.cultural_management_types_name', 'cultural_management_types.cultural_management_types_description', 'multimedia.multimedia_name')
			->where('cultural_management_types_area', $id)
			->where('cultural_management_types.cultural_management_types_state', 1)
			->groupBy('multimedia.multimedia_cultural_management_types')
			->orderBy('cultural_management_types.cultural_management_types_id', 'desc')
			->paginate(3);

		$name = DB::table('cultural_management')
			->where('cultural_management_id', $id)
			->first();

		$managementArea = DB::table('management_area')
			->first();
		$comunidad = DB::table('link')
			->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
			->where('link_state', 1)
			->orderBy('link.link_id', 'asc')
			->get();
		$social = DB::table('social_network')
			->where('social_network_state', 1)
			->get();

		return view('formaciones')
			->withCultural($culturalManagement)
			->withName($name)
			->withSocial($social)->withCategory($comunidad)

			->withManagement($managementArea);
	} else {
		abort(404);
	}
});

Route::get('/noticias', function () {
	//cambio el tipo de noticia de 1 que es evento a 2 que es noticia
	//se muestre descendentemente
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	$news = DB::table('news')
		//->leftJoin('multimedia','multimedia.multimedia_id','=','news.news_id')
		->select('news.news_id', 'news.news_state', 'news.news_title', 'news.news_content', 'news.news_photo', 'news.news_alias')
		->where('news.news_state', '1')
		//esta linea agrupa
		//->groupBy('news.news_id','desc')
		->orderBy('news.news_id', 'desc')
		->paginate(10);


	return view('noticias')
		->withManagement($managementArea)
		->withSocial($social)->withCategory($comunidad)
		->withPrincipal($news[0])
		->withNews($news);
});

Route::get('/noticia/{id}', function ($id) {

	if (isset($id)) {
		$news = App\news::where('news_id', $id)->first();
		$multimedia = App\multimedia::where('multimedia_news', $news->news_id)
			->orderBy('multimedia_type', 'desc')
			->get();
		$image = "";
		foreach ($multimedia as $data) {
			if ($data->multimedia_type == 1) {
				$image = $data->multimedia_name;
			}
		}

		$managementArea = DB::table('management_area')
			->first();

		$social = DB::table('social_network')
			->where('social_network_state', 1)
			->get();
		$comunidad = DB::table('link')
			->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
			->where('link_state', 1)
			->orderBy('link.link_id', 'asc')
			->get();
		return view('noticia')
			->withNews($news)
			->withMultimedia($multimedia)
			->withSocial($social)
			->withCategory($comunidad)
			->withImage($image)
			->withManagement($managementArea);
	} else {
		abort(404);
	}
});


Route::get('/galerias', function () {

	$managementArea = DB::table('management_area')
		->first();

	$gallery = DB::table('gallery')
		->leftJoin('multimedia', 'gallery.gallery_id', '=', 'multimedia.multimedia_gallery')
		->select('gallery.gallery_id', 'gallery.gallery_state', 'gallery.gallery_name', 'multimedia.multimedia_name')
		->where('gallery.gallery_state', 1)
		->groupBy('multimedia.multimedia_gallery')
		->orderBy('gallery.gallery_id', 'desc')
		->paginate(2);
	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();


	return view('galerias')
		->withManagement($managementArea)
		->withSocial($social)
		->withCategory($comunidad)
		->withGallery($gallery);
});

Route::get('/galeria/{id}', function ($id) {

	if (isset($id)) {
		$gallery = App\gallery::where('gallery_id', $id)->first();
		$multimedia = App\multimedia::where('multimedia_gallery', $gallery->gallery_id)
			->orderBy('multimedia_type', 'desc')
			->get();
		$managementArea = DB::table('management_area')
			->first();

		$social = DB::table('social_network')
			->where('social_network_state', 1)
			->get();
		$comunidad = DB::table('link')
			->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
			->where('link_state', 1)
			->orderBy('link.link_id', 'asc')
			->get();
		return view('galeria')
			->withGallery($gallery)
			->withMultimedia($multimedia)
			->withSocial($social)->withCategory($comunidad)

			->withManagement($managementArea);
	} else {
		abort(404);
	}
});


Route::get('/contactos', function () {
	$managementArea = DB::table('management_area')
		->first();
	$socialNetwork = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	return view('contactos')
		->withManagement($managementArea)->withCategory($comunidad)

		->withSocial($socialNetwork);
});

//RUTA MOSTRAR COORDINADORES DE MAESTRIA
Route::get('/mostrarCoordinadores', function () {
	$coordinator = DB::table('coordinator_program')
		->orderBy('coordinator_program.coordinator_program_id', 'asc')
		->get();
	$managementArea = DB::table('management_area')
		->first();
	$socialNetwork = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	return view('mostrarCoordinadores')
		->withManagement($managementArea)
		->withCategory($comunidad)
		->withSocial($socialNetwork)
		->withCoordinator($coordinator);
});

//RUTA MOSTRAR MAESTRIA
Route::get('/maestrias', function () {
	$program = DB::table('program')
		->orderBy('program.program_id', 'asc')
		->get();
	$coordinator = DB::table('coordinator_program')
		->orderBy('coordinator_program.coordinator_program_id', 'asc')
		->get();
	$curricular = DB::table('curriculum_program')
		->orderBy('curriculum_program.curriculum_program_id', 'asc')
		->get();
	$cat = DB::table('category_program')
		->get();
	$type = DB::table('type_program')
		->orderBy('type_program.type_program_id', 'asc')
		->get();
	$managementArea = DB::table('management_area')
		->first();
	$socialNetwork = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	return view('maestrias')
		->withManagement($managementArea)
		->withCategory($comunidad)
		->withSocial($socialNetwork)
		->withProgram($program)
		->withCoordinator($coordinator)
		->withCurricular($curricular)
		->withCat($cat)
		->withType($type);
});


//RUTA INGRESAR DATOS DEL POSTULANTE
Route::get('postula', ['as' => 'postula', 'uses' => 'postulaController@show']);
Route::post('postula', ['as' => 'postula', 'uses' => 'postulaController@store']);

//añadi para mostrar descargas
Route::get('/descargas', function () {
	$managementArea = DB::table('management_area')
		->first();

	/*$downloads = DB::table('download')
    ->where('download_state',1)
    ->get()*/
	$downloads = App\download::where('download_state', 1)
		->get();
	$socialNetwork = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();

	return view('download')
		->withManagement($managementArea)
		->withCategory($comunidad)
		->withDownloads($downloads)
		->withSocial($socialNetwork);
});
Route::get('/modelos', function () {
	return view('modelos');
});

Route::get('/modelos-descargas', function () {
	return view('modelos-descargas');
});
Route::get('/revistas', function () {
	$magazines = \App\magazine::orderBy('magazine_id', 'asc')->get();
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	return view('revistas')
		->withMagazines($magazines)
		->withManagement($managementArea)
		->withSocial($social)
		->withCategory($comunidad);
});

Route::get('/revista/{id}', function ($id) {
	$managementArea = DB::table('management_area')
		->first();
	$magazine = DB::table('magazine')->first();
	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	return view('revista')
		->withMagazine($magazine)
		->withManagement($managementArea)
		->withSocial($social)
		->withCategory($comunidad);
});

Route::get('/ofertas', function () {
	//aqui realice cambio 1 es notiia y 2 es eventos
	//se ordene descendentemente
	$managementArea = DB::table('management_area')
		->first();

	$social = DB::table('social_network')
		->where('social_network_state', 1)
		->get();
	$comunidad = DB::table('link')
		->leftJoin('comunidad', 'comunidad_id', '=', 'link.link_category')
		->where('link_state', 1)
		->orderBy('link.link_id', 'asc')
		->get();
	$news = DB::table('news')
		->leftJoin('multimedia', 'news.news_id', '=', 'multimedia.multimedia_news')
		->select('news.news_id', 'news.news_state', 'news.news_title', 'news.news_content', 'multimedia.multimedia_name', 'news.news_alias')
		->where('news.news_state', 1)
		->where('news.news_type', 3)
		->groupBy('multimedia.multimedia_news')
		->orderBy('news.news_id', 'desc')
		->paginate(2);

	return view('ofertas')
		->withManagement($managementArea)
		->withSocial($social)->withCategory($comunidad)

		->withProjects($news);
});
