<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GetNewsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     * @test
     */
    public function any_user_can_get_news()
    {
        //$response= $this->get('api/news');
        $response = $this->get(route('news.index'));
        //dd($response);

        $response->assertStatus(200)
        ->assertJsonStructure([[
            'news_id',
            'news_title',
            'news_content',
            'news_alias',
            'news_create',
            'news_update',
            'news_state',
            'news_author',
            'news_type',
            'news_management_area',
            'news_user',
            'url',
            'urlImage',
        ]]);


    }
}
