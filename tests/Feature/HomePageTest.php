<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomePageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     * @test
     */
    public function any_user_can_see_home_page()
    {
        //$this->withoutExceptionHandling();
        //1. given => Teniendo datos

        //2. When => Cuando
        $response = $this->get('/');
        //3. Then => Entonces comprobarmos los resultados
        $this->assertDatabaseHas('management_area',[
            'management_area_id' => 1
        ]);
        $response->assertStatus(200);
    }
}
