-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-07-2022 a las 07:21:08
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gad_colta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avance`
--

CREATE TABLE `avance` (
  `avance_id` int(11) UNSIGNED NOT NULL,
  `avance_proyecto_id` int(11) UNSIGNED NOT NULL,
  `avance_doc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `avance_fechainforme` date NOT NULL,
  `avance_observacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `avance_estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `avance_update` timestamp NULL DEFAULT NULL,
  `avance_create` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `avance`
--

INSERT INTO `avance` (`avance_id`, `avance_proyecto_id`, `avance_doc`, `avance_fechainforme`, `avance_observacion`, `avance_estado`, `avance_update`, `avance_create`) VALUES
(1, 1, 'avance.pdf', '2022-07-18', 'Primer Informe', '0', NULL, NULL),
(2, 1, 'capitulo 3_2.docx', '2022-07-22', 'Segundo Informe', '33', '2022-07-25 09:06:18', '2022-07-25 09:06:18'),
(3, 1, '24-06_2020_Trabajo de Titulación-Henry Morocho.docx', '2022-07-28', 'Tercer Informe', '100', '2022-07-26 08:26:25', '2022-07-26 08:26:25'),
(4, 2, 'HU-MS GAD Colta.docx', '2022-07-10', 'Primer Avance', '0', '2022-07-26 08:47:01', '2022-07-26 08:47:01'),
(5, 2, 'procesos.pdf', '2022-10-30', 'Segundo Informe', '50', '2022-07-26 08:47:18', '2022-07-26 08:47:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category_program`
--

CREATE TABLE `category_program` (
  `category_program_id` int(11) UNSIGNED NOT NULL,
  `category_program_description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunidad`
--

CREATE TABLE `comunidad` (
  `comunidad_id` int(10) UNSIGNED NOT NULL,
  `comunidad_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_ubicacion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_type_sociedad` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_type_organizacion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_type_suborganizacion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_num_hom` int(10) UNSIGNED DEFAULT NULL,
  `comunidad_num_muj` int(10) UNSIGNED DEFAULT NULL,
  `comunidad_num_poblacion` int(10) UNSIGNED DEFAULT NULL,
  `comunidad_num_familias` int(10) UNSIGNED DEFAULT NULL,
  `comunidad_num_mestizos` int(10) UNSIGNED DEFAULT NULL,
  `comunidad_num_indigenas` int(10) UNSIGNED DEFAULT NULL,
  `comunidad_cont_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_cont_direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_cont_tel` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_cont_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_ruc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_name_banco` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_num_cuenta` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_type_cuenta` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_observaciones` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comunidad_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `comunidad_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comunidad_state` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `comunidad`
--

INSERT INTO `comunidad` (`comunidad_id`, `comunidad_name`, `comunidad_ubicacion`, `comunidad_type_sociedad`, `comunidad_type_organizacion`, `comunidad_type_suborganizacion`, `comunidad_num_hom`, `comunidad_num_muj`, `comunidad_num_poblacion`, `comunidad_num_familias`, `comunidad_num_mestizos`, `comunidad_num_indigenas`, `comunidad_cont_name`, `comunidad_cont_direccion`, `comunidad_cont_tel`, `comunidad_cont_email`, `comunidad_ruc`, `comunidad_name_banco`, `comunidad_num_cuenta`, `comunidad_type_cuenta`, `comunidad_observaciones`, `comunidad_create`, `comunidad_update`, `comunidad_description`, `comunidad_state`) VALUES
(1, 'Colta Alta', 'colta 45', 'Si', '', '', 256, 4, 260, 20, 10, 90, 'Jose Mires', 'colta alta y cacerio santana', '0325896965', 'jose@gmail.com', '1804799755', 'Banco Pichincha', '2256987454', 'ahorros', 'qwerty', '2022-06-17 07:51:10', '2022-06-19 07:00:42', 'nada', 1),
(2, 'Las Lajas', 'centro colta', 'si', '', '', 256, 964, 1000, 25, 35, 65, 'Marcelo Malta', 'cacerio santana', '032598696', 'kristiano@gmail.com', '1804799755', 'Banco Pichincha', '2256987454', 'corriente', 'sadasdas', '2022-06-19 06:45:12', '2022-06-19 06:48:24', 'adsdasdas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunidad_proyecto`
--

CREATE TABLE `comunidad_proyecto` (
  `comunidad_proyecto_id` int(11) UNSIGNED NOT NULL,
  `comunidad_id` int(10) UNSIGNED NOT NULL,
  `proyecto_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conventions`
--

CREATE TABLE `conventions` (
  `conventions_id` int(10) UNSIGNED NOT NULL,
  `conventions_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `conventions_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `conventions_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conventions_state` tinyint(4) NOT NULL,
  `conventions_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `conventions_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `conventions_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinator_program`
--

CREATE TABLE `coordinator_program` (
  `coordinator_program_id` int(11) UNSIGNED NOT NULL,
  `coordinator_program_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `coordinator_program_last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `coordinator_program_mail` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `coordinator_program_cellphone` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `coordinator_program_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cultural_management`
--

CREATE TABLE `cultural_management` (
  `cultural_management_id` int(10) UNSIGNED NOT NULL,
  `cultural_management_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cultural_management_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cultural_management_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cultural_management_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cultural_management_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cultural_management_state` tinyint(4) NOT NULL,
  `cultural_management_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cultural_management`
--

INSERT INTO `cultural_management` (`cultural_management_id`, `cultural_management_name`, `cultural_management_image`, `cultural_management_description`, `cultural_management_create`, `cultural_management_update`, `cultural_management_state`, `cultural_management_management_area`) VALUES
(1, 'fsdfsdfsd', 'logo.png', 'dasdasd', '2022-06-07 04:17:54', '2022-06-09 04:17:54', 100, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cultural_management_types`
--

CREATE TABLE `cultural_management_types` (
  `cultural_management_types_id` int(10) UNSIGNED NOT NULL,
  `cultural_management_types_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cultural_management_types_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cultural_management_types_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cultural_management_types_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cultural_management_types_state` tinyint(4) NOT NULL,
  `cultural_management_types_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cultural_management_types`
--

INSERT INTO `cultural_management_types` (`cultural_management_types_id`, `cultural_management_types_name`, `cultural_management_types_description`, `cultural_management_types_create`, `cultural_management_types_update`, `cultural_management_types_state`, `cultural_management_types_area`) VALUES
(1, 'cultural', 'dasdasdasd', '2022-06-08 04:17:03', '2022-06-09 04:17:03', 100, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curriculum`
--

CREATE TABLE `curriculum` (
  `id_curriculum` int(10) NOT NULL,
  `type_curriculum` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module_curriculum` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curriculum_program`
--

CREATE TABLE `curriculum_program` (
  `curriculum_program_description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `curriculum_program_module` int(11) UNSIGNED NOT NULL,
  `curriculum_program_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directiva`
--

CREATE TABLE `directiva` (
  `directiva_id` int(10) UNSIGNED NOT NULL,
  `directiva_comunidad_id` int(10) UNSIGNED DEFAULT NULL,
  `directiva_descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `directiva_estado` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `directiva`
--

INSERT INTO `directiva` (`directiva_id`, `directiva_comunidad_id`, `directiva_descripcion`, `directiva_estado`) VALUES
(1, 2, 'Año 2002-2003', 1),
(2, 1, 'año 2011-2011', 2),
(3, 2, 'Año 2022-2023', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `download`
--

CREATE TABLE `download` (
  `download_id` int(10) UNSIGNED NOT NULL,
  `download_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_file` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_state` tinyint(4) NOT NULL,
  `download_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `download_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `download_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(10) UNSIGNED NOT NULL,
  `faculty_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_state` tinyint(4) NOT NULL,
  `faculty_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faculty_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `faculty_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faculty_news`
--

CREATE TABLE `faculty_news` (
  `faculty_news_id` int(10) UNSIGNED NOT NULL,
  `faculty_news_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_news_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_news_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_news_state` tinyint(4) NOT NULL,
  `faculty_news_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `faculty_news_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `faculty_news_faculty` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiamiento_proyecto`
--

CREATE TABLE `financiamiento_proyecto` (
  `financiamiento_proyecto_id` int(11) UNSIGNED NOT NULL,
  `financiamiento_id` int(10) UNSIGNED NOT NULL,
  `proyecto_id` int(10) UNSIGNED NOT NULL,
  `financiamiento_proyecto_update` timestamp NULL DEFAULT NULL,
  `financiamiento_proyecto_create` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `financiamiento_proyecto`
--

INSERT INTO `financiamiento_proyecto` (`financiamiento_proyecto_id`, `financiamiento_id`, `proyecto_id`, `financiamiento_proyecto_update`, `financiamiento_proyecto_create`) VALUES
(1, 1, 2, '2022-07-25 04:48:40', '2022-07-25 04:48:40'),
(2, 2, 3, '2022-07-02 04:49:21', '2022-06-17 04:49:21'),
(3, 3, 1, '2022-07-21 04:49:42', '2022-07-22 04:49:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(10) UNSIGNED NOT NULL,
  `gallery_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gallery_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_state` tinyint(4) NOT NULL,
  `gallery_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gallery_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `gallery_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `gallery_name`, `gallery_description`, `gallery_state`, `gallery_create`, `gallery_update`, `gallery_management_area`) VALUES
(1, 'prueba', 'prueba', 100, '2022-06-08 04:13:28', '2022-06-09 04:13:28', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infoc1_comunidad`
--

CREATE TABLE `infoc1_comunidad` (
  `infoc1_id` int(11) UNSIGNED NOT NULL,
  `infoc1_id_comunidad` int(10) UNSIGNED DEFAULT NULL,
  `infoc1_id_tipoinfcensal` int(10) UNSIGNED DEFAULT NULL,
  `infoc1_infocensalparam` int(10) UNSIGNED DEFAULT NULL,
  `infoc1_valor` int(10) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `infoc1_comunidad`
--

INSERT INTO `infoc1_comunidad` (`infoc1_id`, `infoc1_id_comunidad`, `infoc1_id_tipoinfcensal`, `infoc1_infocensalparam`, `infoc1_valor`, `updated_at`, `created_at`) VALUES
(0, 1, 1, 1, 10, '2022-07-13 06:13:24', '2022-07-04 05:00:00'),
(1, 2, 1, 8, 22, '2022-01-02 06:13:29', '2022-01-02 05:00:00'),
(2, 1, 1, 8, 3, '2022-07-18 06:18:26', '2022-07-18 06:18:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infoc2_comunidad`
--

CREATE TABLE `infoc2_comunidad` (
  `infoc2_id` int(11) UNSIGNED NOT NULL,
  `infoc2_id_comunidad` int(10) UNSIGNED DEFAULT NULL,
  `infoc2_id_tipoinfcensal` int(10) UNSIGNED DEFAULT NULL,
  `infoc2_infocensalparam` int(10) UNSIGNED DEFAULT NULL,
  `infoc2_valor` int(10) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `infoc2_comunidad`
--

INSERT INTO `infoc2_comunidad` (`infoc2_id`, `infoc2_id_comunidad`, `infoc2_id_tipoinfcensal`, `infoc2_infocensalparam`, `infoc2_valor`, `updated_at`, `created_at`) VALUES
(0, 1, 1, 1, 20, NULL, NULL),
(1, 1, 1, 2, 12, '2022-07-18 07:51:52', '2022-07-18 07:51:52'),
(2, 1, 2, 1, 10, '2022-07-19 11:08:45', '2022-07-19 11:08:45'),
(3, 1, 1, 3, 66, '2022-07-19 11:10:31', '2022-07-19 11:10:31'),
(7, 1, 1, 3, 999, '2022-07-19 11:15:33', '2022-07-19 11:15:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infocenso`
--

CREATE TABLE `infocenso` (
  `infocenso_id` int(11) UNSIGNED NOT NULL,
  `infocenso_id_Tipos` int(10) UNSIGNED NOT NULL,
  `infocenso_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `infocenso_update` timestamp NULL DEFAULT NULL,
  `infocenso_create` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `infocenso`
--

INSERT INTO `infocenso` (`infocenso_id`, `infocenso_id_Tipos`, `infocenso_description`, `infocenso_update`, `infocenso_create`) VALUES
(1, 1, 'Mujeres Que No Saben Leer Ni Escribir', '2022-07-17 07:26:02', '2022-07-07 03:10:40'),
(5, 2, 'Desempleados', '2022-07-07 05:00:00', '2022-01-19 03:10:51'),
(7, 3, 'Propia', '2022-07-17 04:13:57', '2022-07-17 04:13:57'),
(8, 1, 'Primaria Culminada', '2022-07-17 07:22:57', '2022-07-17 07:22:57'),
(10, 1, 'Secundaria A Termino Básico', '2022-07-17 07:25:37', '2022-07-17 07:25:37'),
(14, 4, 'Comercio', '2022-07-19 06:04:45', '2022-07-19 06:04:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infocenso2`
--

CREATE TABLE `infocenso2` (
  `infocenso2_id` int(11) UNSIGNED NOT NULL,
  `infocenso2_id_Tipos` int(10) UNSIGNED NOT NULL,
  `infocenso2_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `infocenso2`
--

INSERT INTO `infocenso2` (`infocenso2_id`, `infocenso2_id_Tipos`, `infocenso2_description`) VALUES
(1, 2, 'Luz'),
(2, 2, 'Agua'),
(3, 1, 'Siembra'),
(4, 3, 'Diabetes'),
(5, 4, 'Desnutricion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrantes`
--

CREATE TABLE `integrantes` (
  `integrantes_id` int(10) UNSIGNED NOT NULL,
  `integrantes_directiva_id` int(10) UNSIGNED NOT NULL,
  `integrantes_nombres` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `integrantes_cargo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `integrantes_sexo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `integrantes_directiva_fechaini` date NOT NULL,
  `integrantes_directiva_fechafin` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `integrantes`
--

INSERT INTO `integrantes` (`integrantes_id`, `integrantes_directiva_id`, `integrantes_nombres`, `integrantes_cargo`, `integrantes_sexo`, `integrantes_directiva_fechaini`, `integrantes_directiva_fechafin`, `updated_at`, `created_at`) VALUES
(1, 2, 'Jose Mires', 'Presidente', 'Mascullino', '2002-01-01', '2003-01-01', '2022-07-10', '0000-00-00'),
(2, 1, 'Maria Jeles Garzón', 'Secretaria', 'Feminino', '2010-01-01', '2011-01-01', '2022-07-10', '0000-00-00'),
(3, 3, 'Marcelo Guamán', 'Presidente', 'Femenino', '2020-07-14', '2021-07-14', '2022-07-11', '2022-07-11'),
(4, 1, 'Jorge Mendes', 'Presidente', 'Masculino', '2022-07-20', '2022-07-17', '2022-07-12', '2022-07-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kryptonit3_counter_page`
--

CREATE TABLE `kryptonit3_counter_page` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `kryptonit3_counter_page`
--

INSERT INTO `kryptonit3_counter_page` (`id`, `page`) VALUES
(1, '77461b5f-d7ed-56a5-a2c2-a99aba7ce4f9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kryptonit3_counter_page_visitor`
--

CREATE TABLE `kryptonit3_counter_page_visitor` (
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `visitor_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `kryptonit3_counter_page_visitor`
--

INSERT INTO `kryptonit3_counter_page_visitor` (`page_id`, `visitor_id`, `created_at`) VALUES
(1, 1, '2022-07-24 20:13:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kryptonit3_counter_visitor`
--

CREATE TABLE `kryptonit3_counter_visitor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visitor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `kryptonit3_counter_visitor`
--

INSERT INTO `kryptonit3_counter_visitor` (`id`, `visitor`) VALUES
(1, '36435c9f6376b9ce1ec467d0df70a244385d2d9d53f0e91714a8b569e8f39f9e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `link`
--

CREATE TABLE `link` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `link_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_url` varchar(800) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_state` tinyint(4) NOT NULL,
  `link_category` int(10) UNSIGNED NOT NULL,
  `link_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `link_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `magazine`
--

CREATE TABLE `magazine` (
  `magazine_id` int(10) NOT NULL,
  `magazine_name` varchar(200) NOT NULL,
  `magazine_image` varchar(200) NOT NULL,
  `magazine_file` varchar(200) NOT NULL,
  `magazine_flash` varchar(200) NOT NULL,
  `magazine_state` int(1) NOT NULL COMMENT 'Eliminación lógica',
  `magazine_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para el registro de Revistas';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `management_area`
--

CREATE TABLE `management_area` (
  `management_area_id` int(10) UNSIGNED NOT NULL,
  `management_area_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `management_area_siglas` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `management_area_provincia` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `management_area_ciudad` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `management_area_ruc` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `management_area_ruc_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_logo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_fax` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_map` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_mission` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_vision` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `management_area_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `management_area_objective` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_functions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_mail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_direction` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_image_mission` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_image_objective` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `management_area_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `managment_main_image1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `managment_main_image2` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `managment_main_image3` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `managment_main_image4` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `management_area`
--

INSERT INTO `management_area` (`management_area_id`, `management_area_name`, `management_area_siglas`, `management_area_provincia`, `management_area_ciudad`, `management_area_ruc`, `management_area_ruc_description`, `management_area_logo`, `management_area_phone`, `management_area_fax`, `management_area_map`, `management_area_mission`, `management_area_vision`, `management_area_create`, `management_area_update`, `management_area_objective`, `management_area_description`, `management_area_functions`, `management_area_mail`, `management_area_direction`, `management_area_image_mission`, `management_area_image_objective`, `management_area_image`, `managment_main_image1`, `managment_main_image2`, `managment_main_image3`, `managment_main_image4`) VALUES
(1, 'GAD_COLTA', 'GMCC', 'chimborazo', 'colta', '0602583833', 'ruc personal', 'logo_colta.png', '0325896963', '03258999696-96', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.05897480927!2d-78.77538468524553!3d-1.7015808987592331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91d3046a80c6bc11%3A0xc36ae6fc294eb665!2sGobierno%20Municipal%20de%20Colta!5e0!3m2!1ses-419!2sec!4v1654655871053!5m2!1ses-419!2sec\" width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>', 'eso', 'aja', '2012-06-07 05:00:00', '2022-06-08 07:37:58', 'aa', 'sda', 'asdasd', 'gmcc@gmail.com', 'colta', 'vision.png', 'mision.png', '53-0.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembro`
--

CREATE TABLE `miembro` (
  `miembro_id` int(10) UNSIGNED NOT NULL,
  `miembro_directiva_id` int(10) UNSIGNED NOT NULL,
  `miembro_nombres` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `miembro_cargo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `miembro_sexo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `miembro_directiva_fechaini` date NOT NULL,
  `miembro_directiva_fechafin` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `miembro`
--

INSERT INTO `miembro` (`miembro_id`, `miembro_directiva_id`, `miembro_nombres`, `miembro_cargo`, `miembro_sexo`, `miembro_directiva_fechaini`, `miembro_directiva_fechafin`, `updated_at`, `created_at`) VALUES
(1, 2, 'Jose Mires', 'Presidente', 'Mascullino', '2002-01-01', '2003-01-01', '2022-07-10', '0000-00-00'),
(2, 1, 'Maria Jeles Garzón', 'Secretaria', 'Feminino', '2010-01-01', '2011-01-01', '2022-07-10', '0000-00-00'),
(3, 3, 'Marcelo Guamán', 'Presidente', 'Femenino', '2020-07-14', '2021-07-14', '2022-07-11', '2022-07-11'),
(4, 1, 'Jorge Mendes', 'Presidente', 'Masculino', '2022-07-20', '2022-07-17', '2022-07-12', '2022-07-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module_curricular_pensum`
--

CREATE TABLE `module_curricular_pensum` (
  `module_curricular_pensum_id` int(11) UNSIGNED NOT NULL,
  `module_curricular_pensum_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module_curricular_pensum_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multimedia`
--

CREATE TABLE `multimedia` (
  `multimedia_id` int(10) UNSIGNED NOT NULL,
  `multimedia_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `multimedia_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multimedia_news` int(10) UNSIGNED DEFAULT NULL,
  `multimedia_gallery` int(10) UNSIGNED DEFAULT NULL,
  `multimedia_cultural_management_types` int(10) UNSIGNED DEFAULT NULL,
  `multimedia_type` int(10) UNSIGNED NOT NULL,
  `multimedia_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `multimedia_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `multimedia`
--

INSERT INTO `multimedia` (`multimedia_id`, `multimedia_name`, `multimedia_url`, `multimedia_news`, `multimedia_gallery`, `multimedia_cultural_management_types`, `multimedia_type`, `multimedia_create`, `multimedia_update`) VALUES
(1, 'not', '1.jpg', 1, 1, 1, 1, '2022-06-07 04:11:04', '2022-06-09 04:54:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multimedia_type`
--

CREATE TABLE `multimedia_type` (
  `multimedia_type_id` int(10) UNSIGNED NOT NULL,
  `multimedia_type_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `multimedia_type`
--

INSERT INTO `multimedia_type` (`multimedia_type_id`, `multimedia_type_description`) VALUES
(1, 'foto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE `news` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `news_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_alias` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `news_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `news_state` tinyint(4) NOT NULL,
  `news_type` int(10) UNSIGNED NOT NULL,
  `news_management_area` int(10) UNSIGNED NOT NULL,
  `news_user` int(10) UNSIGNED NOT NULL,
  `news_photo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `news_content`, `news_alias`, `news_create`, `news_update`, `news_state`, `news_type`, `news_management_area`, `news_user`, `news_photo`) VALUES
(1, 'esad', '<p>adsdasdasdasd sin mayor p menor</p>', 'dasdasdas', '2022-06-07 05:00:00', '2022-06-22 00:48:18', 1, 1, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news_type`
--

CREATE TABLE `news_type` (
  `news_type_id` int(10) UNSIGNED NOT NULL,
  `news_type_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `news_type`
--

INSERT INTO `news_type` (`news_type_id`, `news_type_description`) VALUES
(1, 'cronica'),
(2, 'internacionales'),
(5, 'nacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personal_id` int(11) UNSIGNED NOT NULL,
  `personal_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `personal_cargo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `personal_tlf` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personal_id`, `personal_name`, `personal_cargo`, `personal_tlf`) VALUES
(1, 'Jose Luis Gonzales', 'Ingeniero de Obras Publicas', '0995668559');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_proyecto`
--

CREATE TABLE `personal_proyecto` (
  `personal_proyecto_id` int(11) UNSIGNED NOT NULL,
  `personal_id` int(10) UNSIGNED NOT NULL,
  `proyecto_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `personal_proyecto`
--

INSERT INTO `personal_proyecto` (`personal_proyecto_id`, `personal_id`, `proyecto_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postulant`
--

CREATE TABLE `postulant` (
  `postulant_id` int(11) UNSIGNED NOT NULL,
  `postulant_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_id_document` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_document_number` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_phone` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_genre` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_mail` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_nationality` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `postulant_program` int(11) UNSIGNED NOT NULL,
  `postulant_address` varchar(450) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE `presupuesto` (
  `presupuesto_id` int(11) UNSIGNED NOT NULL,
  `presupuesto_monto` float UNSIGNED NOT NULL,
  `presupuesto_acumulado` float UNSIGNED NOT NULL,
  `presupuesto_observacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `presupuesto_estado` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `presupuesto_update` timestamp NULL DEFAULT NULL,
  `presupuesto_create` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `presupuesto`
--

INSERT INTO `presupuesto` (`presupuesto_id`, `presupuesto_monto`, `presupuesto_acumulado`, `presupuesto_observacion`, `presupuesto_estado`, `presupuesto_update`, `presupuesto_create`) VALUES
(1, 4600, 100, '', 'Recaudado', '2022-07-26 07:51:22', '2022-07-25 03:49:43'),
(2, 1000, 10, '', 'Recaudado', '2022-07-26 07:51:37', '2022-07-19 09:34:46'),
(3, 15000, 100, '', 'Recaudado', '2022-07-26 07:52:02', '2022-07-25 09:10:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `program`
--

CREATE TABLE `program` (
  `program_id` int(11) UNSIGNED NOT NULL,
  `program_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `program_type_program` int(11) NOT NULL,
  `program_category_program` int(11) NOT NULL,
  `program_objective` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `program_version` int(11) NOT NULL,
  `program_duration` int(11) NOT NULL,
  `program_date_begin` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `program_date_end` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `program_coordinator_program` int(11) NOT NULL,
  `program_curriculum_program` int(11) NOT NULL,
  `program_state` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `projects_id` int(10) UNSIGNED NOT NULL,
  `projects_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projects_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `projects_state` tinyint(4) NOT NULL,
  `projects_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `projects_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `projects_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `proyectos_id` int(11) UNSIGNED NOT NULL,
  `proyectos_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `proyectos_fechaini` date NOT NULL,
  `proyectos_fechafin` date NOT NULL,
  `proyectos_objetivo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `proyectos_nmeses` int(10) DEFAULT NULL,
  `proyectos_Cumrequisitos` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proyectos_estado` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proyectos_numInf` int(11) DEFAULT NULL,
  `proyectos_update` timestamp NULL DEFAULT NULL,
  `proyectos_create` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`proyectos_id`, `proyectos_name`, `proyectos_fechaini`, `proyectos_fechafin`, `proyectos_objetivo`, `proyectos_nmeses`, `proyectos_Cumrequisitos`, `proyectos_estado`, `proyectos_numInf`, `proyectos_update`, `proyectos_create`) VALUES
(1, 'Luz Para El Parque De Colta', '2022-07-30', '2022-11-30', 'Iluminar Los Espacios Públicos', 3, '', 'No Iniciado', 3, '2022-07-26 07:52:02', '2022-07-19 09:33:56'),
(2, 'Agua Para Pangor Alto', '2022-07-24', '2022-12-24', 'Dotar De Agua Al Poblado De Pangor', 5, '', 'En Proceso', 2, '2022-07-26 07:40:19', '2022-07-25 09:10:52'),
(3, 'CNT Internet', '2022-07-24', '2023-03-24', 'Internet En Colta', 8, '', 'No Iniciado', 4, '2022-07-26 07:40:35', '2022-07-25 09:36:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rate_program`
--

CREATE TABLE `rate_program` (
  `rate_program_id` int(11) UNSIGNED NOT NULL,
  `rate_program_description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rate_program_value` double NOT NULL,
  `rate_program_program` int(11) NOT NULL,
  `rate_program_type_rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requirements_program`
--

CREATE TABLE `requirements_program` (
  `requirements_program_id` int(11) UNSIGNED NOT NULL,
  `requirements_program_description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `requirements_program_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedule_program`
--

CREATE TABLE `schedule_program` (
  `schedule_program_id` int(11) UNSIGNED NOT NULL,
  `schedule_program_day` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_program_hour_begin` time NOT NULL,
  `schedule_program_hour_end` time NOT NULL,
  `schedule_program_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_name` char(50) DEFAULT NULL,
  `slider_description` char(50) DEFAULT NULL,
  `slider_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `slider_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `slider_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_network`
--

CREATE TABLE `social_network` (
  `social_network_id` int(10) UNSIGNED NOT NULL,
  `social_network_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_network_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_network_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_network_state` tinyint(4) NOT NULL,
  `social_network_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `social_network_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `social_network_management_area` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposinfocenso1`
--

CREATE TABLE `tiposinfocenso1` (
  `Tiposinfocenso_id` int(10) UNSIGNED NOT NULL,
  `Tiposinfocenso_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tiposinfocenso1`
--

INSERT INTO `tiposinfocenso1` (`Tiposinfocenso_id`, `Tiposinfocenso_description`) VALUES
(1, 'Datos Educacion'),
(2, 'Datos Empleo'),
(3, 'Datos Vivienda'),
(4, 'Datos Actividad Productiva');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposinfocenso2`
--

CREATE TABLE `tiposinfocenso2` (
  `Tiposinfocenso_id` int(10) UNSIGNED NOT NULL,
  `Tiposinfocenso_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tiposinfocenso2`
--

INSERT INTO `tiposinfocenso2` (`Tiposinfocenso_id`, `Tiposinfocenso_description`) VALUES
(1, 'Producción Agricola y Pecuaria'),
(2, 'Datos servicios basicos'),
(3, 'Datos Enfermedades comunes'),
(4, 'Datos Nutricion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_program`
--

CREATE TABLE `type_program` (
  `type_program_id` int(10) UNSIGNED NOT NULL,
  `type_program_description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_rate`
--

CREATE TABLE `type_rate` (
  `type_rate_id` int(11) UNSIGNED NOT NULL,
  `type_rate_description` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_cv` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_photo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_type` int(10) UNSIGNED NOT NULL,
  `user_estado` int(10) UNSIGNED NOT NULL,
  `user_management_area` int(10) UNSIGNED NOT NULL,
  `user_ci` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_telefono` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_mail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_observaciones` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_cargo_a` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_last_name`, `user_cv`, `user_photo`, `user_create`, `user_update`, `user_type`, `user_estado`, `user_management_area`, `user_ci`, `user_direccion`, `user_telefono`, `user_mail`, `user_observaciones`, `user_cargo_a`, `remember_token`, `password`) VALUES
(1, 'Christian', 'Arjona', 'procesos.pdf', 'Historia.jpg', '2022-06-03 03:27:30', '2022-07-26 04:23:52', 1, 2, 1, '1804799755', 'ambato', '0995644886', 'direccion@cimogsys.com', 'nada', 4, '5FCZ4dQYV97bDIv5GycrfM27eJEX8JEyg7HCBNm1eldLDIQg90k4LWN1Jfpw', '$2y$10$nheu/lR901kVtsLN25CeGu8ybUEYefR.XFruuA/QgBzN3aGv0qTV2'),
(2, 'Simon', 'Gualanga', 'Artículo Cientifico Arias-Uquillas.pdf', '53-0.jpg', '0000-00-00 00:00:00', '2022-06-19 04:20:11', 1, 1, 1, '0602583833', 'Tungurahua', '032587720', 'christianarias@ida.cl', 'nada', 2, 'B4U6ARvYdlxintG8xXJESBHLc0dbSKODhLn8aWwFM60jL7H55ljOy1eT64bi', '$2y$10$oaFbaWJbGiLOV3MmnbGGouGVRCLPMIQx4YiCnQg76qm2J64ab0aIy');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_cargo`
--

CREATE TABLE `user_cargo` (
  `user_cargo_id` int(10) UNSIGNED NOT NULL,
  `user_cargo_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_cargo`
--

INSERT INTO `user_cargo` (`user_cargo_id`, `user_cargo_description`) VALUES
(1, 'Jefe Principal'),
(2, 'Secretario'),
(3, 'Director Departamento'),
(4, 'Jefe de Sistemas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_estado`
--

CREATE TABLE `user_estado` (
  `user_estado_id` int(10) UNSIGNED NOT NULL,
  `user_estado_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_estado`
--

INSERT INTO `user_estado` (`user_estado_id`, `user_estado_description`) VALUES
(1, 'Activo'),
(2, 'Ausente'),
(3, 'Despedido'),
(4, 'Vacaciones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_type`
--

CREATE TABLE `user_type` (
  `user_type_id` int(10) UNSIGNED NOT NULL,
  `user_type_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_description`) VALUES
(1, 'Administrador'),
(2, 'Editor'),
(3, 'Subadministrador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `avance`
--
ALTER TABLE `avance`
  ADD PRIMARY KEY (`avance_id`),
  ADD KEY `avance_proyecto_id_foreign` (`avance_proyecto_id`);

--
-- Indices de la tabla `category_program`
--
ALTER TABLE `category_program`
  ADD PRIMARY KEY (`category_program_id`);

--
-- Indices de la tabla `comunidad`
--
ALTER TABLE `comunidad`
  ADD PRIMARY KEY (`comunidad_id`),
  ADD UNIQUE KEY `comunidad_comunidad_description_unique` (`comunidad_description`);

--
-- Indices de la tabla `comunidad_proyecto`
--
ALTER TABLE `comunidad_proyecto`
  ADD PRIMARY KEY (`comunidad_proyecto_id`),
  ADD KEY `comunidad_id_foreign` (`comunidad_id`),
  ADD KEY `proyecto_id_foreign` (`proyecto_id`);

--
-- Indices de la tabla `conventions`
--
ALTER TABLE `conventions`
  ADD PRIMARY KEY (`conventions_id`),
  ADD KEY `conventions_conventions_management_area_foreign` (`conventions_management_area`);

--
-- Indices de la tabla `coordinator_program`
--
ALTER TABLE `coordinator_program`
  ADD PRIMARY KEY (`coordinator_program_id`);

--
-- Indices de la tabla `cultural_management`
--
ALTER TABLE `cultural_management`
  ADD PRIMARY KEY (`cultural_management_id`),
  ADD UNIQUE KEY `cultural_management_cultural_management_name_unique` (`cultural_management_name`),
  ADD KEY `cultural_management_cultural_management_management_area_foreign` (`cultural_management_management_area`);

--
-- Indices de la tabla `cultural_management_types`
--
ALTER TABLE `cultural_management_types`
  ADD PRIMARY KEY (`cultural_management_types_id`),
  ADD UNIQUE KEY `cultural_management_types_cultural_management_types_name_unique` (`cultural_management_types_name`),
  ADD KEY `cultural_management_types_cultural_management_types_area_foreign` (`cultural_management_types_area`);

--
-- Indices de la tabla `curriculum_program`
--
ALTER TABLE `curriculum_program`
  ADD PRIMARY KEY (`curriculum_program_id`),
  ADD KEY `fk_curriculum_to_module` (`curriculum_program_module`);

--
-- Indices de la tabla `directiva`
--
ALTER TABLE `directiva`
  ADD PRIMARY KEY (`directiva_id`),
  ADD KEY `directiva_comunidad_id_foreign` (`directiva_comunidad_id`);

--
-- Indices de la tabla `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`download_id`),
  ADD UNIQUE KEY `download_download_name_unique` (`download_name`),
  ADD KEY `download_download_management_area_foreign` (`download_management_area`);

--
-- Indices de la tabla `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`),
  ADD UNIQUE KEY `faculty_faculty_name_unique` (`faculty_name`),
  ADD KEY `faculty_faculty_management_area_foreign` (`faculty_management_area`);

--
-- Indices de la tabla `faculty_news`
--
ALTER TABLE `faculty_news`
  ADD PRIMARY KEY (`faculty_news_id`),
  ADD KEY `faculty_news_faculty_news_faculty_foreign` (`faculty_news_faculty`);

--
-- Indices de la tabla `financiamiento_proyecto`
--
ALTER TABLE `financiamiento_proyecto`
  ADD PRIMARY KEY (`financiamiento_proyecto_id`),
  ADD KEY `financiamiento_id_foreign` (`financiamiento_id`),
  ADD KEY `proyectof_id_foreign` (`proyecto_id`);

--
-- Indices de la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`),
  ADD UNIQUE KEY `gallery_gallery_name_unique` (`gallery_name`),
  ADD KEY `gallery_gallery_management_area_foreign` (`gallery_management_area`);

--
-- Indices de la tabla `infoc1_comunidad`
--
ALTER TABLE `infoc1_comunidad`
  ADD PRIMARY KEY (`infoc1_id`),
  ADD KEY `infoc1_id_comunidad_foreign` (`infoc1_id_comunidad`),
  ADD KEY `infoc1_id_tipoinfcensal_foreign` (`infoc1_id_tipoinfcensal`),
  ADD KEY `infoc1_infocensalparam_foreign` (`infoc1_infocensalparam`);

--
-- Indices de la tabla `infoc2_comunidad`
--
ALTER TABLE `infoc2_comunidad`
  ADD PRIMARY KEY (`infoc2_id`),
  ADD KEY `infoc2_id_comunidad_foreign` (`infoc2_id_comunidad`),
  ADD KEY `infoc2_id_tipoinfcensal_foreign` (`infoc2_id_tipoinfcensal`),
  ADD KEY `infoc2_infocensalparam_foreign` (`infoc2_infocensalparam`);

--
-- Indices de la tabla `infocenso`
--
ALTER TABLE `infocenso`
  ADD PRIMARY KEY (`infocenso_id`),
  ADD KEY `infocenso_id_Tipos_foreign` (`infocenso_id_Tipos`);

--
-- Indices de la tabla `infocenso2`
--
ALTER TABLE `infocenso2`
  ADD PRIMARY KEY (`infocenso2_id`),
  ADD KEY `infocenso2_id_Tipos_foreign` (`infocenso2_id_Tipos`);

--
-- Indices de la tabla `integrantes`
--
ALTER TABLE `integrantes`
  ADD PRIMARY KEY (`integrantes_id`);

--
-- Indices de la tabla `kryptonit3_counter_page`
--
ALTER TABLE `kryptonit3_counter_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kryptonit3_counter_page_page_unique` (`page`);

--
-- Indices de la tabla `kryptonit3_counter_page_visitor`
--
ALTER TABLE `kryptonit3_counter_page_visitor`
  ADD KEY `kryptonit3_counter_page_visitor_page_id_index` (`page_id`),
  ADD KEY `kryptonit3_counter_page_visitor_visitor_id_index` (`visitor_id`);

--
-- Indices de la tabla `kryptonit3_counter_visitor`
--
ALTER TABLE `kryptonit3_counter_visitor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kryptonit3_counter_visitor_visitor_unique` (`visitor`);

--
-- Indices de la tabla `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`link_id`),
  ADD UNIQUE KEY `link_link_name_unique` (`link_name`),
  ADD KEY `link_link_category_foreign` (`link_category`),
  ADD KEY `link_link_management_area_foreign` (`link_management_area`);

--
-- Indices de la tabla `magazine`
--
ALTER TABLE `magazine`
  ADD PRIMARY KEY (`magazine_id`),
  ADD KEY `magazine_management_area` (`magazine_management_area`);

--
-- Indices de la tabla `management_area`
--
ALTER TABLE `management_area`
  ADD PRIMARY KEY (`management_area_id`),
  ADD UNIQUE KEY `management_area_management_area_name_unique` (`management_area_name`),
  ADD UNIQUE KEY `management_area_management_area_logo_unique` (`management_area_logo`);

--
-- Indices de la tabla `miembro`
--
ALTER TABLE `miembro`
  ADD PRIMARY KEY (`miembro_id`),
  ADD KEY `miembro_directiva_id_foreign` (`miembro_directiva_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `module_curricular_pensum`
--
ALTER TABLE `module_curricular_pensum`
  ADD PRIMARY KEY (`module_curricular_pensum_id`);

--
-- Indices de la tabla `multimedia`
--
ALTER TABLE `multimedia`
  ADD PRIMARY KEY (`multimedia_id`),
  ADD KEY `multimedia_multimedia_news_foreign` (`multimedia_news`),
  ADD KEY `multimedia_multimedia_type_foreign` (`multimedia_type`),
  ADD KEY `multimedia_multimedia_gallery_foreign` (`multimedia_gallery`),
  ADD KEY `multimedia_multimedia_cultural_management_types_foreign` (`multimedia_cultural_management_types`);

--
-- Indices de la tabla `multimedia_type`
--
ALTER TABLE `multimedia_type`
  ADD PRIMARY KEY (`multimedia_type_id`),
  ADD UNIQUE KEY `multimedia_type_multimedia_type_description_unique` (`multimedia_type_description`);

--
-- Indices de la tabla `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD UNIQUE KEY `news_news_alias_unique` (`news_alias`),
  ADD KEY `news_news_type_foreign` (`news_type`),
  ADD KEY `news_news_management_area_foreign` (`news_management_area`),
  ADD KEY `news_user` (`news_user`);

--
-- Indices de la tabla `news_type`
--
ALTER TABLE `news_type`
  ADD PRIMARY KEY (`news_type_id`),
  ADD UNIQUE KEY `news_type_news_type_description_unique` (`news_type_description`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191)),
  ADD KEY `password_resets_token_index` (`token`(191));

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personal_id`);

--
-- Indices de la tabla `personal_proyecto`
--
ALTER TABLE `personal_proyecto`
  ADD PRIMARY KEY (`personal_proyecto_id`),
  ADD KEY `personal_id_foreign` (`personal_id`),
  ADD KEY `proyectop_id_foreign` (`proyecto_id`);

--
-- Indices de la tabla `postulant`
--
ALTER TABLE `postulant`
  ADD PRIMARY KEY (`postulant_id`),
  ADD KEY `fk_postulant_program` (`postulant_program`);

--
-- Indices de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD PRIMARY KEY (`presupuesto_id`);

--
-- Indices de la tabla `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`program_id`),
  ADD KEY `fk_program_curriculum_program` (`program_curriculum_program`),
  ADD KEY `fk_program_coordinator` (`program_coordinator_program`),
  ADD KEY `fk_program_category_program` (`program_category_program`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projects_id`),
  ADD UNIQUE KEY `projects_projects_name_unique` (`projects_name`),
  ADD KEY `projects_projects_management_area_foreign` (`projects_management_area`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`proyectos_id`);

--
-- Indices de la tabla `rate_program`
--
ALTER TABLE `rate_program`
  ADD PRIMARY KEY (`rate_program_id`),
  ADD KEY `fk_rate_program_program` (`rate_program_program`),
  ADD KEY `fk_type_rate_rate` (`rate_program_type_rate`);

--
-- Indices de la tabla `requirements_program`
--
ALTER TABLE `requirements_program`
  ADD PRIMARY KEY (`requirements_program_id`),
  ADD KEY `fk_requirements_program_program` (`requirements_program_program`);

--
-- Indices de la tabla `schedule_program`
--
ALTER TABLE `schedule_program`
  ADD PRIMARY KEY (`schedule_program_id`),
  ADD KEY `fk_schedule_program_program` (`schedule_program_program`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indices de la tabla `social_network`
--
ALTER TABLE `social_network`
  ADD PRIMARY KEY (`social_network_id`),
  ADD UNIQUE KEY `social_network_social_network_name_unique` (`social_network_name`),
  ADD KEY `social_network_social_network_management_area_foreign` (`social_network_management_area`);

--
-- Indices de la tabla `tiposinfocenso1`
--
ALTER TABLE `tiposinfocenso1`
  ADD PRIMARY KEY (`Tiposinfocenso_id`);

--
-- Indices de la tabla `tiposinfocenso2`
--
ALTER TABLE `tiposinfocenso2`
  ADD PRIMARY KEY (`Tiposinfocenso_id`);

--
-- Indices de la tabla `type_program`
--
ALTER TABLE `type_program`
  ADD PRIMARY KEY (`type_program_id`);

--
-- Indices de la tabla `type_rate`
--
ALTER TABLE `type_rate`
  ADD PRIMARY KEY (`type_rate_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_user_cv_unique` (`user_cv`),
  ADD UNIQUE KEY `user_user_photo_unique` (`user_photo`),
  ADD UNIQUE KEY `user_remember_token_unique` (`remember_token`),
  ADD KEY `user_user_estado_foreign` (`user_estado`),
  ADD KEY `user_user_management_area_foreign` (`user_management_area`),
  ADD KEY `user_user_type_foreign` (`user_type`),
  ADD KEY `user_user_cargo_foreign` (`user_cargo_a`);

--
-- Indices de la tabla `user_cargo`
--
ALTER TABLE `user_cargo`
  ADD PRIMARY KEY (`user_cargo_id`);

--
-- Indices de la tabla `user_estado`
--
ALTER TABLE `user_estado`
  ADD PRIMARY KEY (`user_estado_id`),
  ADD UNIQUE KEY `user_estado_user_estado_description_unique` (`user_estado_description`);

--
-- Indices de la tabla `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_id`),
  ADD UNIQUE KEY `user_type_user_type_description_unique` (`user_type_description`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `avance`
--
ALTER TABLE `avance`
  MODIFY `avance_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `category_program`
--
ALTER TABLE `category_program`
  MODIFY `category_program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comunidad`
--
ALTER TABLE `comunidad`
  MODIFY `comunidad_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `comunidad_proyecto`
--
ALTER TABLE `comunidad_proyecto`
  MODIFY `comunidad_proyecto_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `conventions`
--
ALTER TABLE `conventions`
  MODIFY `conventions_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coordinator_program`
--
ALTER TABLE `coordinator_program`
  MODIFY `coordinator_program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cultural_management`
--
ALTER TABLE `cultural_management`
  MODIFY `cultural_management_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cultural_management_types`
--
ALTER TABLE `cultural_management_types`
  MODIFY `cultural_management_types_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `curriculum_program`
--
ALTER TABLE `curriculum_program`
  MODIFY `curriculum_program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `directiva`
--
ALTER TABLE `directiva`
  MODIFY `directiva_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `download`
--
ALTER TABLE `download`
  MODIFY `download_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `faculty_news`
--
ALTER TABLE `faculty_news`
  MODIFY `faculty_news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `financiamiento_proyecto`
--
ALTER TABLE `financiamiento_proyecto`
  MODIFY `financiamiento_proyecto_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `infoc1_comunidad`
--
ALTER TABLE `infoc1_comunidad`
  MODIFY `infoc1_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `infoc2_comunidad`
--
ALTER TABLE `infoc2_comunidad`
  MODIFY `infoc2_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `infocenso`
--
ALTER TABLE `infocenso`
  MODIFY `infocenso_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `infocenso2`
--
ALTER TABLE `infocenso2`
  MODIFY `infocenso2_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `integrantes`
--
ALTER TABLE `integrantes`
  MODIFY `integrantes_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `kryptonit3_counter_page`
--
ALTER TABLE `kryptonit3_counter_page`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `kryptonit3_counter_visitor`
--
ALTER TABLE `kryptonit3_counter_visitor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `link`
--
ALTER TABLE `link`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `magazine`
--
ALTER TABLE `magazine`
  MODIFY `magazine_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `management_area`
--
ALTER TABLE `management_area`
  MODIFY `management_area_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `miembro`
--
ALTER TABLE `miembro`
  MODIFY `miembro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `module_curricular_pensum`
--
ALTER TABLE `module_curricular_pensum`
  MODIFY `module_curricular_pensum_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `multimedia`
--
ALTER TABLE `multimedia`
  MODIFY `multimedia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `multimedia_type`
--
ALTER TABLE `multimedia_type`
  MODIFY `multimedia_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `news_type`
--
ALTER TABLE `news_type`
  MODIFY `news_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personal_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `personal_proyecto`
--
ALTER TABLE `personal_proyecto`
  MODIFY `personal_proyecto_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `postulant`
--
ALTER TABLE `postulant`
  MODIFY `postulant_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  MODIFY `presupuesto_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `program`
--
ALTER TABLE `program`
  MODIFY `program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `projects_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `proyectos_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rate_program`
--
ALTER TABLE `rate_program`
  MODIFY `rate_program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `requirements_program`
--
ALTER TABLE `requirements_program`
  MODIFY `requirements_program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `schedule_program`
--
ALTER TABLE `schedule_program`
  MODIFY `schedule_program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `social_network`
--
ALTER TABLE `social_network`
  MODIFY `social_network_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tiposinfocenso1`
--
ALTER TABLE `tiposinfocenso1`
  MODIFY `Tiposinfocenso_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tiposinfocenso2`
--
ALTER TABLE `tiposinfocenso2`
  MODIFY `Tiposinfocenso_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `type_program`
--
ALTER TABLE `type_program`
  MODIFY `type_program_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `type_rate`
--
ALTER TABLE `type_rate`
  MODIFY `type_rate_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user_cargo`
--
ALTER TABLE `user_cargo`
  MODIFY `user_cargo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user_estado`
--
ALTER TABLE `user_estado`
  MODIFY `user_estado_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user_type`
--
ALTER TABLE `user_type`
  MODIFY `user_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `avance`
--
ALTER TABLE `avance`
  ADD CONSTRAINT `avance_proyecto_id_foreign` FOREIGN KEY (`avance_proyecto_id`) REFERENCES `proyectos` (`proyectos_id`);

--
-- Filtros para la tabla `comunidad_proyecto`
--
ALTER TABLE `comunidad_proyecto`
  ADD CONSTRAINT `comunidad_id_foreign` FOREIGN KEY (`comunidad_id`) REFERENCES `comunidad` (`comunidad_id`),
  ADD CONSTRAINT `proyecto_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`proyectos_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `conventions`
--
ALTER TABLE `conventions`
  ADD CONSTRAINT `conventions_conventions_management_area_foreign` FOREIGN KEY (`conventions_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `cultural_management`
--
ALTER TABLE `cultural_management`
  ADD CONSTRAINT `cultural_management_cultural_management_management_area_foreign` FOREIGN KEY (`cultural_management_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `cultural_management_types`
--
ALTER TABLE `cultural_management_types`
  ADD CONSTRAINT `cultural_management_types_cultural_management_types_area_foreign` FOREIGN KEY (`cultural_management_types_area`) REFERENCES `cultural_management` (`cultural_management_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `curriculum_program`
--
ALTER TABLE `curriculum_program`
  ADD CONSTRAINT `curriculum_program_ibfk_1` FOREIGN KEY (`curriculum_program_id`) REFERENCES `module_curricular_pensum` (`module_curricular_pensum_id`);

--
-- Filtros para la tabla `directiva`
--
ALTER TABLE `directiva`
  ADD CONSTRAINT `directiva_comunidad_id_foreign` FOREIGN KEY (`directiva_comunidad_id`) REFERENCES `comunidad` (`comunidad_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `download`
--
ALTER TABLE `download`
  ADD CONSTRAINT `download_download_management_area_foreign` FOREIGN KEY (`download_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `faculty`
--
ALTER TABLE `faculty`
  ADD CONSTRAINT `faculty_faculty_management_area_foreign` FOREIGN KEY (`faculty_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `faculty_news`
--
ALTER TABLE `faculty_news`
  ADD CONSTRAINT `faculty_news_faculty_news_faculty_foreign` FOREIGN KEY (`faculty_news_faculty`) REFERENCES `faculty` (`faculty_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `financiamiento_proyecto`
--
ALTER TABLE `financiamiento_proyecto`
  ADD CONSTRAINT `financiamiento_id_foreign` FOREIGN KEY (`financiamiento_id`) REFERENCES `presupuesto` (`presupuesto_id`),
  ADD CONSTRAINT `proyectof_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`proyectos_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `gallery_gallery_management_area_foreign` FOREIGN KEY (`gallery_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `infoc1_comunidad`
--
ALTER TABLE `infoc1_comunidad`
  ADD CONSTRAINT `infoc1_id_comunidad_foreign` FOREIGN KEY (`infoc1_id_comunidad`) REFERENCES `comunidad` (`comunidad_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `infoc1_id_tipoinfcensal_foreign` FOREIGN KEY (`infoc1_id_tipoinfcensal`) REFERENCES `tiposinfocenso1` (`Tiposinfocenso_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `infoc1_infocensalparam_foreign` FOREIGN KEY (`infoc1_infocensalparam`) REFERENCES `infocenso` (`infocenso_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `infoc2_comunidad`
--
ALTER TABLE `infoc2_comunidad`
  ADD CONSTRAINT `infoc2_id_comunidad_foreign` FOREIGN KEY (`infoc2_id_comunidad`) REFERENCES `comunidad` (`comunidad_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `infoc2_id_tipoinfcensal_foreign` FOREIGN KEY (`infoc2_id_tipoinfcensal`) REFERENCES `tiposinfocenso2` (`Tiposinfocenso_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `infoc2_infocensalparam_foreign` FOREIGN KEY (`infoc2_infocensalparam`) REFERENCES `infocenso2` (`infocenso2_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `infocenso`
--
ALTER TABLE `infocenso`
  ADD CONSTRAINT `infocenso_id_Tipos_foreign` FOREIGN KEY (`infocenso_id_Tipos`) REFERENCES `tiposinfocenso1` (`Tiposinfocenso_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `infocenso2`
--
ALTER TABLE `infocenso2`
  ADD CONSTRAINT `infocenso2_id_Tipos_foreign` FOREIGN KEY (`infocenso2_id_Tipos`) REFERENCES `tiposinfocenso2` (`Tiposinfocenso_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `kryptonit3_counter_page_visitor`
--
ALTER TABLE `kryptonit3_counter_page_visitor`
  ADD CONSTRAINT `kryptonit3_counter_page_visitor_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `kryptonit3_counter_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kryptonit3_counter_page_visitor_visitor_id_foreign` FOREIGN KEY (`visitor_id`) REFERENCES `kryptonit3_counter_visitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `link`
--
ALTER TABLE `link`
  ADD CONSTRAINT `link_link_category_foreign` FOREIGN KEY (`link_category`) REFERENCES `comunidad` (`comunidad_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `link_link_management_area_foreign` FOREIGN KEY (`link_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `magazine`
--
ALTER TABLE `magazine`
  ADD CONSTRAINT `magazine_ibfk_1` FOREIGN KEY (`magazine_management_area`) REFERENCES `management_area` (`management_area_id`);

--
-- Filtros para la tabla `miembro`
--
ALTER TABLE `miembro`
  ADD CONSTRAINT `miembro_directiva_id_foreign` FOREIGN KEY (`miembro_directiva_id`) REFERENCES `directiva` (`directiva_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `multimedia`
--
ALTER TABLE `multimedia`
  ADD CONSTRAINT `multimedia_multimedia_cultural_management_types_foreign` FOREIGN KEY (`multimedia_cultural_management_types`) REFERENCES `cultural_management_types` (`cultural_management_types_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `multimedia_multimedia_gallery_foreign` FOREIGN KEY (`multimedia_gallery`) REFERENCES `gallery` (`gallery_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `multimedia_multimedia_news_foreign` FOREIGN KEY (`multimedia_news`) REFERENCES `news` (`news_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `multimedia_multimedia_type_foreign` FOREIGN KEY (`multimedia_type`) REFERENCES `multimedia_type` (`multimedia_type_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`news_user`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `news_news_management_area_foreign` FOREIGN KEY (`news_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `news_news_type_foreign` FOREIGN KEY (`news_type`) REFERENCES `news_type` (`news_type_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `personal_proyecto`
--
ALTER TABLE `personal_proyecto`
  ADD CONSTRAINT `personal_id_foreign` FOREIGN KEY (`personal_id`) REFERENCES `personal` (`personal_id`),
  ADD CONSTRAINT `proyectop_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`proyectos_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `postulant`
--
ALTER TABLE `postulant`
  ADD CONSTRAINT `fk_postulant_program` FOREIGN KEY (`postulant_program`) REFERENCES `program` (`program_id`);

--
-- Filtros para la tabla `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_projects_management_area_foreign` FOREIGN KEY (`projects_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `social_network`
--
ALTER TABLE `social_network`
  ADD CONSTRAINT `social_network_social_network_management_area_foreign` FOREIGN KEY (`social_network_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_user_cargo_foreign` FOREIGN KEY (`user_cargo_a`) REFERENCES `user_cargo` (`user_cargo_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_user_estado_foreign` FOREIGN KEY (`user_estado`) REFERENCES `user_estado` (`user_estado_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_user_management_area_foreign` FOREIGN KEY (`user_management_area`) REFERENCES `management_area` (`management_area_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_user_type_foreign` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`user_type_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
