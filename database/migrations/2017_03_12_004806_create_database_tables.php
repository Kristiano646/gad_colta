<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user_type', function (Blueprint $table) {
        $table->increments('user_type_id');
        $table->string('user_type_description',100)->unique();

    });

       Schema::create('user', function (Blueprint $table) {
        $table->increments('user_id');
        $table->string('user_name',100);
        $table->string('user_last_name',100);
        $table->string('user_mail',100)->unique();
        $table->string('user_photo',100)->unique()->nullable();
        $table->timestamp('user_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('user_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->rememberToken()->unique()->nullable();
        $table->string('password',100);
        $table->string('user_phone',20)->unique()->nullable();
        $table->integer('user_type')->unsigned();
        $table->foreign('user_type')->references('user_type_id')->on('user_type')->onDelete('cascade');
    });

       Schema::create('multimedia_type', function (Blueprint $table) {
        $table->increments('multimedia_type_id');
        $table->string('multimedia_type_description',100)->unique();

    });

       Schema::create('authority_type', function (Blueprint $table) {
        $table->increments('authority_type_id');
        $table->string('authority_type_description',100)->unique();

    });

       Schema::create('management_area', function (Blueprint $table) {
        $table->increments('management_area_id');
        $table->string('management_area_name',100)->unique();
        $table->string('management_area_logo',100)->nullable()->unique();
        $table->string('management_area_phone',100)->nullable();
        $table->text('management_area_map')->nullable();
        $table->text('management_area_mission')->nullable();                    
        $table->text('management_area_vision')->nullable();
        $table->timestamp('management_area_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('management_area_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->text('management_area_objective')->nullable();
        $table->text('management_area_description')->nullable();
        $table->text('management_area_functions')->nullable();
        $table->string('management_area_mail',100)->nullable();
        $table->string('management_area_direction',100)->nullable();
        $table->string('management_area_image_mission',100)->nullable();
        $table->string('management_area_image_objective',100)->nullable();
        $table->string('management_area_image',100)->nullable();            
    });

       Schema::create('category', function (Blueprint $table) {
        $table->increments('category_id');
        $table->string('category_description',100)->unique();
        $table->tinyInteger('category_state');
        $table->timestamp('category_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('category_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->integer('category_parent')->unsigned()->nullable();
        $table->foreign('category_parent')->references('category_id')->on('category')->onDelete('cascade');
    });

       Schema::create('gallery', function (Blueprint $table) {
        $table->increments('gallery_id');
        $table->string('gallery_name',100)->unique();
        $table->text('gallery_description')->nullable();
        $table->tinyInteger('gallery_state');
        $table->timestamp('gallery_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('gallery_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->integer('gallery_management_area')->unsigned();
        $table->foreign('gallery_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');

    });

       Schema::create('news_type', function (Blueprint $table) {
        $table->increments('news_type_id');
        $table->string('news_type_description',100)->unique();

    });

       Schema::create('news', function (Blueprint $table) {
        $table->increments('news_id');
        $table->string('news_title',100)->unique();
        $table->text('news_content');
        $table->string('news_alias',100)->unique();
        $table->timestamp('news_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('news_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->tinyInteger('news_state');
        $table->integer('news_author')->unsigned();
        $table->integer('news_type')->unsigned();
        $table->integer('news_management_area')->unsigned();
        $table->foreign('news_type')->references('news_type_id')->on('news_type')->onDelete('cascade');
        $table->foreign('news_author')->references('user_id')->on('user')->onDelete('cascade');
        $table->foreign('news_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
    });

       Schema::create('cultural_management', function (Blueprint $table) {
        $table->increments('cultural_management_id');
        $table->string('cultural_management_name',100)->unique();
        $table->string('cultural_management_image',100);
        $table->text('cultural_management_description')->nullable();
        $table->timestamp('cultural_management_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('cultural_management_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->tinyInteger('cultural_management_state');
        $table->integer('cultural_management_management_area')->unsigned();
        $table->foreign('cultural_management_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
    });

       Schema::create('cultural_management_types', function (Blueprint $table) {
        $table->increments('cultural_management_types_id');
        $table->string('cultural_management_types_name',100)->unique();
        $table->text('cultural_management_types_description')->nullable();
        $table->timestamp('cultural_management_types_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('cultural_management_types_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->tinyInteger('cultural_management_types_state');
        $table->integer('cultural_management_types_area')->unsigned();
        $table->foreign('cultural_management_types_area')->references('cultural_management_id')->on('cultural_management')->onDelete('cascade');
    });

       Schema::create('multimedia', function (Blueprint $table) {
        $table->increments('multimedia_id');
        $table->string('multimedia_name',100);
        $table->string('multimedia_url')->nullable();
        $table->integer('multimedia_news')->unsigned()->nullable();
        $table->integer('multimedia_gallery')->unsigned()->nullable();
        $table->integer('multimedia_cultural_management_types')->unsigned()->nullable();
        $table->integer('multimedia_type')->unsigned();
        $table->timestamp('multimedia_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('multimedia_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->foreign('multimedia_news')->references('news_id')->on('news')->onDelete('cascade')->onUpdate('cascade');
        $table->foreign('multimedia_type')->references('multimedia_type_id')->on('multimedia_type')->onDelete('cascade');
        $table->foreign('multimedia_gallery')->references('gallery_id')->on('gallery')->onDelete('cascade');
        $table->foreign('multimedia_cultural_management_types')->references('cultural_management_types_id')->on('cultural_management_types')->onDelete('cascade');

    });

       Schema::create('authority', function (Blueprint $table) {
        $table->increments('authority_id');
        $table->string('authority_name',100);
        $table->string('authority_last_name',100);
        $table->string('authority_cv',100)->nullable()->unique();
        $table->string('authority_photo',100)->nullable()->unique();
        $table->timestamp('authority_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('authority_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->integer('authority_type')->unsigned();
        $table->integer('authority_management_area')->unsigned();
        $table->foreign('authority_type')->references('authority_type_id')->on('authority_type')->onDelete('cascade');
        $table->foreign('authority_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
    });

       Schema::create('link', function (Blueprint $table) {
        $table->increments('link_id');
        $table->string('link_name',100)->unique();
        $table->string('link_url',800);
        $table->text('link_description')->nullable();
        $table->tinyInteger('link_state');
        $table->integer('link_category')->unsigned();
        $table->timestamp('link_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('link_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->integer('link_management_area')->unsigned();
        $table->foreign('link_category')->references('category_id')->on('category')->onDelete('cascade');
        $table->foreign('link_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
    });

       Schema::create('download', function (Blueprint $table) {
        $table->increments('download_id');
        $table->string('download_name',100)->unique();
        $table->text('download_description');
        $table->string('download_file',100);
        $table->tinyInteger('download_state');
        $table->timestamp('download_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('download_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->integer('download_management_area')->unsigned();
        $table->foreign('download_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
    });

       Schema::create('social_network', function (Blueprint $table) {
        $table->increments('social_network_id');
        $table->string('social_network_name',100)->unique();
        $table->string('social_network_url',500);
        $table->string('social_network_image');
        $table->tinyInteger('social_network_state');
        $table->timestamp('social_network_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('social_network_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->integer('social_network_management_area')->unsigned();
        $table->foreign('social_network_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
    });

   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {     
       Schema::drop('social_network');
       Schema::drop('download');
       Schema::drop('link');
       Schema::drop('authority');
       Schema::drop('multimedia');
       Schema::drop('cultural_management_types');
       Schema::drop('cultural_management');
       Schema::drop('news');
       Schema::drop('news_type');
       Schema::drop('gallery');
       Schema::drop('category');
       Schema::drop('management_area');
       Schema::drop('authority_type');
       Schema::drop('multimedia_type');
       Schema::drop('user');
       Schema::drop('user_type');
   }
}
