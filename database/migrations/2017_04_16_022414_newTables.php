<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('faculty', function (Blueprint $table) {
            $table->increments('faculty_id');
            $table->string('faculty_name',100)->unique();
            $table->text('faculty_image');
            $table->tinyInteger('faculty_state');
            $table->timestamp('faculty_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('faculty_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->integer('faculty_management_area')->unsigned();
            $table->foreign('faculty_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
        });

        Schema::create('faculty_news', function (Blueprint $table) {
            $table->increments('faculty_news_id');
            $table->string('faculty_news_name',100)->unique();
            $table->text('faculty_news_description');
            $table->text('faculty_news_image');
            $table->tinyInteger('faculty_news_state');
            $table->timestamp('faculty_news_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('faculty_news_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->integer('faculty_news_faculty')->unsigned();
            $table->foreign('faculty_news_faculty')->references('faculty_id')->on('faculty')->onDelete('cascade');
        });

        Schema::create('projects', function (Blueprint $table) {
            $table->increments('projects_id');
            $table->string('projects_name',100)->unique();
            $table->text('projects_description');
            $table->tinyInteger('projects_state');
            $table->timestamp('projects_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('projects_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->integer('projects_management_area')->unsigned();
            $table->foreign('projects_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
        });

        Schema::create('conventions', function (Blueprint $table) {
            $table->increments('conventions_id');
            $table->string('conventions_name',100)->unique();
            $table->text('conventions_type');
            $table->string('conventions_file');
            $table->tinyInteger('conventions_state');
            $table->timestamp('conventions_create')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('conventions_update')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->integer('conventions_management_area')->unsigned();
            $table->foreign('conventions_management_area')->references('management_area_id')->on('management_area')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conventions');
        Schema::drop('projects');
        Schema::drop('faculty_news');
        Schema::drop('faculty');
    }
}
