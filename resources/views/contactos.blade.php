@extends('layouts.plantilla')

@section('title') Contactos @stop

@section('estilos')
@parent
<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')

<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="contactos">

			<div class="ed-item main-center">
				<h3>Contactos</h3>
			</div>
			<div class="ed-item cross-center web-60">
				<!-- <img src="{{$management->management_area_map}}"> -->
				<div id="iframe1"></div>
			</div>
			<div class="ed-item cross-center web-40">
				<div class="ed-container">
					<div class="ed-item no-padding cross-center">
						<p><b>Dirección:</b> {{$management->management_area_direction}}</p>
						<p><b>Email:</b> {{$management->management_area_mail}}</p>
						<p><b>Telf:</b> {{$management->management_area_phone}}</p>
						<p><b>Fax:</b> {{$management->management_area_fax}}</p>
						<p><b>{{$management->management_area_ciudad}} - {{$management->management_area_provincia}}</b></p>
					</div>
					<!--
					<div class="ed-item cross-center desde-web" id="redes">
						<div class="ed-container">
							@foreach($social as $socialNetwork)
							<div class="ed-item main-center cross-center no-padding base-20">
								<a href="{{$socialNetwork->social_network_url}}" target="blank" title="{{$socialNetwork->social_network_name}}">
									<img src="{{ asset('socialNetwork/'.$socialNetwork->social_network_image) }}" alt="">
								</a>
							</div>
							@endforeach
						</div>
					</div>
					-->
				</div>
			</div>
		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">
		$('nav').addClass('sticky');


		// $('body').on({
		// 	'mousewheel': function(e) {
		// 		if (e.target.id == 'el') return;
		// 		e.preventDefault();
		// 		e.stopPropagation();
		// 	}
		// })

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if (target.length) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});


		
		function escribiriframe() {
			var iframe1= '{!!$management->management_area_map!!}'
			document.getElementById("iframe1").innerHTML = iframe1
		}
		window.onload = function() {
			escribiriframe();
		}
	</script>
	@stop
</body>
@stop