<!DOCTYPE html>
<html lang="es">
<head>
	<title>@yield('title') - GAD-Colta</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">   
	<link rel="icon" href="{{ asset('img/iconos/vincu.ico') }}">
	@section('estilos')
	
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/inicio.css') }}">
	@show
</head>
@section('body')
<!-- **************************************************** -->
@section('menu')
<nav class="ed-container full">
	<div class="ed-item base-1-3 hasta-web main-center cross-center">
		<a class="menu-act">
			<i class="fa fa-bars fa-2x"></i>
		</a>
	</div>
	<div class="ed-item main-center cross-center base-1-3 web-10">
		<a href="{{ url('/') }}">
			<img id="fsp-blanco" src="{{ asset('img/logos/'.$management->management_area_logo)}} " alt="logo">
		</a>
	</div>
	<div class="ed-item main-center cross-center base-75 tablet-1-3 web-80">
		<ul class="ed-menu web-horizontal default">
			<li class="hasta-web">
				<a class="menu-act">
					Menú <i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</li>
			<li>
				<a>Inicio</a>
				<ul>
					<li>
						<!--a href="{{ url('/quienes-somos') }}">Quiénes Somos?</a-->
                        <a href="{{ url('/quienes-somos') }}">Quiénes Somos?</a>
					</li>
					<li>
						<!--a href="{{ url('/mision') }}">Misión - Visión</a-->
                        <a href="{{ url('/mision') }}">Misión - Visión</a>
					</li>
					<li>
						<!--a href="{{ url('/objetivos') }}">Objetivos</a-->
                      	<a href="{{ url('/objetivos') }}">Objetivos</a>
					</li>
					<li>
						<!--a href="{{ url('/funciones') }}">Responsabilidades</a-->
                        <a href="{{ url('/funciones') }}">Responsabilidades</a>
					</li>
					
				</ul>
			</li>
	
			<li>
              	<!--a href="{{url('/noticias')}}">Noticias</a-->
				<a href="{{url('/noticias')}}">Noticias</a>
			</li>
			<li>
				<a>Información</a>
				<ul>
					<li>
           		    <!--a href="{{ url('/descargas') }}">Descargas</a-->
						<a href="https://gadcolta.gob.ec/gadcolta/">Página Principal</a>
					</li>
					
				</ul>
			</li>
			

			

			<li>
               <!--a href="{{ url('/descargas') }}">Descargas</a-->
				<a href="{{ url('/descargas') }}">Descargas</a>
			</li>
			
			<li>
				<!--a href="{{ url('/contactos') }}">Contactos</a-->
              	<a href="{{ url('/contactos') }}">Contactos</a>
			</li>
		</ul>
	</div>
	
</nav>
<!-- **************************************************** -->
@show
@section('header')

@show
@section('main')

@show
@section('footer')
<footer>
	<div class="ed-container full" id="footer">
		<div class="ed-item main-center cross-center desde-web web-25">

		</div>
		<div class="ed-item main-center cross-center web-50">
			<p> {{$management->management_area_name}} - Chimborazo </p>
			<p> <a href="">Términos de Uso</a> | <a href="">Políticas de Privacidad</a> | <a href="">Acerca de</a> | <a href="{{asset('docs/creditos.pdf')}}" target="_blank">Créditos</a>| <a target="_blank" href="{{route('login')}}">Login</a> </p>
		</div>
		<div class="ed-item main-center cross-center no-padding web-25">
			<div class="ed-container main-center full">
				
				@forelse($social as $socialData)
				<div class="ed-item main-center cross-center no-padding base-20">
					<!--a href="{{$socialData->social_network_url}}" target="_blank"><img class="social-net" src="{{ asset('socialNetwork/'.$socialData->social_network_image)}}" alt=""> </a-->
                    <a href="{{$socialData->social_network_url}}" target="_blank"><img class="social-net" src="#" alt=""> </a>
				</div>
				@empty
				@endforelse

				<!-- *************************** -->

				<!-- *************************** -->

			</div>
		</div>

	</div>
</footer>
@show
@section('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript">
	$('.menu-act').on('click', function() {
	            //$('.ed-menu').closest('.ed-item').slideToggle();
	            //$('#element').animate({width: 'toggle'});

	            $('.ed-menu').closest('.ed-item').animate({width: 'toggle'});
	        });
	$('ul.ed-menu li').on('click', function() {
		event.stopPropagation();
		$(this).children('ul').slideToggle();
	});
</script>
<script>
		  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-77474853-5', 'auto');
		  ga('send', 'pageview');*/
		</script>
		@show
		@show
		</html>
