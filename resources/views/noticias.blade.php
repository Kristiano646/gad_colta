@extends('layouts.plantilla')

@section('title') Noticias @stop

@section('estilos')
@parent

@stop
@section('body')

<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="noticias-facultad">
			<div class="ed-item main-center">
				<h3>NOTICIAS</h3>
				
			</div>
			<hr><br>
				<div class="ed-container full">
					@forelse($news as $newsData)
					@if($newsData->news_id != null)
					<div class="ed-item base-100 web-50">
						<div class="ed-container main-center noticia-facultad" id="principal">
							<a href="{{ url('/noticia/'.$newsData->news_id) }}"><i class="fa fa-plus fa-2x"></i></a>
							<div class="ed-item cross-center main-center">
								@if($newsData->news_id != null)
								<img class="noticia__item_pequena" src="{{ asset('img/noticias/'.$newsData->news_photo) }}" alt="imagen de las noticias">
								@else
								Imagen no Disponible
								@endif

								<p>{!! str_limit($newsData->news_title, $limit = 50, $end = ' ...') !!}</p>
							</div>
						</div>
					</div>
					@endif
					@empty
					No existen noticias a visualizar
					@endforelse
				</div>
			<div class="ed-item main-center" id="paginacion">
				{{$news->links()}}

			</div>
		</article>
	</main>
	<br>
	<br>
	<br>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">
		$('nav').addClass('sticky');

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if (target.length) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});
	</script>
	@stop
</body>
@stop