@extends('layouts.plantilla')

@section('title') Vinculación @stop

@section('estilos')
	@parent
	<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')
	<body class="">
		@parent
		@section('menu')
			@parent
		@stop
		@section('main')
			<main>
        <article class="ed-container full" id="0">
    			<div class="ed-item main-center cross-center">
    				<h1> Quiénes Somos? </h1>
    			</div>
    			<div class="ed-item main-end">
					<h1>{!!$management->management_area_name!!}</h1>
    			
    			</div>
    			<div class="ed-item cross-start web-50">
    				<img src="{!!asset('img/logos/'.$management->management_area_logo)!!}" alt="">
    			</div>
    			<div class="ed-item cross-start web-50">
				<b>	<p>Fecha de Fundacion:  {!!date('j F, Y', strtotime($management->management_area_create))!!}       </p></b>
    				<p>{!!$management->management_area_description!!}</p>
					
    			</div>
				
    		</article>
			</main>
		@stop
		@section('footer')
			@parent
		@stop
		@section('scripts')
			@parent
			<script type="text/javascript">

				$('nav').addClass('sticky');

				$('a[href^="#"]').on('click', function(event) {
				    var target = $(this.getAttribute('href'));
				    if( target.length ) {
				        event.preventDefault();
				        $('html, body').stop().animate({
				            scrollTop: target.offset().top
				        }, 1000);
				    }
				});

			</script>
		@stop
	</body>
@stop
