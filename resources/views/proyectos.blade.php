@extends('layouts.plantilla')

@section('title') Proyectos @stop

@section('estilos')
@parent
<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')
<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="facultad-proyectos">
			<div class="ed-item main-end cross-center">
				<h3>Proyectos Culturales {{$management->management_area_name}}</h3>
			</div>
			<div class="ed-item">
				@forelse($projects as $projectsData)
				<div class="ed-container proyecto">
					<div class="ed-item cross-center web-85 contenido">
						<h2> {{$projectsData->news_title}} </h2>
						<img src="{{ asset('img/noticias/'.$projectsData->multimedia_name) }}" alt="">
						<p>{!! str_limit($projectsData->news_content, $limit = 250, $end = ' ...') !!}</p>
						<span>
							<a href="{{ url('/noticia/'.$projectsData->news_id) }}">... Leer más</a>

						</span>
					</div>
				</div>
				@empty
					No hay proyectos
				@endforelse
			</div>
			{{$projects->links()}}

		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">

		$('nav').addClass('sticky');

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});
	</script>
	@stop
</body>
@stop
