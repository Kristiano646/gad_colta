@extends('layouts.admin')

@section('title') Comunidades @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<form method="POST" action="{{route('updateComunidad')}}" name="f2" class="action__form" enctype=multipart/form-data>
			<div class="form__title">
				<h3>MODIFICAR COMUNIDAD</h3>
				<br>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="comunidadId" value="">
			<div class="contenedor__categorias">
				<div class="main__function">
					<h3>Datos Generales</h3>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre: </label>
					</div>
					<div class="container__item">
						<input type="text" name="comunidadName">
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Ubicación:</label>
					</div>
					<div class="container__item">
						<input type="text" name="comunidadUbicacion">
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Jurídico:</label>
					</div>
					<div class="container__item">
						<select name="comunidadTipo">
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Tipo de Organización:</label>
					</div>
					<div class="container__item">
						<select name="comunidadTipoOrg" id="TipoOrg" onchange="cambia_org1()">
							<option value="0" selected>Seleccione... </option>
							<option value="1">Según la ganancia....</option>
							<option value="2">Según la estructura....</option>
							<option value="3">Según la propiedad....</option>
							<option value="4">Según su tamaño....</option>
						</select>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Subtipo de Organización:</label>
					</div>
					<div class="container__item">
						<select id="submenu" name="comunidadSubmenu" class="form-control" onchange="llenarInputs()">
							<option>----</option>
						</select>
						<input type="text" id="submenut" name="comunidadSubmenut" style="display: none;"></input>
					</div>
				</div>
				@if(Session::has('mensaje'))
				<div class="form__container">
					<div id="mensaje">
						{{Session::get('mensaje')}}
					</div>
				</div>
				@endIf
				@if (count($errors) > 0)
				<div class="form__container">
					<ul id="mensaje">
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
			<div class="contenedor__categorias">
				<div class="main__function">
					<h3>Contacto</h3>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre y Apellido: </label>
					</div>
					<div class="container__item">
						<input type="text" name="ComunidadConNom" required></input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Dirección: </label>
					</div>
					<div class="container__item">
						<input type="text" name="ComunidadConDir" required></input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Teléfono: </label>
					</div>
					<div class="container__item">
						<input type="tel" name="ComunidadConTel" maxlength="10" minlength="10" placeholder=" Digite 10 números" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required></input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Email: </label>
					</div>
					<div class="container__item">
						<input type="email" name="ComunidadConEmail" required></input>
					</div>
				</div>
			</div>

			<div class="contenedor__categorias">
				<div class="main__function">
					<h3>Población</h3>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Número de hombres: </label>
					</div>
					<div class="container__item">
						<input type="number" name="ComunidadNhombres" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Número de Mujeres: </label>
					</div>
					<div class="container__item">
						<input type="number" name="ComunidadNmujeres" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Población Base: </label>
					</div>
					<div class="container__item">
						<input type="number" name="ComunidadPbase" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Númro de Familias: </label>
					</div>
					<div class="container__item">
						<input type="number" name="ComunidadNfamilias" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
					</div>
				</div>
			</div>
			<div class="contenedor__categorias">
				<div class="main__function">
					<h3>Etnicidad</h3>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Mestizos: </label>
					</div>
					<div>
						<input type="number" class="mest" id="mest" name="ComunidadMestizos" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>%</input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Indígenas: </label>
					</div>
					<div>
						<input type="number" class="indi" id="indi" name="ComunidadIndigenas" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required readonly>%</input>
					</div>
				</div>
			</div>

			<div class="contenedor__categorias">
				<div class="main__function">
					<h3>Datos Bancarios</h3>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Cédula: </label>
					</div>
					<div class="container__item">
						<input id="target" type="text" name="ComunidadRuc" required></input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre del Banco: </label>
					</div>
					<div class="container__item">
						<input type="text" name="ComunidadNBanco" required></input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Cuenta Bancaria: </label>
					</div>
					<div class="container__item">
						<input type="tel" name="ComunidadNumCuen" maxlength="20" placeholder="(Número de cuenta)" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required></input>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Tipo de Cuenta:</label>
					</div>
					<div class="container__item">
						<select id="comunidadState" name="comunidadtypeCuen">
							<option value=""></option>
							<option value="ahorros">Ahorros</option>
							<option value="corriente">Corrinte</option>
						</select>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Observaciones: </label>
					</div>
					<div class="container__item">
						<textarea name="ComunidadObs" rows="10" cols="40">Escribe aquí tus comentarios</textarea>
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Descripción: </label>
					</div>
					<div class="container__item">
						<input type="text" name="comunidadDescription">
					</div>
				</div>


			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<section>
	<div class="page__delete">
		<form method="POST" action="{{route('deleteComunidad')}}" class="action__form" enctype=multipart/form-data>
			<div class="form__title">
				<h1>Eliminar Comunidad</h1>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="comunidadId" value="">
			<div class="contenedor__categorias">
				<div class="main__function">
					<h3>Datos Generales</h3>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre: </label>
					</div>
					<div class="container__item">
						<input type="text" name="comunidadName" disabled>
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Ubicación:</label>
					</div>
					<div class="container__item">
						<input type="text" name="comunidadUbicacion" disabled>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Descripción:</label>
					</div>
					<div class="container__item">
						<input type="text" name="comunidadDescription" disabled>
					</div>
				</div>
			</div>
			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<section>
	<div class="page__main">
		<div class="insert__form">
			<form method="POST" action="{{route('comunidad')}}" name="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
				<div class="form__title">
					<h2>AGREGAR COMUNIDAD </h2>
				</div>

				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="ComunidadId" value="">
				<div class="contenedor__categorias">
					<div class="main__function">
						<h3>Datos Generales </h3>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Nombre: </label>
						</div>
						<div class="container__item">
							<input type="text" name="comunidadName">
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Ubicación:</label>
						</div>
						<div class="container__item">
							<input type="text" name="comunidadUbicacion">
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Jurídico:</label>
						</div>
						<div class="container__item">
							<select name="comunidadTipo">
								<option value="si">si</option>
								<option value="no">no</option>
							</select>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Tipo de Organización:</label>
						</div>
						<div class="container__item">
							<select name="comunidadTipoOrg" id="TipoOrg" onchange="cambia_org()">
								<option value="0" selected>Seleccione... </option>
								<option value="1">Según la ganancia....</option>
								<option value="2">Según la estructura....</option>
								<option value="3">Según la propiedad....</option>
								<option value="4">Según su tamaño....</option>
							</select>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Subtipo de Organización:</label>
						</div>
						<div class="container__item">
							<select id="submenu1" name="comunidadSubmenu1" class="form-control" onchange="llenarInputs1()">
								<option>--</option>
							</select>
							<input type="text" id="submenut1" name="comunidadSubmenut1" style="display: none;"></input>
						</div>
					</div>
					@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje">
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
				</div>
				<div class="contenedor__categorias">
					<div class="main__function">
						<h3>Contacto</h3>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Nombre y Apellido: </label>
						</div>
						<div class="container__item">
							<input type="text" name="ComunidadConNom" required></input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Dirección: </label>
						</div>
						<div class="container__item">
							<input type="text" name="ComunidadConDir" required></input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Teléfono: </label>
						</div>
						<div class="container__item">
							<input type="tel" name="ComunidadConTel" maxlength="10" minlength="10" placeholder=" Digite 10 números  " onkeypress='return event.charCode >= 48 && event.charCode <= 57' required></input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Email: </label>
						</div>
						<div class="container__item">
							<input type="email" name="ComunidadConEmail" required></input>
						</div>
					</div>
				</div>

				<div class="contenedor__categorias">
					<div class="main__function">
						<h3>Población</h3>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Número de hombres: </label>
						</div>
						<div class="container__item">
							<input type="number" name="ComunidadNhombres" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Número de Mujeres: </label>
						</div>
						<div class="container__item">
							<input type="number" name="ComunidadNmujeres" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Población Base: </label>
						</div>
						<div class="container__item">
							<input type="number" name="ComunidadPbase" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Número de Familias: </label>
						</div>
						<div class="container__item">
							<input type="number" name="ComunidadNfamilias" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
						</div>
					</div>
				</div>
				<div class="contenedor__categorias">
					<div class="main__function">
						<h3>Etnicidad</h3>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Mestizos: </label>
						</div>
						<div>
							<input type="number" class="mest" id="mest1" name="ComunidadMestizos" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>%</input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Indígenas: </label>
						</div>
						<div>
							<input type="number" class="indi" id="indi1" name="ComunidadIndigenas" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required readonly>%</input>
						</div>
					</div>
				</div>

				<div class="contenedor__categorias">
					<div class="main__function">
						<h3>Datos Bancarios</h3>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Cédula: </label>
						</div>
						<div class="container__item">
							<input id="target" type="text" name="ComunidadRuc"  maxlength="10" minlength="10" required></input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Nombre del Banco: </label>
						</div>
						<div class="container__item">
							<input type="text" name="ComunidadNBanco" required></input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Cuenta Bancaria: </label>
						</div>
						<div class="container__item">
							<input type="tel" name="ComunidadNumCuen" maxlength="10" placeholder="(Número de cuenta)" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required></input>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Tipo de Cuenta:</label>
						</div>
						<div class="container__item">
							<select id="comunidadState" name="comunidadtypeCuen">
								<option value=""></option>
								<option value="ahorros">Ahorros</option>
								<option value="corriente">Corrinte</option>
							</select>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Observaciones: </label>
						</div>
						<div class="container__item">
							<textarea name="ComunidadObs" rows="3" cols="6" placeholder="Escribe aquí tus comentarios"></textarea>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<input type="text" name="comunidadDescription">
						</div>
					</div>


				</div>
				<div class="form__button">
					<div class="button__save">
						<input type="submit" value="Agregar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>
			</form>
		</div>

	</div>

</section>


<main>
	<div class="page__main1">
		<div class="main__link">
			<a href="{{route('directivas')}}">Directivas</a>
			<a href="{{route('infosenso1')}}">Información Censal I</a>
			<a href="{{route('infosenso2')}}">Información Censal II</a>
			<!-- <a href="{{route('bcodatos')}}">Banco de Datos</a> -->



		</div>

		<div class="main__title">
			<h3>INFORMACIÓN DE LA COMUNIDAD</h3>
			<hr>
		</div>
		

		<div class="main__content">
			<h4>Comunidades existentes </h4>
			<div class="main__boton">
				<a class="insert"><i class="fa fa-plus"> </i>Nuevo</a>
				&nbsp;
				<a href="{{route('downloadComunidad')}}" class="pdf" title="pdf">
					<i class="fa fa-file-pdf-o" aria-hidden="true"> </i>Generar PDF
				</a>
			</div>
			<table class="content__table" id="comunidad">
				<thead class="table__head">
					<tr>
						<th>Nombre Comunidad</th>
						<th>Ubicación</th>
						<th>Nombre Contacto</th>
						<th>Teléfono</th>
						<th>Email</th>
						<th>Cédula</th>
						<th>Opciones</th>

					</tr>
				</thead>
				<tbody>
					@forelse($category as $categoryData )
					<tr class="data__info" data-id="{{$categoryData->comunidad_id}}" data-name="{{$categoryData->comunidad_name}}" data-ubicacion="{{$categoryData->comunidad_ubicacion}}" data-description="{{$categoryData->comunidad_description}}" data-state="{{$categoryData->comunidad_state}}" data-type_sociedad="{{$categoryData->comunidad_type_sociedad}}" data-type_organizacion="{{$categoryData->comunidad_type_organizacion}}" data-type_sub_organizacion="{{$categoryData->comunidad_type_suborganizacion}}" data-cont_name="{{$categoryData->comunidad_cont_name}}" data-cont_direccion="{{$categoryData->comunidad_cont_direccion}}" data-cont_tel="{{$categoryData->comunidad_cont_tel}}" data-cont_email="{{$categoryData->comunidad_cont_email}}" data-num_hom="{{$categoryData->comunidad_num_hom}}" data-num_muj="{{$categoryData->comunidad_num_muj}}" data-num_poblacion="{{$categoryData->comunidad_num_poblacion}}" data-num_familias="{{$categoryData->comunidad_num_familias}}" data-num_mestizos="{{$categoryData->comunidad_num_mestizos}}" data-num_indigenas="{{$categoryData->comunidad_num_indigenas}}" data-ruc="{{$categoryData->comunidad_ruc}}" data-name_banco="{{$categoryData->comunidad_name_banco}}" data-num_cuenta="{{$categoryData->comunidad_num_cuenta}}" data-type_cuenta="{{$categoryData->comunidad_type_cuenta}}" data-observaciones="{{$categoryData->comunidad_observaciones}}" data-description="{{$categoryData->comunidad_description}}">
						<td>{{$categoryData->comunidad_name}}</td>
						<td>{{$categoryData->comunidad_ubicacion}}</td>
						<td>{{$categoryData->comunidad_cont_name}}</td>
						<td>{{$categoryData->comunidad_cont_tel}}</td>
						<td>{{$categoryData->comunidad_cont_email}}</td>
						<td>{{$categoryData->comunidad_ruc}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							@if (Auth::user()->user_type <= 3) <a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								@endif
							
						</td>
					</tr>
					@empty
					<tr>
						<td></td>
						<td class="table__msj"> ! No hay categorías registradas...</td>
						<td></td>
					</tr>
					@endforelse

				</tbody>
			</table>
		</div>
	</div>
	</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">
	$("#target").blur(function() {



		numero = document.getElementById('target').value;
		/* alert(numero); */

		var suma = 0;
		var residuo = 0;
		var pri = false;
		var pub = false;
		var nat = false;
		var numeroProvincias = 22;
		var modulo = 11;
		/* Verifico que el campo no contenga letras */
		var ok = 1;
		for (i = 0; i < numero.length && ok == 1; i++) {
			var n = parseInt(numero.charAt(i));
			if (isNaN(n)) ok = 0;
		}
		if (ok == 0) {
			alert("No puede ingresar caracteres en el número");
			document.getElementById('target').value = "";
			return false;
		}

		if (numero.length < 10) {
			alert('El número de cédula ingresado no es válido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Los primeros dos digitos corresponden al codigo de la provincia */
		provincia = numero.substr(0, 2);
		if (provincia < 1 || provincia > numeroProvincias) {
			alert('El código de la provincia (dos primeros dígitos) es inválido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Aqui almacenamos los digitos de la cedula en variables. */
		d1 = numero.substr(0, 1);
		d2 = numero.substr(1, 1);
		d3 = numero.substr(2, 1);
		d4 = numero.substr(3, 1);
		d5 = numero.substr(4, 1);
		d6 = numero.substr(5, 1);
		d7 = numero.substr(6, 1);
		d8 = numero.substr(7, 1);
		d9 = numero.substr(8, 1);
		d10 = numero.substr(9, 1);

		/* El tercer digito es: */
		/* 9 para sociedades privadas y extranjeros   */
		/* 6 para sociedades publicas */
		/* menor que 6 (0,1,2,3,4,5) para personas naturales */

		if (d3 == 7 || d3 == 8) {
			alert('El tercer dígito ingresado es inválido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Solo para personas naturales (modulo 10) */
		if (d3 < 6) {
			nat = true;
			p1 = d1 * 2;
			if (p1 >= 10) p1 -= 9;
			p2 = d2 * 1;
			if (p2 >= 10) p2 -= 9;
			p3 = d3 * 2;
			if (p3 >= 10) p3 -= 9;
			p4 = d4 * 1;
			if (p4 >= 10) p4 -= 9;
			p5 = d5 * 2;
			if (p5 >= 10) p5 -= 9;
			p6 = d6 * 1;
			if (p6 >= 10) p6 -= 9;
			p7 = d7 * 2;
			if (p7 >= 10) p7 -= 9;
			p8 = d8 * 1;
			if (p8 >= 10) p8 -= 9;
			p9 = d9 * 2;
			if (p9 >= 10) p9 -= 9;
			modulo = 10;
		}

		/* Solo para sociedades publicas (modulo 11) */
		/* Aqui el digito verficador esta en la posicion 9, en las otras 2 en la pos. 10 */
		else if (d3 == 6) {
			pub = true;
			p1 = d1 * 3;
			p2 = d2 * 2;
			p3 = d3 * 7;
			p4 = d4 * 6;
			p5 = d5 * 5;
			p6 = d6 * 4;
			p7 = d7 * 3;
			p8 = d8 * 2;
			p9 = 0;
		}

		/* Solo para entidades privadas (modulo 11) */
		else if (d3 == 9) {
			pri = true;
			p1 = d1 * 4;
			p2 = d2 * 3;
			p3 = d3 * 2;
			p4 = d4 * 7;
			p5 = d5 * 6;
			p6 = d6 * 5;
			p7 = d7 * 4;
			p8 = d8 * 3;
			p9 = d9 * 2;
		}

		suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
		residuo = suma % modulo;

		/* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
		digitoVerificador = residuo == 0 ? 0 : modulo - residuo;

		/* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
		if (pub == true) {
			if (digitoVerificador != d9) {
				alert('El ruc de la empresa del sector público es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			/* El ruc de las empresas del sector publico terminan con 0001*/
			if (numero.substr(9, 4) != '0001') {
				alert('El ruc de la empresa del sector público debe terminar con 0001');
				document.getElementById('target').value = "";
				return false;
			}
		} else if (pri == true) {
			if (digitoVerificador != d10) {
				alert('El ruc de la empresa del sector privado es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			if (numero.substr(10, 3) != '001') {
				alert('El ruc de la empresa del sector privado debe terminar con 001');
				document.getElementById('target').value = "";
				return false;
			}
		} else if (nat == true) {
			if (digitoVerificador != d10) {
				alert('El número de cédula de la persona natural es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			if (numero.length > 10 && numero.substr(10, 3) != '001') {
				alert('El ruc de la persona natural debe terminar con 001');
				document.getElementById('target').value = "";
				return false;
			}
		}
		return true;

	});


	var carrera = document.querySelector('#TipoOrg');
	var materia = document.querySelector('#submenu1');
	var op1 = new Array("-", "Con fines de Lucro", "Sin fines de Lucro", "con fines administrativos")
	var op2 = new Array("-", "Formales", "Informales")
	var op3 = new Array("-", "Privadas", "Publicas")
	var op4 = new Array("-", "Pequeña", "Mediana", "Grande")
	var todasProvincias = [
		[],
		op1,
		op2,
		op3,
		op4,
	];

	function cambia_org() {
		//tomo el valor del select del pais elegido 
		var pais

		pais = document.f1.TipoOrg[document.f1.TipoOrg.selectedIndex].value

		//miro a ver si el pais está definido 
		if (pais != 0) {
			//si estaba definido, entonces coloco las opciones de la provincia correspondiente. 
			//selecciono el array de provincia adecuado 
			mis_provincias = todasProvincias[pais]
			//calculo el numero de provincias 
			num_provincias = mis_provincias.length
			//marco el número de provincias en el select 
			document.f1.submenu1.length = num_provincias

			//para cada provincia del array, la introduzco en el select 
			for (i = 0; i < num_provincias; i++) {
				document.f1.submenu1.options[i].value = mis_provincias[i]
				document.f1.submenu1.options[i].text = mis_provincias[i]

			}
		} else {
			//si no había provincia seleccionada, elimino las provincias del select 
			document.f1.submenu1.length = 1

			//coloco un guión en la única opción que he dejado 
			document.f1.submenu1.options[0].value = "-"
			document.f1.submenu1.options[0].text = "-"

		}
		//marco como seleccionada la opción primera de provincia 
		document.f1.submenu1.options[0].selected = true

	}


	var op11 = new Array("-", "Con fines de Lucro", "Sin fines de Lucro", "Con Fines Administrativos")
	var op21 = new Array("-", "Formales", "Informales")
	var op31 = new Array("-", "Privadas", "Publicas")
	var op41 = new Array("-", "Pequeña", "Mediana", "Grande")
	var todasProvincias = [
		[],
		op11,
		op21,
		op31,
		op41,
	];

	function cambia_org1() {
		//tomo el valor del select del pais elegido 
		var pais

		pais = document.f2.TipoOrg[document.f2.TipoOrg.selectedIndex].value

		//miro a ver si el pais está definido 
		if (pais != 0) {
			//si estaba definido, entonces coloco las opciones de la provincia correspondiente. 
			//selecciono el array de provincia adecuado 
			mis_provincias = todasProvincias[pais]
			//calculo el numero de provincias 
			num_provincias = mis_provincias.length
			//marco el número de provincias en el select 

			document.f2.submenu.length = num_provincias
			//para cada provincia del array, la introduzco en el select 
			for (i = 0; i < num_provincias; i++) {

				document.f2.submenu.options[i].value = mis_provincias[i]
				document.f2.submenu.options[i].text = mis_provincias[i]

			}
		} else {
			//si no había provincia seleccionada, elimino las provincias del select 

			document.f2.submenu.length = 1
			//coloco un guión en la única opción que he dejado 

			document.f2.submenu.options[0].value = "-"
			document.f2.submenu.options[0].text = "-"
		}
		//marco como seleccionada la opción primera de provincia 

		document.f2.submenu.options[0].selected = true

	}


	document.getElementById("mest1").onchange = function() {
		alerta()
	};

	function alerta() {
		let inputValue = document.getElementById("mest1").value;
		document.getElementById('indi1').value = 100 - inputValue;
		console.log(inputValue)
	};

	document.getElementById("mest").onchange = function() {
		alerta1()
	};

	function alerta1() {
		let inputValue = document.getElementById("mest").value;
		document.getElementById('indi').value = 100 - inputValue;
		console.log(inputValue)
	};

	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();

		cargarFormulario(datos);
		$('.page__delete').slideToggle();
	});
	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();

		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});
	$('.insert').on('click', function(event) {
		event.preventDefault();
		$('.page__main').slideToggle();
	});


	function llenarInputs() {
		console.log("llega")
		nombreSelec = document.getElementById('submenu');

		nombre = document.getElementById('submenut');

		nombre.value = nombreSelec.options[nombreSelec.selectedIndex].text;
		console.log(nombreSelec.options[nombreSelec.selectedIndex].text)
		nombre.disabled = false;

	}

	function llenarInputs1() {
		console.log("llega1")
		nombreSelec = document.getElementById('submenu1');

		nombre = document.getElementById('submenut1');

		nombre.value = nombreSelec.options[nombreSelec.selectedIndex].text;
		console.log(nombreSelec.options[nombreSelec.selectedIndex].text)
		nombre.disabled = false;

	}

	function cargarFormulario(datos) {
		console.log(datos)
		$('.action__form')[0].reset();
		$(".action__form input[name=comunidadId]").val(datos['id']);
		$(".action__form input[name=comunidadName]").val(datos['name']);
		$(".action__form input[name=comunidadUbicacion]").val(datos['ubicacion']);
		$(".action__form select[name=comunidadTipo]").val(datos['type_sociedad']);
		$(".action__form select[name=comunidadTipoOrg").val(datos['type_organizacion']);
		$(".action__form select[name=comunidadSubmenu]").val(datos['type_sub_organizacion']);
		$(".action__form input[name=comunidadSubmenut]").val(datos['type_sub_organizacion']);
		$(".action__form input[name=ComunidadConNom]").val(datos['cont_name']);
		$(".action__form input[name=ComunidadConDir]").val(datos['cont_direccion']);
		$(".action__form input[name=ComunidadConTel]").val(datos['cont_tel']);
		$(".action__form input[name=ComunidadConEmail]").val(datos['cont_email']);
		$(".action__form input[name=ComunidadNhombres]").val(datos['num_hom']);
		$(".action__form input[name=ComunidadNmujeres]").val(datos['num_muj']);
		$(".action__form input[name=ComunidadPbase]").val(datos['num_poblacion']);
		$(".action__form input[name=ComunidadNfamilias]").val(datos['num_familias']);
		$(".action__form input[name=ComunidadMestizos]").val(datos['num_mestizos']);
		$(".action__form input[name=ComunidadIndigenas]").val(datos['num_indigenas']);
		$(".action__form input[name=ComunidadRuc]").val(datos['ruc']);
		$(".action__form input[name=ComunidadNBanco]").val(datos['name_banco']);
		$(".action__form input[name=ComunidadNumCuen]").val(datos['num_cuenta']);
		$(".action__form select[name=comunidadtypeCuen]").val(datos['type_cuenta']);
		$(".action__form textarea[name=ComunidadObs]").val(datos['observaciones']);
		$(".action__form input[name=comunidadDescription]").val(datos['description']);

	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
		$('#form__insert')[0].reset();
		$('#action__form')[0].reset();


	});

	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#comunidad').DataTable({
			"ordering": false,
			"info": false,
			"paging": false
		});
	});
</script>
@stop