@extends('layouts.admin')

@section('title')Proyectos @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">


@stop
@section('main')




<section>
	<div class="page__update">
		<div class="form__title">
			<h3>Actualizar Proyecto</h3>
		</div>
		<form method="POST" action="{{route('proyectosupd')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="ProId" value="">
			<div class="contenedor__categorias">

				<h3>Datos Proyecto</h3>
				<div class="form__container">
					<div class="container__label">
						<label>Nombre del Proyecto: </label>
					</div>
					<div class="container__item">
						<input type="text" name="NombrePro" required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Tipo Proyecto: </label>
					</div>
					<div class="container__item">

						<select name="typepro">
							<option value=""></option>
							@foreach($typepro as $type)
							<option value="{{$type->proyectos_type_id}}">{{$type->proyectos_type_description}}</option>
							@endforeach
						</select>

					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label>Objetivo: </label>
					</div>
					<div class="container__item">
						<input type="text" name="ObjetivoPro" required>
					</div>
				</div>
				<hr>
				<h3>Fechas Estimadas</h3>
				<div class="form__container">
					<div class="container__label">
						<label>Fecha Inicio actividades: </label>
					</div>
					<div class="container__item">
						<input type="date" id="finia" name="fechaini" required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label>Fecha Fin actividades: </label>
					</div>
					<div class="container__item">
						<input type="date" id="ffina" name="fechafin" onchange="getValueInput()" required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Meses: </label>
					</div>
					<div class="container__item">
						<input type="text" class="valorda" id="valueMeses" name="valueMeses" Readonly>
					</div>
				</div>
				<hr>
				<h3>Requisitos</h3>

				<div class="form__container">
					<div class="container__label">
						<label for="">Estado: </label>
					</div>
					<div class="container__item">
						<select name="estadoreq" id="estadoreq">
							<option value="" selected>Elija nuevo Parametro</option>
							<option value="No Iniciado">No Iniciado</option>
							<option value="En Proceso">En Proceso</option>
							<option value="Caducado">Caducado</option>
							<option value="Abandonado">Abandonado</option>
							<option value="Culminado">Culminado</option>
						</select>
					</div>
				</div>
				<hr>
				<h3>Financiamiento</h3>
				<div class="form__container">
					<div class="container__label">
						<label for="">Monto: </label>
					</div>
					<div class="container__item">
						<input type="number" name="montodinero" id="montodinero" required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Acumulado: </label>
					</div>
					<div class="container__item">
						<input type="number" name="acumdinero" id="acumdinero" required>
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Estado: </label>
					</div>
					<div class="container__item">
						<select name="estadodinero" id="estadodinero">
							<option value="" selected>Elija nuevo Parametro</option>
							<option value="Recaudado">Recaudado</option>
							<option value="Solicitado">Solicitado</option>
							<option value="Rechazado">Rechazado</option>
							<option value="Aprobado">Aprobado</option>
						</select>
					</div>
				</div>
				<hr>
				<h3>Encargado:</h3>
				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre: </label>
					</div>
					<div class="container__item">

						<select name="idencargados">
							@foreach($encargados as $encar)
							<option value="{{$encar->personals_id}}">{{$encar->personal_name}} {{$encar->personal_last_name}}</option>
							@endforeach
						</select>

					</div>
				</div>
				<h3>Comunidad:</h3>
				<div class="form__container">
					<div class="container__label">
						<label for="">Comunidades Beneficiadas: </label>
					</div>
					<div class="container__item">
						<select name="seleccom[]" id="seleccom" multiple="">
							@foreach($comu as $cmd)
							<option value="{{$cmd->comunidad_id}}" require>{{$cmd->comunidad_name}}</option>
							@endforeach
						</select>
					</div>
				</div>


			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" name="validar" value="Actualizar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<section>
	<div class="page__main">
		<div class="form__title">
			<h2>Agregar Proyecto</h2>
		</div>
		<div class="insert__form">

			<form method="POST" action="{{route('proyectosadd')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="ProId" value="">

				<div class="contenedor__categorias">

					<h3>Datos Proyecto</h3>
					<div class="form__container">
						<div class="container__label">
							<label>Nombre del Proyecto: </label>
						</div>
						<div class="container__item">
							<input type="text" name="NombrePro" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Tipo Proyecto: </label>
						</div>
						<div class="container__item">

							<select name="typepro">
								<option value=""></option>
								@foreach($typepro as $type)
								<option value="{{$type->proyectos_type_id}}">{{$type->proyectos_type_description}}</option>
								@endforeach
							</select>

						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Objetivo: </label>
						</div>
						<div class="container__item">
							<input type="text" name="ObjetivoPro" required>
						</div>
					</div>
					<hr>
					<h3>Fechas Estimadas</h3>
					<div class="form__container">
						<div class="container__label">
							<label>Fecha Inicio actividades: </label>
						</div>
						<div class="container__item">
							<input type="date" id="fainia" name="fechaini" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Fecha Fin actividades: </label>
						</div>
						<div class="container__item">
							<input type="date" id="fafina" name="fechafin" onchange="getValueInputAdd()" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Meses: </label>
						</div>
						<div class="container__item">
							<input type="text" class="valorda" id="valueMesesA" name="valueMeses" Readonly>
						</div>
					</div>
					<hr>
					<h3>Requisitos</h3>
					<div class="form__container">
						<div class="container__label">
							<label for="">Cumple Requisitos: </label>
						</div>
						<div class="container__item">
							<input type="checkbox" name="chkcumple" id="" value="chkcumple" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Número de Informes totales del Proyecto:</label>
						</div>
						<div class="container__item">
							<input type="number" name="numinf" id="" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Estado: </label>
						</div>
						<div class="container__item">
							<select name="estadoreq" id="estadoreq">
								<option value=""></option>
								<option value="No Iniciado">No Iniciado</option>
								<option value="En Proceso">En Proceso</option>
								<option value="Caducado">Caducado</option>
								<option value="Abandonado">Abandonado</option>
								<option value="Culminado">Culminado</option>
							</select>
						</div>
					</div>
					<hr>
					<h3>Financiamiento</h3>
					<div class="form__container">
						<div class="container__label">
							<label for="">Monto: </label>
						</div>
						<div class="container__item">
							<input type="number" name="montodinero" id="montodinero" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Acumulado: </label>
						</div>
						<div class="container__item">
							<input type="number" name="acumdinero" id="acumdinero" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Observación: </label>
						</div>
						<div class="container__item">
							<input type="text" name="observaciondinero" id="observaciondinero" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Estado: </label>
						</div>
						<div class="container__item">
							<select name="estadodinero" id="estadodinero">
								<option value=""></option>
								<option value="Recaudado">Recaudado</option>
								<option value="Solicitado">Solicitado</option>
								<option value="Rechazado">Rechazado</option>
								<option value="Aprobado">Aprobado</option>
							</select>
						</div>
					</div>
					<hr>
					<h3>Encargado:</h3>
					<div class="form__container">
						<div class="container__label">
							<label for="">Nombre: </label>
						</div>
						<div class="container__item">

							<select name="idencargados">
								<option value=""></option>
								@foreach($encargados as $encar)
								<option value="{{$encar->personals_id}}">{{$encar->personal_name}} {{$encar->personal_last_name}}</option>
								@endforeach
							</select>

						</div>
					</div>
					<h3>Comunidad:</h3>
					<div class="form__container">
						<div class="container__label">
							<label for="">Comunidades Beneficiadas: </label>
						</div>
						<div class="container__item">
							<select name="seleccom[]" id="seleccom" multiple="">
								@foreach($comu as $cmd)
								<option value="{{$cmd->comunidad_id}}">{{$cmd->comunidad_name}}</option>
								@endforeach
							</select>
						</div>
					</div>

				</div>

				<div class="form__button">
					<div class="button__save">
						<input type="submit" name="validar" value="Agregar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>

			</form>


		</div>
	</div>
</section>

<section>
	<div class="page__delete">

		<form method="POST" action="{{route('proyectosdlt')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="ProId" value="">
			<div class="contenedor__categorias">

				<h3>Eliminar Proyecto</h3>
				<div class="form__container">
					<div class="container__label">
						<label>Nombre del Proyecto: </label>
					</div>
					<div class="container__item">
						<input type="text" name="NombrePro" required readonly readonly>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label>Objetivo: </label>
					</div>
					<div class="container__item">
						<input type="text" name="ObjetivoPro" required readonly>
					</div>
				</div>
				<hr>
				<h3>Fechas Estimadas</h3>
				<div class="form__container">
					<div class="container__label">
						<label>Fecha Inicio actividades: </label>
					</div>
					<div class="container__item">
						<input type="date" id="finia" name="fechaini" required readonly>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label>Fecha Fin actividades: </label>
					</div>
					<div class="container__item">
						<input type="date" id="ffina" name="fechafin" onchange="getValueInput()" required readonly>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Meses: </label>
					</div>
					<div class="container__item">
						<input type="text" class="valorda" id="valueMeses" name="valueMeses" Readonly>
					</div>
				</div>
				<hr>
				<h3>Requisitos</h3>

				<div class="form__container">
					<div class="container__label">
						<label for="">Estado: </label>
					</div>
					<div class="container__item">
						<select name="estadoreq" id="estadoreq" disabled>
							<option value="" selected>Elija nuevo Parametro</option>
							<option value="No Iniciado">No Iniciado</option>
							<option value="En Proceso">En Proceso</option>
							<option value="Caducado">Caducado</option>
							<option value="Abandonado">Abandonado</option>
							<option value="Culminado">Culminado</option>
						</select>
					</div>
				</div>
				<hr>
				<h3>Financiamiento</h3>
				<div class="form__container">
					<div class="container__label">
						<label for="">Monto: </label>
					</div>
					<div class="container__item">
						<input type="number" name="montodinero" id="montodinero" required readonly>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Acumulado: </label>
					</div>
					<div class="container__item">
						<input type="number" name="acumdinero" id="acumdinero" required readonly>
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Estado: </label>
					</div>
					<div class="container__item">
						<select name="estadodinero" id="estadodinero" disabled>
							<option value="" selected>Elija nuevo Parametro</option>
							<option value="Recaudado">Recaudado</option>
							<option value="Solicitado">Solicitado</option>
							<option value="Rechazado">Rechazado</option>
							<option value="Aprobado">Aprobado</option>
						</select>
					</div>
				</div>


			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" name="validar" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>



	</div>
</section>

<main>
	<div class="page__main1">
		<div class="main__link">
			<a href="{{route('encargados')}}">Encargados Proyectos</a>
		</div>
		<div class="main__title">
			<h3>PROYECTOS DEL {{$management->management_area_name}}</h3>
			<hr>
			<br>
		</div>
		<div class="main__content">
			<h4>Proyectos Existentes </h4>
			<div class="main__boton">
				<a class="insert"><i class="fa fa-plus"></i>Nuevo</a>
				&nbsp;
				<a href="{{route('downloadProyectos')}}" class="pdf" title="Generar Reporte"><i class="fa fa-file-pdf-o" aria-hidden="true">
					</i>Generar Informe</a>
			</div>
			<br>
			<br>
			<br>

			<table class="content__table" id="user1">
				<thead class="table__head">
					<tr>
						<th>Nombre</th>
						<th>Fecha Inicio</th>
						<th>Fecha Fin</th>
						<th>Tiempo Estimado</th>
						<th>Presupuesto</th>
						<th>Encargado</th>
						<th>Comunidades</th>
						<th>Acciones</th>
						<th>Registrar</th>
						<th>Establecer</th>
					</tr>
				</thead>


				<tbody>

					@foreach($presupuesto as $PresData)
					@endforeach
					@foreach($pres as $pr)
					@endforeach
					@foreach($proyec as $CltProyecto)
					@endforeach
					@foreach($proyec as $ProyData)
					<tr class="data__info" data-id-proyecto="{{$CltProyecto->proyectos_id}}" data-name="{{$CltProyecto->proyectos_name}}" data-fechaini="{{$CltProyecto->proyectos_fechaini}}" data-fechafin="{{$CltProyecto->proyectos_fechafin}}" data-objetivo="{{$CltProyecto->proyectos_objetivo}}" data-financiamiento="{{$CltProyecto->proyectos_id_financiamiento}}" data-meses="{{$CltProyecto->proyectos_nmeses}}" data-estadoreq="{{$CltProyecto->proyectos_estado}}" data-typepro="{{$CltProyecto->type_id}}" data-estadodinero="{{$PresData->presupuesto_estado}}" @forelse($pres as $pr) @if($pr->proyecto_id==$CltProyecto->proyectos_id)
						data-montodin="{{$pr->presupuesto_monto}}"
						data-acumdin="{{$pr->presupuesto_acumulado}}"
						@endif
						@endforeach
						@forelse($encpro as $encproy)
						@if($encproy->proyecto_id==$CltProyecto->proyectos_id)
						data-personalEnc="{{$encproy->personals_id}}"
						@endif
						@endforeach
						@forelse($comu as $cmd)
						@forelse($beneficiados as $bnf)
						@if($bnf->proyecto_id==$CltProyecto->proyectos_id && $bnf->comunidad_id==$cmd->comunidad_id)
						data-seleccom={{$cmd->comunidad_id}}
						@endif
						@endforeach
						@endforeach
						>


						<td>{{$ProyData->proyectos_name}}</td>
						<td>{{$ProyData->proyectos_fechaini}}</td>
						<td>{{$ProyData->proyectos_fechafin}}</td>
						<td>{{$ProyData->proyectos_nmeses}} Meses</td>
						@forelse($pres as $pr)
						@if($pr->proyecto_id==$ProyData->proyectos_id)
						<td>{{$pr->presupuesto_monto}}</td>
						@endif
						@endforeach
						@forelse($encpro as $encproy)
						@if($encproy->proyecto_id==$ProyData->proyectos_id)
						<td>{{$encproy->personal_name}} {{$encproy->personal_last_name}}</td>
						@endif
						@endforeach
						@forelse($comu as $cmd)
						@forelse($beneficiados as $bnf)
						@if($bnf->proyecto_id==$ProyData->proyectos_id && $bnf->comunidad_id==$cmd->comunidad_id)
						<td style="display: block;">{{$cmd->comunidad_name}}</td>
						@endif
						@endforeach
						@endforeach
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							<a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
							<form class="pdf" method="get" action="{{route('downloadProyectosid')}}">
								<input type="hidden" name="ProId" value="">
								<input type="submit" name="pdfbtn" class="btn btn-success" value="PDF">
							</form>
						</td>
						<td>
							<form class="avance" method="get" action="{{route('avance')}}">
								<input type="hidden" name="ProId" value="">
								<input type="submit" name="avancebtn" class="btn btn-danger" value="Avance">
							</form>
						</td>
						<td>
							<form class="convenio" method="get" action="{{route('convenio')}}">
								<input type="hidden" name="ProId" value="">
								<input type="submit" name="conveniobtn" class="btn btn-warning" value="Convenio">
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>

			</table>
			<br><br><br>

		</div>
</main>

@stop

@section('scripts')
@parent


<script type="text/javascript">
	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();

		cargarFormulario(datos);
		$('.page__delete').slideToggle();
	});

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		console.log(datos);
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	// $('.resource').on('click', function(event) {
	// 	event.preventDefault();
	// 	var datos = $(this).closest('.data__info').data();
	// 	$('.page__resource').slideToggle();
	// 	$('#form__resource h1').append(datos['alias']);
	// 	$(".action__form input[name=newsId]").val(datos['id']);
	// });
	$('.insert').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		$('.page__main').slideToggle();

	});

	$('.btn-danger').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);

		$('.avance').submit();

	});


	$('.btn-success').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);

		$('.pdf').submit();

	});



	$('.btn-warning').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);

		$('.convenio').submit();

	});







	function cargarFormulario(datos) {
		$('.action__form')[0].reset();
		$(".action__form input[name=ProId]").val(datos['idProyecto']);
		$(".avance input[name=ProId]").val(datos['idProyecto']);
		$(".convenio input[name=ProId]").val(datos['idProyecto']);
		$(".pdf input[name=ProId]").val(datos['idProyecto']);
		$(".action__form input[name=NombrePro]").val(datos['name']);
		$(".action__form input[name=fechaini]").val(datos['fechaini']);
		$(".action__form input[name=fechafin]").val(datos['fechafin']);
		$(".action__form input[name=ObjetivoPro]").val(datos['objetivo']);
		$(".action__form input[name=avancePro]").val(datos['avance']);
		$(".action__form input[name=PresuPro]").val(datos['financiamiento']);
		$(".action__form input[name=valueMeses]").val(datos['meses']);
		$(".action__form select[name=estadoreq]").val(datos['estadoreq']);
		$(".action__form input[name=montodinero]").val(datos['montodin']);
		$(".action__form input[name=acumdinero]").val(datos['acumdin']);
		$(".action__form select[name=estadodinero").val(datos['estadodinero']);
		$(".action__form select[name=idencargados").val(datos['personalenc']);
		$(".action__form select[id=seleccom").val(datos['seleccom']);
		$(".action__form select[name=typepro").val(datos['typepro']);

	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
		$('#form__insert')[0].reset();
		$('#action__form')[0].reset();
	});



	$('#mensaje').fadeOut(5000);

	$('input.fecha').datepicker({
		dateFormat: 'yy-mm-dd'
	});



	const getValueInput = () => {
		let dateTo = document.getElementById("finia").value;
		let dateFrom = document.getElementById("ffina").value;
		var fechaInicio = new Date(dateTo);
		var fechaFin = new Date(dateFrom);
		var diff = fechaFin - fechaInicio;
		var dias = diff / (1000 * 60 * 60 * 24);
		var meses = parseInt((dias % 365) / 30)

		$("#valueMeses").val(meses);


	}

	const getValueInputAdd = () => {
		let dateTo = document.getElementById("fainia").value;
		let dateFrom = document.getElementById("fafina").value;
		var fechaInicio = new Date(dateTo);
		var fechaFin = new Date(dateFrom);
		var diff = fechaFin - fechaInicio;
		var dias = diff / (1000 * 60 * 60 * 24);
		var meses = parseInt((dias % 365) / 30)

		$("#valueMesesA").val(meses);


	}
</script>
<script>
	$(document).ready(function() {
		$('#user').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});

	$(document).ready(function() {
		$('#user1').DataTable({
			"ordering": true,
			"info": false,
			"paging": false,

		});
	});
	$(document).ready(function() {
		$('#u1').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});
</script>
<script>
	function valor() {
		let municipio = document.getElementById("dirin").value;
		//Se actualiza en municipio inm
		document.getElementById("miemin").value = municipio;
	}
</script>


@stop