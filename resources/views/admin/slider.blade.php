@extends('layouts.admin')

@section('title') Slider @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<form method="POST" action="{{route('updateSlider')}}" class="action__form" enctype= multipart/form-data>
			<div class="form__title">
				<h3>Modificar contendido del Slider</h3>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="sliderId" value="">
			
			<div class="form__container">
				<div class="container__label">
					<label for="">Nombre: </label>
				</div>
				<div class="container__item">
					<input type="text" name="sliderName">
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Descripcion: </label>
				</div>
				<div class="container__item">
					<input type="text" name="sliderDescription">
				</div>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Imagen: </label>
				</div>
				<div class="container__item">
					<input type="file" name="sliderImage">
				</div>					
			</div>

			<div class="form__button">
				<div class="button__save">						
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section> 

<section>
	<div class="page__delete" >
		<form method="POST" action="{{route('deleteSlider')}}" class="action__form" enctype= multipart/form-data >	
			<div class="form__title">
				<h3>Eliminar Slider</h3>
			</div>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="sliderId" >

			<div class="form__container">
				<div class="container__label">
					<label for="">Nombre: </label>
				</div>
				<div class="container__item">
					<input type="text" name="sliderName"  disabled>
				</div>					
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Descripción: </label>
				</div>
				<div class="container__item">
					<input type="text" name="sliderDescription" disabled>
				</div>
			</div>	
			<div class="form__container">
				<div class="container__label">
					<label for="">Imagen: </label>
				</div>
				<div class="container__item">
					
					<input type="label"  name="sliderDescription1" disabled> 
				</div>					
			</div>

			<div class="form__button">
				<div class="button__save">						
					<input type="submit" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>
		</form>
	</div>
</section>


<main>
	<div class="page__main">
		<div class="main__title">
			<h3>CONTENIDO SLIDER</h3>
		</div>
		<hr><br>

		<div class="main__insert">
			<div class="main__function">
				<h4>Agregar imagen a Slider</h4>
			</div>	
			<div class="insert__form">
				<form method="POST" action="{{route('slider')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

					<div class="form__container">
						<div class="container__label">
							<label for="">Nombre: </label>
						</div>
						<div class="container__item">
							<input type="text" name="sliderName" >
						</div>					
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Descripcion: </label>
						</div>
						<div class="container__item">
							<textarea name="sliderDescription" rows="3"></textarea>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Imagen: </label>
						</div>
						<div class="container__item">
							<input type="file" name="sliderImage" >
						</div>					
					</div>
					@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje">
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">						
							<input type="submit" value="Agregar">
						</div>
						<div class="button__cancel">
							<input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="main__content">
			<br>
			<h4>Recursos Slider Existentes </h4>
			<hr><br>
			<table  class="content__table" id="slider">
				<thead class="table__head">
					<tr>
						<th>Nombre</th>
						<th>Descripcion</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($social as $slider )
					<tr class="data__info" 
					data-id="{{$slider->slider_id}}"
					data-nombre="{{$slider->slider_name}}"
					data-descripcion="{{$slider->slider_description}}"
					data-imagen="{{$slider->slider_image}}">
					<td>{{$slider->slider_name}}</td>
					<td>{{$slider->slider_description}}</td>
					<td>
						<a href="{{ route('sliderData',[$slider->slider_id]) }}" title="visualizar" href=""><i class="fa fa-search" aria-hidden="true"></i></a>
						<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						{{-- @if (Auth::user()->user_type != 1) --}}
						<a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
						{{-- @endif --}}

					</td>
					</tr>
					@empty
					<tr >
						<td ></td>
						<td class="table__msj"> ! No hay sliders registrados...</td>
						<td ></td>
					</tr>
					@endforelse

				</tbody>
		</table> 
		<br><br>
	</div>
</div>
</main>
@stop

@section('scripts')
@parent


<script type="text/javascript">

	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__delete').slideToggle();
	});

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update').slideToggle();//muestra un emergente
	});

	function cargarFormulario(datos){
		$('.action__form')[0].reset();
		$(".action__form input[name=sliderId]").val(datos['id']);
		$(".action__form input[name=sliderName]").val(datos['nombre']);
		$(".action__form input[name=sliderDescription]").val(datos['descripcion']);	
		$(".action__form input[name=sliderDescription1]").val(datos['imagen']);
		
	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update ').slideToggle();
		$('#form__insert')[0].reset();

	});

	$('#mensaje').fadeOut(5000);
//slider time
	$(document).ready(function() {
					$('#slider').DataTable({
						responsive: true,
		   				scrollX: true,
		  				autoWidth: false,
						"language": {"url": "{{ asset ('DataTables-1.10.13/languaje.json')}}"},
						columnDefs: [
            				{targets: 2, bSortable: false},
        				],
        				lengthMenu: [5, 10,15],
					});
				} );

</script>
@stop 
