@extends('layouts.admin')

@section('title') Tarifas @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<form method="POST" action="{{route('updateRate')}}" class="action__form" enctype= multipart/form-data>
			<div class="form__title">
				<h1>Ingreso de Tarifas </h1>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="categoryId" value="">
			<div class="form__container">
				<div class="container__label">
					<label for="">Descripción Tarifa: </label>
				</div>
				<div class="container__item">
					<input type="text" name="rateDescription"  >
				</div>					
			</div>
			
            <div class="form__container">
				<div class="container__label">
					<label for="">Valor Tarifa: </label>
				</div>
				<div class="container__item">
					<input type="number" name="rateValue"  step="0.01">
				</div>					
			</div>
			

			<div class="form__container">
				<div class="container__label">
					<label for="">Programa:</label>
				</div>
				<div class="container__item">
					<select  name="rateProgram" id="rateProgram" >
						<option value="" >

						</option>
						@forelse($option1 as $curricularModule)
						<option value="{{$curricularModule->program_id}}">
							{{$curricularModule->program_description}}
						</option>
						@empty
						<option value="">No existen programas</option>
						@endforelse
				</select>
				</div>
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Tarifa:</label>
				</div>
				<div class="container__item">
					<select  name="rateType" id="rateType" >
						<option value="" >

						</option>
						@forelse($option2 as $type)
						<option value="{{$type->type_rate_id}}">
							{{$type->type_rate_description}}
						</option>
						@empty
						<option value="">No existen tarifas</option>
						@endforelse
				</select>
				</div>
			</div>


			<div class="form__button">
				<div class="button__save">						
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
	<div class="page__main">
		<div class="main__title">
			<h1>Gestión de Tarifas</h1>
		</div>

		<div class="main__insert">
			<div class="main__function">
				<h2>Agregar</h2>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('rate')}}" class="action__form" id="form__insert">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="form__container">
				<div class="container__label">
					<label for="">Descripción Tarifa: </label>
				</div>
				<div class="container__item">
					<input type="text" name="rateDescription"  >
				</div>					
			</div>
			
            <div class="form__container">
				<div class="container__label">
					<label for="">Valor Tarifa: </label>
				</div>
				<div class="container__item">
					<input type="number" name="rateValue" step="0.01" min="1">
				</div>					
			</div>
			

			<div class="form__container">
				<div class="container__label">
					<label for="">Programa:</label>
				</div>
				<div class="container__item">
					<select  name="rateProgram" id="rateProgram" >
						<option value="" >

						</option>
						@forelse($option1 as $curricularModule)
						<option value="{{$curricularModule->program_id}}">
							{{$curricularModule->program_description}}
						</option>
						@empty
						<option value="">No existen horarios</option>
						@endforelse
				</select>
				</div>
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Tarifa:</label>
				</div>
				<div class="container__item">
					<select  name="rateType" id="rateType" >
						<option value="" >

						</option>
						@forelse($option2 as $type)
						<option value="{{$type->type_rate_id}}">
							{{$type->type_rate_description}}
						</option>
						@empty
						<option value="">No existen tarifas</option>
						@endforelse
				</select>
				</div>
			</div>
		

				
				@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje" >
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">						
							<input type="submit" value="Agregar">
						</div>
						<div class="button__cancel">
							<input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="main__content">
			<h2>Tarifas Existentes</h2>
			<table  class="content__table" id="category">
				<thead class="table__head">
					<tr>
						<th>Descripción</th>
						<th>Valor</th>
                        <th>Programa</th>
                        <th>Tipo de Tarifa</th>
                        <th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($category as $categoryData )
					<tr class="data__info" 
					data-id="{{$categoryData->rate_program_id}}"
					data-desc="{{$categoryData->rate_program_description}}"
					data-value="{{$categoryData->rate_program_value}}"
					data-prog="{{$categoryData->rate_program_program}}"
                    data-type="{{$categoryData->rate_program_type_rate}}"
                    >
					<td>{{$categoryData->rate_program_description}}</td>
					<td>{{$categoryData->rate_program_value}}</td>
					<td>
                    @foreach($option1 as $parentName2)
							@if($parentName2->program_id == $categoryData->rate_program_program)
								{{$parentName2->program_description}}
							@endif
						@endforeach
					</td>
                    <td>
                    @foreach($option2 as $parentName1)
							@if($parentName1->type_rate_id == $categoryData->rate_program_type_rate)
								{{$parentName1->type_rate_description}}
							@endif
						@endforeach
					</td>
					<td>
						<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					</td>
				</tr>
				@empty
				<tr >
					<td  colspan="5" class="table__msj"> ! No hay tarifas...</td>
				</tr>
				@endforelse

			</tbody>
		</table>
	</div>
</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	function cargarFormulario(datos){
		$('.action__form')[0].reset();
		$(".action__form input[name=categoryId]").val(datos['id']);
		$(".action__form input[name=rateDescription]").val(datos['desc']);
		$(".action__form input[name=rateValue]").val(datos['value']);
        $(".action__form input[name=rateProgram]").val(datos['prog']);
        $(".action__form input[name=rateType]").val(datos['type']);
	
    }

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource').slideToggle();
		$('#form__insert')[0].reset();
	});

	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#category').DataTable({
			"ordering": false,
			"info":     false,
			"paging":   false
		});
	} );
	
</script>
@stop