@extends('layouts.admin')

@section('title') Categorías @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<div class="form__title">
			<h1>Actualizar Miembro</h1>
		</div>
		<form method="POST" action="{{route('infosenso2')}}" class="action__form" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="directivaId" value="">
			<input type="hidden" name="miembroId" value="">


			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Actualizar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>
		</form>
	</div>
</section>

<section>
	<div class="page__main">
		<div class="form__title">
			<h2>Agregar Informacion</h2>
		</div>
		<div class="insert__form">

			<form method="POST" action="{{route('directivasing')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="directivaId" value="">



				<div class="form__button">
					<div class="button__save">
						<input type="submit" name="validar" value="Validar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>
			</form>


		</div>
	</div>
</section>

<section>
	<div class="page__delete">
		<form method="POST" action="{{route('directivasdel')}}" class="action__form" enctype="multipart/form-data">


			<div class="form__button">
				<div class="button__save">
					<input type="submit" name="eliminar" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>
		</form>
	</div>
</section>

<main>
	<div class="page__main1">
		<div class="main__link">
			<a href="{{route('directivas')}}">Directivas</a>
			<a href="{{route('infosenso1')}}">Información Censal I</a>
			<a href="{{route('infosenso2')}}">Información Censal II</a>
			<!-- <a href="{{route('bcodatos')}}">Banco de Datos</a> -->
		</div>
		<div class="main__title">
			<h3>Informacion de Censo </h3>
			<hr><br>
		</div>
		<div class="main__content">
			<h4> Datos Censal II </h4>


			<div class="card-body">
				<form method="post" action="{{route('addinfosen2')}}" name="frmfil" id="frmfil" class="action__form" enctype=multipart/form-data>

					<div class="row mb-3">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="col-md-3">
							<select class="form-control status_id" name="idcomunidad" required>
								<option value="">Seleccione</option>
								@foreach($comunidad as $NameComunidad)
								<option value="{{$NameComunidad->comunidad_id}}">{{$NameComunidad->comunidad_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-3">
							<select class="form-control status_id" id="tipoinfo" name="tipoinfo" required>
								<option value="">Seleccione</option>
								@foreach($tiposen as $Tipsen)
								<option value="{{$Tipsen->Tiposinfocenso_id}}">{{$Tipsen->Tiposinfocenso_description}}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-1">
							<input type="submit" id="Buscar" class="btn btn-danger" value="Buscar" />
						</div>
					</div>
				</form>
			</div>
			<form method="post" action="{{route('enviarinf2')}}" name="frmfil" id="frmfil" class="action__form" enctype=multipart/form-data>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				@if($auxi==1)
				<table class="content__table" id="user1">
					<thead class="table__head">
						<tr>
							<th>Tipo</th>
							<th>Parametro</th>
							<th>Valor</th>
							<th>Opciones</th>
						</tr>
					</thead>
					@foreach($infoco as $Datainf)
					<input type="hidden" name="idComunidad" value="{{$Datainf->comunidad_id}}">
					<H2>{{$Datainf->comunidad_name}}</H2>
					@endforeach
					<tbody>
						<tr>
							@foreach($tipo as $Infcomu)
							@endforeach
							<input type="hidden" name="idTipo" value="{{$Infcomu->infocenso2_id_Tipos}}">
							<td>{{$Infcomu->Tiposinfocenso_description}}</td>
							<td>
								<select class="form-control status_id" name="idparam">
									<option value="" selected>Seleccione</option>
									@foreach($tipo as $Infcomu)
									<option value="{{$Infcomu->infocenso2_id}}">{{$Infcomu->infocenso2_description}}</option>
									@endforeach
								</select>
							</td>
							<td><input type="number" name="valoring"></td>
							<td>
								<div class="col-xs-6">
									<input class="btn btn-success" type="submit" Value="Ok">

								</div>
							</td>
						</tr>
					</tbody>
				</table>
				@endif
				@if($auxi==0)
				<table class="content__table" id="user1">
					<thead class="table__head">
						<tr>
							<th>Tipo</th>
							<th>Parametro</th>
							<th>Valor</th>
							<th>Opciones</th>
						</tr>
					</thead>
					@foreach($infoco as $Datainf)
					<input type="hidden" name="idComunidad" value="{{$Datainf->comunidad_id}}">
					<H2>{{$Datainf->comunidad_name}}</H2>
					@endforeach
					<tbody>

					</tbody>
				</table>
				@endif

			</form>
			<br>
			<br>
			<br>

			<table class="content__table" id="user">
				<thead class="table__head">
					<tr>
						<th>Comunidad</th>
						<th>Parametro</th>
						<th>Subparametro</th>
						<th>Valor</th>
						<!-- <th>Acciones</th> -->
					</tr>
				</thead>
				<tbody>



					@forelse($infc as $ifs)
					@endforeach
					@forelse($senin as $infs)
					@endforeach
					@forelse($senin as $infs)

					<tr class="data__info" data-infocenso-id="{{$infs->infoc2_id}}" data-id-comunidad="{{$infs->infoc2_id_comunidad}}" data-id-tipo="{{$infs->infoc2_id_tipoinfcensal}}" data-parametro="{{$infs->infoc2_infocensalparam}}" data-valor="{{$infs->infoc2_valor}}" data-descripcion="{{$ifs->infocenso2_description}}">

						@foreach($infc as $ifs)
						@if($infs->infoc2_infocensalparam==$ifs->infocenso2_id)
						<td> {{$infs->comunidad_name}} </td>
						@endif
						@endforeach
						@foreach($infc as $ifs)
						@if($infs->infoc2_infocensalparam==$ifs->infocenso2_id)
						<td>{{$ifs->Tiposinfocenso_description}}</td>
						@endif
						@endforeach
						@foreach($infc as $ifs)
						@if($infs->infoc2_infocensalparam==$ifs->infocenso2_id)
						<td>{{$ifs->infocenso2_description}}</td>
						@endif
						@endforeach
						@foreach($infc as $ifs)
						@if($infs->infoc2_infocensalparam==$ifs->infocenso2_id)
						<td> {{$infs->infoc2_valor}} </td>
						@endif
						@endforeach



						<!-- 
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							<a class="delete" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
						</td> -->



					</tr>
					@endforeach










				</tbody>
			</table>
			<br><br><br>
		</div>
</main>
@stop


@section('scripts')
@parent



<script type="text/javascript">
	$('#mensaje').fadeOut(5000);

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		console.log(datos);
		$('.page__update').slideToggle();
	});
	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		console.log(datos)
		$('.page__delete').slideToggle();
	});

	$('.insert').on('click', function(event) {
		event.preventDefault();
		$('.page__main').slideToggle();

	});

	$('.validar').on('click', function(event) {
		event.preventDefault();

	});

	/*window.onload = function() {
        alert("Hello!");
    }*/


	(function() {
		const forma = document.getElementById("f1");
		forma.addEventListener("submit", function(event) {
			event.preventDefault();
			var form = document.getElementById("f1"); //document.querySelector("f1");
			var datos = new FormData(form);
			console.log(form)
			var init = {
				method: form.method,
				body: datos
			};
			fetch(form.action, init)
				.then(function(response) {
					//document.getElementById("miembroform").style.display = "block";
					document.getElementById("ffin").disabled = false;
					document.getElementById("fini").disabled = false;
					document.getElementById("sex").disabled = false;
					document.getElementById("carg").disabled = false;
					document.getElementById("nom").disabled = false;


				})
				.catch(function(error) {
					//document.getElementById("miembroform").style.display = "none";
					console.log('Hubo un problema con la petición Fetch:' + error.message);
				});

		});

	})();





	function cargarFormulario(datos) {
		console.log(datos)
		$('.action__form')[0].reset();
		$(".action__form select[name=comuniname]").val(datos['comunidadName']);
		$(".action__form input[name=miembroId]").val(datos['miembroid']);
		$(".action__form select[name=comunidadId]").val(datos['comunidadId']);
		$(".action__form input[name=directivaId]").val(datos['directivaId']);
		$(".action__form input[name=NombreM]").val(datos['nombre']);
		$(".action__form input[name=CargoM]").val(datos['cargo']);
		$(".action__form input[name=fechafin]").val(datos['fechafin']);
		$(".action__form input[name=fechaini]").val(datos['fechaini']);

	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
		$('#form__insert')[0].reset();
		$('#action__form')[0].reset();
	});





	$('#mensaje').fadeOut(5000);
</script>
<script>
	$(document).ready(function() {
		$('#user').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});

	$(document).ready(function() {
		$('#user1').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});
	$(document).ready(function() {
		$('#u1').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});
</script>
<script>
	function valor() {
		let municipio = document.getElementById("dirin").value;
		//Se actualiza en municipio inm
		document.getElementById("miemin").value = municipio;
	}
</script>


@stop