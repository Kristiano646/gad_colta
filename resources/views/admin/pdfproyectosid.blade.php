<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style> 
        .titulo{ 
            font-size: 12px;
            border: .5px solid black;
            padding: 0 10px;
        } 
        .container{ 
            margin: 0 0 0 -24px;
            font-size: 10px;  
        }
        .titulo-1{
            gap: 5px;
            width: 40%;
            height: 18%;
            float: left;
            margin: 10px;
            padding: 0;
            
            
        }
        .sub{
            border: .5px solid black;
            width: 100%; 
            height: 13%;
            padding-left: 5px;
            
        }
    </style>
    
</head>

<body>
    <div class="ed-item main-center cross-center base-1-3 web-10">
        <img id="fsp-blanco" src="img/logos/encabezado-colta.png" alt="logo" width="725px" height="100px">
        
    </div>
    <main>
        @foreach($presu as $PresData)
        @endforeach

        @foreach($presu as $presData)
        @endforeach

        @forelse($encproy as $enc)
        <br>
        <h6 style="text-align:center;">INFORMACIÓN DEL PROYECTO <hr></h6>

        <h6>Datos informativos del proyecto</h6>
        <div class="titulo">
            <p><span><b>Proyecto:</b>  </span> {{$pro->proyectos_name}} </p>
            <p><span><b>Tipo de Proyecto:</b>  </span> </p>
            <p><span><b>Objetivo:</b>  </span> {{$pro->proyectos_objetivo}}</p>
            @forelse($presuproy as $prepr)
                @if($pro->proyectos_id==$prepr->proyectos_id)
                    <p><span><b>Presupuesto:</b>  </span> {{$prepr->presupuesto_monto}} dólares <span><b>     Estado del proyectp: </b></span></p>
                @endif
            @endforeach
            
        </div>
        <div class="container">
 
                <div class="col titulo-1">
                    <h6>Fechas estimadas</h6>
                    <div class="sub">
                        <p > <span><b>Fecha de Inicio: </b>  </span>{{$pro->proyectos_fechaini}}</p>
                        <p > <span><b>Fecha de finalizacion: </b>  </span>{{$pro->proyectos_fechafin}}</p>
                        <p > <span><b>Tiempo Estimado: </b>  </span>{{$pro->proyectos_nmeses}} Meses</p>

                    </div>
                </div>
                
                <div class="titulo-1">
                    <h6>Comunidades beneficiadas</h6>
                    <div class="sub">
                    
						<p > <span><b>&nbsp; </b>  </span> </p>
						
                        
                    </div> 
                </div>
                
                <div class="titulo-1">
                    <h6>Encargado del proyecto</h6>
                    <div class="sub">
                    @forelse($encproy as $enp)
                        @if($pro->proyectos_id==$enp->proyecto_id)
                            <p > <span><b>Nombre:</b>  </span> {{$enp->personal_name}} {{$enp->personal_last_name}}</p>
                            <p > <span><b>Teléfono: </b>  </span> {{$enp->personal_tlf}}</p>
                            <p > <span><b>Email: </b>  </span>{{$enp->personal_email}} </p>
                        @endif
                    @endforeach
                    </div>
                </div>
            
        </div>
        @empty
        @endforelse
        <h6>Avances del Proyecto</h6>
        <p style="font-size:9pt;">&nbsp;Para realizar los avances de proyecto y llegar a su culminación se necesita subir un total de <b>{{$pro->proyectos_numInf}}</b> informes. </p>
        <div class="main__content">
            <table class="table table-striped">
                <thead class="table-primary">
                    <tr style="font-size:10pt;">
                        <th>Fecha Informe</th>
                        <th>Observacion</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($proy as $pry)
                    @endforeach
                    @foreach($avance as $avc)
                    @if($avc->avance_proyecto_id == $id)
                    <tr style="font-size:9pt;">
                        <td>{{$avc->avance_fechainforme}}</td>
                        <td>{{$avc->avance_observacion}}</td>
                        <td>{{$avc->avance_estado}}%</td>

                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>

        <h6>Convenios realizados</h6>
        <table class="table table-striped">
            <thead class="table-primary">
                <tr style="font-size:10pt;">
                    <th>Garantia</th>
                    <th>Monto Garantia</th>
                    <th>Tipo Garantia</th>
                    <th>Fecha de vencimiento de Garantia</th>
                    
                    <th>Aporte Municipio</th>
                    <th>Aporte Comunidad</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>


                @foreach($proyectos as $pry)
                @endforeach
                @foreach($proy as $cnv)
                @if($cnv->convenios_id_proyecto != null)
                <tr class="data__info" data-id-proyecto="{{$pry->proyectos_id}}" data-name="{{$pry->proyectos_name}}" data-fechaini="{{$cnv->convenios_fech_ini}}" data-fechafin="{{$cnv->convenios_fech_final}}" data-aportemuni="{{$cnv->convenios_aporte_municipio}}" data-aportecomu="{{$cnv->convenios_aporte_comunidad}}" style="font-size:9pt;">
                    @foreach($proyectos as $pry)
                    @if($pry->proyectos_id == $cnv->convenios_id_proyecto)
                    
                    @endif
                    @endforeach
                    @foreach($encproy as $pro)
                    @endforeach
                    @if($pro->personal_id == $cnv->convenios_id_encargado)
                    @endif
                    @foreach($costo as $cost)
                    @endforeach
                    @foreach($garantia as $gar)
                    @endforeach
                    @if($gar->garantia_proyecto_id == $pry->proyectos_id)
                    <td>{{$gar->garantia_observacion}}</td>
                    <td>{{$gar->garantia_monto}}</td>
                    <td>{{$gar->garantia_id_tipo}}</td>
                    <td>{{$gar->garantia_fechven}}</td>
                    <td>{{$cnv->convenios_aporte_municipio}}</td>
                    <td>{{$cnv->convenios_aporte_comunidad}}</td>
                    <td>{{$cnv->convenios_sal}}</td>
                    @endif
                    
                    
                    
                </tr>
                @else
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>¡No existe ningún convenio. !!!</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @endif
                @endforeach
            </tbody>

        </table>






    </main>
</body>

</html>