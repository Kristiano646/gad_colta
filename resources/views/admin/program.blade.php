@extends('layouts.admin')

@section('title') Proyectos GAD Colta @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">

		<form method="POST" action="{{route('updateProgram')}}" class="action__form" enctype=multipart/form-data>
			<div class="form__title">
				<h1>Modificar Proyectos GAD Cantón Colta</h1>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="categoryId" value="">
			<div class="form__container">
				<div class="container__label">
					<label for="">Nombre: </label>
				</div>
				<div class="container__item">
					<input type="text" name="programDescription">
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Proyecto:</label>
				</div>
				<div class="container__item">
					<select name="programType" id="programType">
						<option value="">

						</option>
						@forelse($option4 as $type)
						<option value="{{$type->type_program_id}}">
							{{$type->type_program_description}}
						</option>
						@empty
						<option value="">No existen tipos de proyecto</option>
						@endforelse
					</select>
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Categoria de Proyecto:</label>
				</div>
				<div class="container__item">
					<select name="programCategoryProgram" id="programCategoryProgram">
						<option value="">

						</option>
						@forelse($option3 as $catprog)
						<option value="{{$catprog->category_program_id}}">
							{{$catprog->category_program_description}}
						</option>
						@empty
						<option value="">No existen categorías de proyecto</option>
						@endforelse
					</select>
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
				<label for="">Objetivo: </label>
				</div>
				<div class="container__item">
					<textarea name="programObjective" rows="5" id="programObjective"></textarea>
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Versión (Solo número): </label>
				</div>
				<div class="container__item">
					<input type="text" name="programVersion">
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Duración (En meses): </label>
				</div>
				<div class="container__item">
					<input type="text" name="programDuration">
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Fecha Inicio: </label>
				</div>
				<div class="container__item">
					<input type="date" name="programBegin">
				</div>
			</div>


			<div class="form__container">
				<div class="container__label">
					<label for="">Fecha Fin: </label>
				</div>
				<div class="container__item">
					<input type="date" name="programEnd">
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Encargado:</label>
				</div>
				<div class="container__item">
					<select name="programCoordinator" >
					
						@forelse($option2 as $coordinator)
						<option value="{{$coordinator->coordinator_program_id}}">
							{{$coordinator->coordinator_program_name}}
						</option>
						@empty
						<option value="">No existen Encargado</option>
						@endforelse
					</select>

				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Plan Operativo Anual:</label>
				</div>
				<div class="container__item">
					<select name="programCurriculum" id="programCurriculum">
						<option value="">

						</option>
						@forelse($option1 as $curricular)
						<option value="{{$curricular->curriculum_program_id}}">
							{{$curricular->curriculum_program_description}}
						</option>
						@empty
						<option value="">No existen Plan Operativo Anual</option>
						@endforelse
					</select>
				</div>
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Estado del Proyecto:</label>
				</div>
				<div class="container__item">
					<select name="programState" id="programState">
						<option value="Iniciado">
							Iniciado
						</option>
						<option value="En Ejecución">
							En Ejecución
						</option>
						<option value="Culminado">
							Culminado
						</option>
					</select>
				</div>
			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>


		</form>

	</div>
</section>

<main>
	<div class="page__main">
		<div class="main__title">
			<h1>Gestión de Proyectos GAD Cantón COLTA </h1>
		</div>

		<div class="main__insert">
			<div class="main__function">
				<h2>Agregar nueva Entrada: </h2>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('program')}}" class="action__form" id="form__insert">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

					<div class="form__container">
						<div class="container__label">
							<label for="">Nombre: </label>
						</div>
						<div class="container__item">
							<input type="text" name="programDescription">
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Tipo de Proyecto:</label>
						</div>
						<div class="container__item">
							<select name="programType" id="programType">
								<option value="">

								</option>
								@forelse($option4 as $type)
								<option value="{{$type->type_program_id}}">
									{{$type->type_program_description}}
								</option>
								@empty
								<option value="">No existen tipos de proyectos</option>
								@endforelse
							</select>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Categoria de Proyecto:</label>
						</div>
						<div class="container__item">
							<select name="programCategory" id="programCategory">
								<option value="">

								</option>
								@forelse($option3 as $catprog)
								<option value="{{$catprog->category_program_id}}">
									{{$catprog->category_program_description}}
								</option>
								@empty
								<option value="">No existen categorías de proyecto</option>
								@endforelse
							</select>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Objetivo: </label>
						</div>
						<div class="container__item">
							<textarea name="programObjective" rows="5" id="programObjective"></textarea>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Versión (Solo número): </label>
						</div>
						<div class="container__item">
							<input type="text" name="programVersion">
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Duración (En meses): </label>
						</div>
						<div class="container__item">
							<input type="text" name="programDuration">
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Fecha Inicio: </label>
						</div>
						<div class="container__item">
							<input type="date" name="programBegin">
						</div>
					</div>


					<div class="form__container">
						<div class="container__label">
							<label for="">Fecha Fin: </label>
						</div>
						<div class="container__item">
							<input type="date" name="programEnd">
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Encargado:</label>
						</div>
						<div class="container__item">
							<select name="programCoordinator" id="programCoordinator">
								<option value="">

								</option>
								@forelse($option2 as $coordinator)
								<option value="{{$coordinator->coordinator_program_id}}">
									{{$coordinator->coordinator_program_name}}
								</option>
								@empty
								<option value="">No existen Encargados</option>
								@endforelse
							</select>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Tipo de Plan Operativo Anual:</label>
						</div>
						<div class="container__item">
							<select name="programCurriculum" id="programCurriculum">
								<option value="">

								</option>
								@forelse($option1 as $curricular)
								<option value="{{$curricular->curriculum_program_id}}">
									{{$curricular->curriculum_program_description}}
								</option>
								@empty
								<option value="">No existen Plan Opertivo Anual</option>
								@endforelse
							</select>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Estado del Proyecto:</label>
						</div>
						<div class="container__item">
							<select name="programState" id="programState" require>
							<option value="En Ejecución">
									
								</option>
								<option value="En Ejecución">
									En Ejecución
								</option>
								<option value="Culminado">
									Culminado
								</option>
								<option value="Ofertado">
									Ofertado
								</option>
							</select>
						</div>
					</div>
					@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje">
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">
							<input type="submit" value="Agregar">
						</div>
						<div class="button__cancel">
							<input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="main__content">
			<h2>Proyecto Existentes</h2>
			<table class="content__table" id="category">
				<thead class="table__head">
					<tr>
						<th>Nombre</th>
						<th>Tipo de Proyecto</th>
						<th>Categoría de Proyecto</th>
						<th>Objetivo</th>
						<th>Versión</th>
						<th>Duración</th>
						<th>Fecha Inicio</th>
						<th>Fecha Fin</th>
						<th>Encargado</th>
						<th>Plan Operativo Anual</th>
						<th>Estado</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($category as $categoryData)

					<tr class="data__info" data-id="  {{$categoryData->program_id}}" data-nombre="{{$categoryData->program_description}}" data-descripcion="{{$categoryData->program_type_program}}" data-cat="{{$categoryData->program_category_program}}" data-obj="{{$categoryData->program_objective}}" data-version="{{$categoryData->program_version}}" data-duration="{{$categoryData->program_duration}}" data-dtbegin="{{$categoryData->program_date_begin}}" data-dtend="{{$categoryData->program_date_end}}" data-state="{{$categoryData->program_state}}" data-coordinatorpro="{{$categoryData->program_coordinator_program}}" data-curriculumpro="{{$categoryData->program_curriculum_program}}">

						<td>{{$categoryData->program_description}}</td>
						<td>
							@foreach($option4 as $parentName4)
							@if($parentName4->type_program_id == $categoryData->program_type_program)
							{{$parentName4->type_program_description}}
							@endif
							@endforeach
						</td>
						<td>
							@foreach($option3 as $parentName3)
							@if($parentName3->category_program_id == $categoryData->program_category_program)
							{{$parentName3->category_program_description}}
							@endif
							@endforeach
						</td>
						<td>{{$categoryData->program_objective}}</td>
						<td>{{$categoryData->program_version}}</td>
						<td>{{$categoryData->program_duration}}</td>
						<td>{{$categoryData->program_date_begin}}</td>
						<td>{{$categoryData->program_date_end}}</td>
						<td>
							@foreach($option2 as $parentName2)
							@if($parentName2->coordinator_program_id == $categoryData->program_coordinator_program)
							{{$parentName2->coordinator_program_name}}
							@endif
							@endforeach
						</td>
						<td>
							@foreach($option1 as $parentName1)
							@if($parentName1->curriculum_program_id == $categoryData->program_curriculum_program)
							{{$parentName1->curriculum_program_description}}
							@endif
							@endforeach
						</td>
						<td>{{$categoryData->program_state}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							<a class="delete" title="eliminar"><i class="fa fa-trash" aria-hidden="true"></i></a>
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="5" class="table__msj"> ! No hay programas ingresados...</td>
					</tr>
					@endforelse

				</tbody>
			</table>
		</div>
	</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">
	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	function cargarFormulario(datos) {
		console.log(datos);
		$('.action__form')[0].reset();
		$(".action__form input[name=programId]").val(datos['id']);
		$(".action__form input[name=programDescription]").val(datos['nombre']);
		$(".action__form select[name=programType]").val(datos['descripcion']);

		$(".action__form select[name=programCategoryProgram]").val(datos['cat']);
		$(".action__form textarea[name=programObjective]").val(datos['obj']);
		$(".action__form input[name=programVersion]").val(datos['version']);


		$(".action__form input[name=programDuration]").val(datos['duration']);
		$(".action__form input[name=programBegin]").val(datos['dtbegin']);
		$(".action__form input[name=programEnd]").val(datos['dtend']);


		$(".action__form select[name=programState]").val(datos['state']);
		$(".action__form select[name=programCoordinator]").val(datos['coordinatorpro']);
		$(".action__form select[name=programCurriculum]").val(datos['curriculumpro']);

	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource').slideToggle();
		$('#form__insert')[0].reset();
	});

	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#category').DataTable({
			"ordering": false,
			"info": false,
			"paging": false
		});
	});
</script>
@stop