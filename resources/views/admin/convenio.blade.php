@extends('layouts.admin')

@section('title') Convenios @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">


@stop
@section('main')
<section>
    <div class="page__delete">

        <form method="POST" action="{{route('dltavance')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="avcId" value="">
            <input type="hidden" name="ProId" value=>
            <div class="contenedor__categorias">

                <h6>Eliminar Avance</h6>
                <div class="form__container">
                    <div class="container__label">
                        <label>Fecha de Avance: </label>
                    </div>
                    <div class="container__item">
                        <input type="date" id="finia" name="finia" readonly>
                    </div>
                </div>


                <div class="form__container">
                    <div class="container__label">
                        <label for="">Observacion: </label>
                    </div>
                    <div class="container__item">
                        <input type="text" name="observacionavance" readonly>
                    </div>
                </div>



            </div>

            <div class="form__button">
                <div class="button__save">
                    <input type="submit" name="validar" value="Eliminar">
                </div>
                <div class="button__cancel">
                    <input type="button" class="cancel__btn" value="Cancelar">
                </div>
            </div>

        </form>



    </div>
</section>

<section>
    <div class="page__update">
        <div class="form__title">
            <h1>Actualizar Avance</h1>
        </div>
        <form method="POST" action="{{route('actavance')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="avcId" value="">
            <input type="hidden" name="ProId" value=>
            <input type="hidden" name="porcentaje" value=>
            <div class="contenedor__categorias">


                <div class="form__container">
                    <div class="container__label">
                        <label>Fecha de Avance: </label>
                    </div>
                    <div class="container__item">
                        <input type="date" id="finia" name="finia">
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Informe de Avance:</label>
                    </div>
                    <div class="container__item">
                        <input type="file" name="Infav" accept=".doc, .docx, .pdf" class="custom-file-input" required>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label for="">Observacion: </label>
                    </div>
                    <div class="container__item">
                        <input type="text" name="observacionavance">
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label for="">Estado: </label>
                    </div>
                    <div class="container__item">

                        <input type="range" min="0" max="100" value=disabled>
                        <output id="rangevalue" class="labelout"></output>
                    </div>
                </div>




            </div>

            <div class="form__button">
                <div class="button__save">
                    <input type="submit" name="validar" value="Actualizar">
                </div>
                <div class="button__cancel">
                    <input type="button" class="cancel__btn" value="Cancelar">
                </div>
            </div>

        </form>
    </div>
</section>

<section>
    <div class="page__main">
        <div class="form__title">
            <h3>Registar Nuevo Convenio </h3>
        </div>
        <form method="POST" action="{{route('addconvenio')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            

            <div class="contenedor__categorias">
            
                @foreach($proye as $cnv)
                @endforeach
                @foreach($encpro as $pro)
                @endforeach
                @foreach($perproy as $perproye)
                @endforeach
                @foreach($proyec as $pry)
                @foreach($costo as $cost)
                @endforeach
                @foreach($data as $dt)
                @endforeach
                @foreach($dataper as $dtp)
                @endforeach
                @foreach($encarg as $encg)
                @endforeach
                @if($pry->proyectos_id == $dt->proyectos_id)

                <input type="hidden" name="ProId" value="{{$pry->proyectos_id}}">
                @if($dtp->personal_id==$pro->personal_id)
                <input type="hidden" name="PersId" value="{{$pro->personal_id}}">
                @endif
                <input type="hidden" name="costop" id="costop" value="{{$cost->presupuesto_monto}}">
                <input type="hidden" name="saldoi" id="saldoi" value="{{$cnv->convenios_sal}}">
                <h3>Datos del Proyecto</h3>
                <div class="form__container">
                    <div class="container__label">
                        <label>Nombre del Proyecto: </label>
                    </div>
                    <div class="container__item">

                        <input type="text" value="{{$pry->proyectos_name}}" readonly>

                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Nombre del Encargado: </label>
                    </div>
                    <div class="container__item">
                        @if($dtp->personal_id==$pro->personal_id)
                        <input type="text" value="{{$pro->personal_name}} {{$pro->personal_last_name}}" readonly>
                        @endif
                    </div>
                </div>
                @endif
                @endforeach
                <div class="form__container">
                    <div class="container__label">
                        <label>Fecha de Inicio: </label>
                    </div>
                    <div class="container__item">
                        <input type="date" id="finia" name="fechaini" value="{{$pry->proyectos_fechaini}}" readonly>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Fecha de Fin: </label>
                    </div>
                    <div class="container__item">
                        <input type="date" id="finia" name="fechafin"  value="{{$pry->proyectos_fechafin}}" readonly>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Aporte PRODEPINE USD:</label>
                    </div>
                    <div class="container__item">
                        <input type="number" name="aporteProde" id="aporteProde" required>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Aporte Contri. Comunidad USD:</label>
                    </div>
                    <div class="container__item">
                        <input type="number" name="aporteCom" id="aporteCom" required>
                    </div>
                </div>
                <hr>
                <h3>Plan de Desembolsos</h3>
                <div class="form__container">
                    <div class="container__label">
                        <label>Fecha: </label>
                    </div>
                    <div class="container__item">
                        <input type="date" id="fechdes" name="finia" required>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Desembolso USD:</label>
                    </div>
                    <div class="container__item">
                        <input type="number" name="desemb" id="desemb" onchange="getValueInput()" required>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Acumulado:</label>
                    </div>
                    <div class="container__item">
                        <input type="number" name="acum" id="acum" Readonly>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label for="">Saldo: </label>
                    </div>
                    <div class="container__item">
                        <input type="number" name="saldo" id="saldo" Readonly>
                    </div>
                </div>
                <hr>
                <h3>Garantía</h3>
                <div class="form__container">
                    <div class="container__label">
                        <label for="">Monto: </label>
                    </div>
                    <div class="container__item">
                        <input type="number" name="monto" required>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label for="">Tipo Garantía: </label>
                    </div>
                    <div class="container__item">
                        <select name="tipgarantia">
                            @forelse($tipog as $tipg)
                            <option value="{{$tipg->tipos_garantia_id}}">{{$tipg->tipos_garantia_description}}</option>
                            @empty
                            <option>No existen tipos </option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label>Fecha Vence: </label>
                    </div>
                    <div class="container__item">
                        <input type="date" name="fechven" required>
                    </div>

                </div>
                <div class="form__container">
                    <div class="container__label">
                        <label for="">Observaciones: </label>
                    </div>
                    <div class="container__item">
                        <input type="text" name="observ">
                    </div>
                </div>
            </div>

            <div class="form__button">
                <div class="button__save">
                    <input type="submit" name="validar" value="Agregar">
                </div>
                <div class="button__cancel">
                    <input type="button" class="cancel__btn" value="Cancelar">
                </div>
            </div>

        </form>
    </div>
</section>


<main>

    <div class="page__main1">

        <div class="main__link">
            <a href="{{route('proyectos')}}">Proyectos</a>
            <a href="{{route('encargados')}}">Encargados Proyectos</a>

        </div>

        <div class="main__title">
            <h3>Convenios</h3>
        </div>
        <hr><br>

        <div class="main__insert">

           @empty($cnv)
            <div class="main__boton">
                <a class="insert"><i class="fa fa-plus"></i>Nuevo</a>
            </div>
            @else      
            @if($cnv->convenios_sal=='0')
            <h5 style="text-align: center;">!! Desembolsos completos no es necesario más Convenios</h5>  
            @else
            <div class="main__boton">
                <a class="insert"><i class="fa fa-plus"></i>Nuevo</a>
            </div>
            @endif    
            @endempty 
          
            <div class="insert__form">
                <h3>Convenios Registrados</h3>
                <div class="main__content">

                    <table class="content__table" id="user">
                        <thead class="table__head">
                            <tr>
                                <th>Proyecto</th>
                                <th>Fecha Inicial</th>
                                <th>Fecha Final</th>
                                <th>Encargado</th>
                                <th>Costo</th>
                                <th>Aporte Municipio</th>
                                <th>Aporte Comunidad</th>
                                <th>Saldo</th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach($proyec as $pry)
                            @endforeach
                            @foreach($proye as $cnv)
                            @if($cnv->convenios_id_proyecto != null)
                            <tr class="data__info" data-id-proyecto="{{$pry->proyectos_id}}" data-name="{{$pry->proyectos_name}}" data-fechaini="{{$cnv->convenios_fech_ini}}" data-fechafin="{{$cnv->convenios_fech_final}}" data-aportemuni="{{$cnv->convenios_aporte_municipio}}" data-aportecomu="{{$cnv->convenios_aporte_comunidad}}">
                                @foreach($proyec as $pry)
                                @if($pry->proyectos_id == $cnv->convenios_id_proyecto)
                                <td>{{$pry->proyectos_name}}</td>
                                @endif
                                @endforeach
                                <td>{{$cnv->convenios_fech_ini}}</td>
                                <td>{{$cnv->convenios_fech_final}}</td>
                                @foreach($encpro as $pro)
                                @endforeach
                                @if($pro->personal_id == $cnv->convenios_id_encargado)
                                <td>{{$pro->personal_name}} {{$pro->personal_last_name}}</td>
                                @endif
                                @foreach($costo as $cost)
                                <td>{{$cost->presupuesto_monto}}</td>
                                @endforeach
                                <td>{{$cnv->convenios_aporte_municipio}}</td>
                                <td>{{$cnv->convenios_aporte_comunidad}}</td>
                                <td>{{$cnv->convenios_sal}}</td>
                            </tr>
                            @else
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>¡No hay Registros!!!</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">
    $('.delete').on('click', function(event) {
        event.preventDefault();
        var datos = $(this).closest('.data__info').data();

        cargarFormulario(datos);
        $('.page__delete').slideToggle();
    });

    $('.update').on('click', function(event) {
        event.preventDefault();
        var datos = $(this).closest('.data__info').data();
        console.log(datos)
        cargarFormulario(datos);
        $('.page__update').slideToggle();
    });
    $('.insert').on('click', function(event) {

        $('.page__main').slideToggle();

    });


    const getValueInput = () => {
        let aprtcom = document.getElementById("aporteCom").value;
        let aprtmun = document.getElementById("aporteProde").value;
        let desem = document.getElementById("desemb").value;
        let sal = document.getElementById("saldoi").value;
        let costp = document.getElementById("costop").value;
        var sum = parseFloat(aprtcom) + parseFloat(aprtmun);

        console.log(sal);
        $("#acum").val(sum);

        if (sal != null) {
            var sald = parseFloat(sal) - sum;
            console.log(costp);
            $("#saldo").val(sald);

        } else
            var sald = 0;
        console.log("aqui");
        $("#saldo").val(sald);


    }

    const getValuesaldo = () => {
        let aprtcom = document.getElementById("aporteCom").value;
        let aprtmun = document.getElementById("aporteProde").value;
        let desem = document.getElementById("desemb").value;
        var sum = parseFloat(aprtcom) + parseFloat(aprtmun) + parseFloat(desem);
        $("#acum").val(sum);


    }

    function cargarFormulario(datos) {
        $('.action__form')[0].reset();
        $(".action__form input[name=idProyecto]").val(datos['idProyecto']);




    }
    $('input.cancel__btn').click(function(event) {
        $(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
        $('#form__insert')[0].reset();
        $('#action__form')[0].reset();
    });
    $('#mensaje').fadeOut(5000);
    $('input.fecha').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $(document).ready(function() {
        $('#proyectos').DataTable({
            "ordering": false,
            "info": false,
            "paging": false
        });
    });
    $(document).ready(function() {
        $('#user').DataTable({
            "ordering": true,
            "info": false,
            "paging": false
        });
    });
</script>

@stop