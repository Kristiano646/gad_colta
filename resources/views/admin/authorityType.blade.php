@extends('layouts.admin')

@section('title') Categorías @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<form method="POST" action="{{route('updateAuthorityType')}}" class="action__form" enctype=multipart/form-data>
			<div class="form__title">
				<h1>Modificar Tipo</h1>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="categoryId" value="">

			<div class="form__container">
				<div class="container__label">
					<label for="">Descripción: </label>
				</div>
				<div class="container__item">
					<input type="text" name="authorityDescription">
				</div>
			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>
<section>
	<div class="page__update2">
		<form method="POST" action="{{route('updateAuthorityEstado')}}" class="action__form" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="categoryId" value="">
			<div class="form__title">
				<h1>Modificar Estado</h1>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Descripción: </label>
				</div>
				<div class="container__item">
					<input type="text" name="authorityDescription">
				</div>
			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
	<div class="page__main">
		<div class="main__link">
		<a href="{{route('userType')}}">Usuarios</a>
			<a href="{{route('authorityType')}}">Autoridades</a>
			<a href="{{route('multimediaType')}}">Multimedia</a>
			<a href="{{route('newsType')}}">Noticias</a>
			<a href="{{route('convenioType')}}">Convenios</a>
			<a href="{{route('senso1')}}">Informacion Sensal I</a>
			<a href="{{route('senso2')}}">Informacion Sensal II</a>
			<a href="{{route('proyectosType')}}">Proyectos</a>
		</div>
		<div class="main__title">
			<h1>Tipos de Autoridad</h1>
		</div>

		<div class="main__insert">
			<div class="main__function">
				<h2>Agregar</h2>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('authorityType')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">


					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<input type="text" name="authorityDescription">
						</div>
					</div>
					@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje">
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">
							<input type="submit" value="Agregar">
						</div>

					</div>
				</form>
			</div>
		</div>

		<div class="main__content">
			<h2>Tipos de autoridad Existentes </h2>
			<table class="content__table" id="authority">
				<thead class="table__head">
					<tr>
						<th>Descripción</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($cargo as $authorityCargo)
					<tr class="data__info" data-id="{{$authorityCargo->user_cargo_id}}" data-descripcion="{{$authorityCargo->user_cargo_description}}">
						<td>{{$authorityCargo->user_cargo_description}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="2" class="table__msj">! No hay tipos de autoridades registrados...</td>
					</tr>
					@endforelse

				</tbody>
			</table>
		</div>

		<div class="main__title">
			<h1>Tipos de Estado de Autoridades</h1>
		</div>
		<div class="main__insert">
			<div class="main__function">
				<h2>Agregar</h2>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('authorityEstado')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">


					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<input type="text" name="authorityDescription">
						</div>
					</div>
					@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje">
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">
							<input type="submit" value="Agregar">
						</div>

					</div>
				</form>
			</div>
		</div>
		<div class="main__content">
			<h2>Tipos de autoridad Existentes </h2>
			<table class="content__table" id="authority2">
				<thead class="table__head">
					<tr>
						<th>Descripción</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($estado as $categoryData )
					<tr class="data__info" data-id="{{$categoryData->user_estado_id}}" data-descripcion="{{$categoryData->user_estado_description}}">
						<td>{{$categoryData->user_estado_description}}</td>
						<td>
							<a class="update2" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="2" class="table__msj">! No hay tipos de autoridades registrados...</td>
					</tr>
					@endforelse

				</tbody>
			</table>
		</div>


	</div>


	
</main>

@stop


@section('scripts')
@parent
<script type="text/javascript">
	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});
	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		console.log(datos);
		$('.page__delete').slideToggle();
	});
	$('.update2').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update2').slideToggle();
	});

	function cargarFormulario(datos) {
		$('.action__form')[0].reset();
		$(".action__form input[name=categoryId]").val(datos['id']);
		$(".action__form input[name=authorityDescription]").val(datos['descripcion']);
	}
	$('.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource, .page__update2').slideToggle();
	});

	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#authority').DataTable({
			"ordering": false,
			"info": false,
			"paging": false
		});
	});

	$(document).ready(function() {
		$('#authority2').DataTable({
			"ordering": false,
			"info": false,
			"paging": false
		});
	});
</script>

@stop