@extends('layouts.admin')

@section('title') Avance @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">


@stop
@section('main')
<section>
	<div class="page__delete">

		<form method="POST" action="{{route('dltavance')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="avcId" value="">
            <input type="hidden" name="ProId" value={{$id}}>
			<div class="contenedor__categorias">

				<h3>Eliminar Avance</h3>
                <div class="form__container">
                        <div class="container__label">
                            <label>Fecha de Avance: </label>
                        </div>
                        <div class="container__item">
                            <input type="date" id="finia" name="finia" readonly>
                        </div>
                    </div>

                 
                    <div class="form__container">
                        <div class="container__label">
                            <label for="">Observación: </label>
                        </div>
                        <div class="container__item">
                            <input type="text" name="observacionavance" readonly>
                        </div>
                    </div>
                    <div class="form__container">
                        <div class="container__label">
                            <label for="">Estado: </label>
                        </div>
                        <div class="container__item">

                            <input type="range" min="0" max="100" value={{$porcentaje}} disabled>
                            <output id="rangevalue" class="labelout">{{$porcentaje}}%</output>
                        </div>
                    </div>


			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" name="validar" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>



	</div>
</section>

<section>
	<div class="page__update">
		<div class="form__title">
			<h3>Actualizar Avance</h3>
		</div>
		<form method="POST" action="{{route('actavance')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="avcId" value="">
            <input type="hidden" name="ProId" value={{$id}}>
                    <input type="hidden" name="porcentaje" value={{$porcentaje}}>
			<div class="contenedor__categorias">

				
                <div class="form__container">
                        <div class="container__label">
                            <label>Fecha de Avance: </label>
                        </div>
                        <div class="container__item">
                            <input type="date" id="finia" name="finia" >
                        </div>
                    </div>
                    <div class="form__container">
                        <div class="container__label">
                            <label>Informe de Avance:</label>
                        </div>
                        <div class="container__item">
                            <input type="file" name="Infav" accept=".doc, .docx, .pdf" class="custom-file-input" required>
                        </div>
                    </div>
                    <div class="form__container">
                        <div class="container__label">
                            <label for="">Observación: </label>
                        </div>
                        <div class="container__item">
                            <input type="text" name="observacionavance">
                        </div>
                    </div>
                    <div class="form__container">
                        <div class="container__label">
                            <label for="">Estado: </label>
                        </div>
                        <div class="container__item">

                            <input type="range" min="0" max="100" value={{$porcentaje}} disabled>
                            <output id="rangevalue" class="labelout">{{$porcentaje}}%</output>
                        </div>
                    </div>
                 
                                  


			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" name="validar" value="Actualizar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
    <div class="page__main">

         <div class="main__link">
            <a href="{{route('proyectos')}}">Proyectos</a>
            
            <!-- <a href="{{route('encargados')}}">Encargados Proyectos</a> -->
            
        </div> 

        <div class="main__title">
            <h3>Avances de Proyectos</h3>
        </div>
        <hr><br>

        <div class="main__insert">
            <div class="main__function">
                <h4>Agregar</h4>
            </div>
            <div class="insert__form">
                <form method="POST" action="{{route('addavance')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
                    <input type="hidden" name="ProId" value={{$id}}>
                    <input type="hidden" name="porcentaje" value={{$porcentaje}}>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form__container">
                        <div class="container__label">
                            <label>Fecha de Avance: </label>
                        </div>
                        <div class="container__item">
                            <input type="date" id="finia" name="finia" required>
                        </div>
                    </div>

                    <div class="form__container">
                        <div class="container__label">
                            <label>Informe de Avance:</label>
                        </div>
                        <div class="container__item">
                            <input type="file" name="Infav" accept=".doc, .docx, .pdf" class="custom-file-input">
                        </div>
                    </div>
                    <div class="form__container">
                        <div class="container__label">
                            <label for="">Observación: </label>
                        </div>
                        <div class="container__item">
                            <input type="text" name="observacionavance">
                        </div>
                    </div>
                    <div class="form__container">
                        <div class="container__label">
                            <label for="">Estado: </label>
                        </div>
                        <div class="container__item">

                            <input type="range" min="0" max="100" value={{$porcentaje}} disabled>
                            <output id="rangevalue" class="labelout">{{$porcentaje}}%</output>
                        </div>
                    </div>
                    @if(Session::has('mensaje'))
                    <div class="form__container">
                        <div id="mensaje">
                            {{Session::get('mensaje')}}
                        </div>
                    </div>
                    @endIf
                    @if (count($errors) > 0)
                    <div class="form__container">
                        <ul id="mensaje">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @forelse($proyec as $ProyData)
                    @endforeach
                    <div class="form__button">

                        @if($porcentaje != 100)
                        <div class="button__save">
                            <input type="submit" value="Agregar">
                        </div>
                        @endif
                        @if($porcentaje >= 100)
                        <div class="button__save">
                            <input type="submit" value="Agregar" disabled>
                        </div>
                        @endif
                        <div class="button__cancel">
                            <input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
                        </div>
                    </div>
                </form>

                <div class="espacio">
                    <h4>Avances Registrados</h4>
                </div>
                <hr><br>

                <div class="main__content">
                    <table class="content__table" id="user">
                        <thead class="table__head">
                            <tr>

                                <th>Informe avance</th>
                                <th>Fecha Informe</th>
                                <th>Observación</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach($proy as $pry)
                            @endforeach

                            @foreach($avance as $avc)
                             
                          
                            @if($avc->avance_proyecto_id == $id)
                           
                            <tr class="data__info" 
                            data-id-proyecto="{{$id}}" 
                            data-id-avc="{{$avc->avance_id}}" 
                            data-proyecto-name="{{$pry->proyectos_name}}" 
                            data-avance-doc="{{$avc->avance_doc}}" data-avance-fech="{{$avc->avance_fechainforme}}" data-avance-observacion="{{$avc->avance_observacion}}" data-avance-estado="{{$avc->avance_estado}}">
                                <td>{{$avc->avance_doc}}<a target="_blank" href="{{asset('docs/proyectos/'.$id.'/avance/'.$avc->avance_doc)}}"><i class="fa fa-file-pdf-o"></i></a></td>
                                <td>{{$avc->avance_fechainforme}}</td>
                                <td>{{$avc->avance_observacion}}</td>
                                <td>{{$avc->avance_estado}}%</td>
                                <td>
                                    <a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    @if (Auth::user()->user_type <= 3) <a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        @endif
                                </td>
                           
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                    <br><br>
                </div>
            </div>
        </div>


    </div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">
    $('.delete').on('click', function(event) {
        event.preventDefault();
        var datos = $(this).closest('.data__info').data();
console.log(datos);
        cargarFormulario(datos);
        $('.page__delete').slideToggle();
    });

    $('.update').on('click', function(event) {
        event.preventDefault();
        var datos = $(this).closest('.data__info').data();
        console.log(datos)
        cargarFormulario(datos);
        $('.page__update').slideToggle();
    });

    // $('.resource').on('click', function(event) {
    // 	event.preventDefault();
    // 	var datos = $(this).closest('.data__info').data();
    // 	$('.page__resource').slideToggle();
    // 	$('#form__resource h1').append(datos['alias']);
    // 	$(".action__form input[name=newsId]").val(datos['id']);
    // });
    $('.insert').on('click', function(event) {
        event.preventDefault();
        $('.page__main').slideToggle();

    });




    function cargarFormulario(datos) {
        $('.action__form')[0].reset();
        $(".action__form input[name=avcId]").val(datos['idAvc']);
        $(".action__form input[name=finia]").val(datos['avanceFech']);
        $(".action__form input[name=observacionavance]").val(datos['avanceObservacion']);
       

    }

    $('input.cancel__btn').click(function(event) {
        $(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
        $('#form__insert')[0].reset();
        $('#action__form')[0].reset();
    });


    $('#mensaje').fadeOut(5000);

    $('input.fecha').datepicker({
        dateFormat: 'yy-mm-dd'
    });



    const getValueInput = () => {
        let dateTo = document.getElementById("finia").value;
        let dateFrom = document.getElementById("ffina").value;
        var fechaInicio = new Date(dateTo);
        var fechaFin = new Date(dateFrom);
        var diff = fechaFin - fechaInicio;
        var dias = diff / (1000 * 60 * 60 * 24);
        var meses = parseInt((dias % 365) / 30)

        $("#valueMeses").val(meses);

    }


    $(document).ready(function() {
        $('#proyectos').DataTable({
            "ordering": false,
            "info": false,
            "paging": false
        });
    });

    $(document).ready(function() {
        $('#user').DataTable({
            "ordering": true,
            "info": false,
            "paging": false
        });
    });
</script>

@stop