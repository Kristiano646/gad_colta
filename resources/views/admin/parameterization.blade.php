@extends('layouts.admin')

@section('title') Inicio @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{asset('css/admin.css')}}">
@stop
@section('main')
<main>
	<div class="page__main">

		
		<div class="main__title">
			<h3>Parametrización General</h3>
			<hr><br>
		</div>

		<div class="parametrizacion">
			
		    <a href="{{route('userType')}}">Usuarios</a>
			<a href="{{route('authorityType')}}">Autoridades</a>
			<a href="{{route('multimediaType')}}">Multimedia</a>
			<a href="{{route('newsType')}}">Noticias</a>
			<a href="{{route('convenioType')}}">Convenios</a>
			<a href="{{route('senso1')}}">Informacion Sensal I</a>
			<a href="{{route('senso2')}}">Informacion Sensal II</a>
			<a href="{{route('proyectosType')}}">Proyectos</a>
			
		</div>

	</div>
</div>
</main>
@stop