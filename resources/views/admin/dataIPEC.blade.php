@extends('layouts.admin')

@section('title') Inicio @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{asset('css/admin.css')}}">
@stop
@section('main')
<main>
	<div class="page__main">

		<div class="main__link">			
            <a href="{{route('coordinator')}}">Coordinadores</a>
			<a href="{{route('module')}}">Modulos de Mallas Curriculares</a>
			<a href="{{route('curricular')}}"> Mallas Curriculares</a>
			<a href="{{route('program')}}">Programas</a>
			<a href="{{route('requirements')}}">Requerimientos</a>
			<a href="{{route('schedule')}}">Horarios</a>
		<a href="{{route('rate')}}">Tarifas</a>
		</div>
		<div class="main__title">
			<h1>Datos GAD Cantón Colta</h1>
		</div>

		<div class="main__insert">
			
			<div class="insert__form">
				<div>
					<p>
						En esta página usted podrá encontrar los enlaces para ingresar los datos del IPEC: Coordinadores, mallas curriculares, programas, requrimientos y horarios.
					</p>

					<p>
						Esto permitirá que la información se genere correctamente en el portal web.
					</p>
				</div>
			</div>
		</div>

	</div>
</div>
</main>
@stop