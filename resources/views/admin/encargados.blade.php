@extends('layouts.admin')

@section('title') Funcionarios @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')
<section>
	<div class="page__delete">
		<form method="POST" action="{{route('deleteEncargado')}}" class="action__form" id="form__update" enctype=multipart/form-data>
			<div class="form__title">
				<h3>¿Eliminar?</h3>
			</div>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="encargadoId" value="">
			@if (Auth::user()->user_type == 1 || Auth::user()->user_type == 2 )

			<div class="form__container">
				<div class="container__label">
					<label for="">Nombre: </label>
				</div>
				<div class="container__item">
					<input type="text" name="NombreEnc" disabled>
				</div>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Apellido: </label>
				</div>
				<div class="container__item">
					<input type="text" name="ApellidoEnc" disabled>
				</div>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Cédula: </label>
				</div>
				<div class="container__item">
					<input type="text" name="CiEnc" disabled>
				</div>
			</div>

			@endif

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<section>
	<div class="page__main">
		<div class="form__title">
			<h3>Agregar Proyecto</h3>
		</div>
		<div class="insert__form">

			<form method="POST" action="{{route('encargadosadd')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="ProId" value="">

				<div class="contenedor__categorias">

					<h6>Datos Encargado</h6>
					<div class="form__container">
						<div class="container__label">
							<label>Nombre: </label>
						</div>
						<div class="container__item">
							<input type="text" name="NombreEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Apellido: </label>
						</div>
						<div class="container__item">
							<input type="text" name="ApellidoEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Cédula:</label>
						</div>
						<div class="container__item">
							<input id="target" type="text" name="CiEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Cargo: </label>
						</div>
						<div class="container__item">
							<input type="text" name="CargoEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Teléfono: </label>
						</div>
						<div class="container__item">
							<input type="tel" id="fafina" name="FonoEnc"  required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Curriculum: </label>
						</div>
						<div class="container__item">
							<input type="file" name="CvEnc" accept=".doc, .docx, .pdf" class="custom-file-input" required>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Dirección:</label>
						</div>
						<div class="container__item">
							<input type="text" name="DirEnc" id="" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Email: </label>
						</div>
						<div class="container__item">
							<input type="email" name="emailEnc" id="" required>
						</div>
					</div>

				</div>

				<div class="form__button">
					<div class="button__save">
						<input type="submit" name="validar" value="Agregar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>

			</form>


		</div>
	</div>
</section>

<section>
	<div class="page__update">
		<div class="form__title">
			<h3>Actualizar Proyecto</h3>
		</div>
		<form method="POST" action="{{route('encargadosact')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="encargadoId" value="">

				<div class="contenedor__categorias">

					<h4>Datos Encargado</h4>
					<div class="form__container">
						<div class="container__label">
							<label>Nombre: </label>
						</div>
						<div class="container__item">
							<input type="text" name="NombreEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Apellido: </label>
						</div>
						<div class="container__item">
							<input type="text" name="ApellidoEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Cédula:</label>
						</div>
						<div class="container__item">
							<input id="target" type="text" name="CiEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Cargo: </label>
						</div>
						<div class="container__item">
							<input type="text" name="CargoEnc" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label>Teléfono: </label>
						</div>
						<div class="container__item">
							<input type="tel" id="fafina" name="FonoEnc"  required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Dirección:</label>
						</div>
						<div class="container__item">
							<input type="text" name="DirEnc" id="" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Email: </label>
						</div>
						<div class="container__item">
							<input type="email" name="emailEnc" id="" required>
						</div>
					</div>

				</div>

				<div class="form__button">
					<div class="button__save">
						<input type="submit" name="validar" value="Agregar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>

			</form>
	</div>
</section>


<main>
	<div class="page__main1">
	<div class="main__link">
           
		   <a href="{{route('proyectos')}}">Proyectos</a>
		   <!-- <a href="{{route('convenio')}}">Convenio</a> -->
            
		   
	   </div> 
		<div class="main__insert">
			<div class="main__title">
				<h3>ENCARGADOS PARA LOS PROYECTOS DEL {{$management->management_area_name}} </h3>
				<hr><br>
			</div>
			<div class="main__boton">
				<a class="insert"><i class="fa fa-plus"></i>Nuevo</a>
			</div>
		</div>

		<div class="main__content">
			<table class="content__table" id="user">
				<thead class="table__head">
					<tr>

						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cargo</th>
						<th>Teléfono</th>
						
						<th>Dirección</th>
						<th>Email</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($encargados as $Dataenc)
					<tr class="data__info" data-id-encargado="{{$Dataenc->personal_id}}" data-id-email="{{$Dataenc->personal_email}}" data-id-direc="{{$Dataenc->personal_direccion}}"	data-encargado-name="{{$Dataenc->personal_name}}" data-encargado-apll="{{$Dataenc->personal_last_name}}" data-encargado-cargo="{{$Dataenc->personal_cargo}}" data-encargado-tlf="{{$Dataenc->personal_tlf}}" data-encargado-ci="{{$Dataenc->personal_ci}}" data-encargado-cv="{{$Dataenc->personal_cv}}">
						<td>{{$Dataenc->personal_name}}</td>
						<td>{{$Dataenc->personal_last_name}}</td>
						<td>{{$Dataenc->personal_cargo}}</td>
						<td>{{$Dataenc->personal_tlf}}</td>
						
						<td>{{$Dataenc->personal_direccion}}</td>
						<td>{{$Dataenc->personal_email}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							@if (Auth::user()->user_type <= 3) <a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								@endif
						</td>

					</tr>
					@endforeach

				</tbody>
			</table>
			<br>
		</div>





		@if (count($errors) > 0 )
		<div class="main__msj">
			<ul id="mensaje">
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if(Session::has('mensaje'))
		<div class="main__msj">
			<ul id="mensaje">
				{{Session::get('mensaje')}}
			</ul>
		</div>
		@endIf

	</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">
	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__delete').slideToggle();
	});
	//$('input.fecha').datepicker({ dateFormat: 'yy-mm-dd' });
	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();

		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	$('.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
		$('#form__insert')[0].reset();
		$('#action__form')[0].reset();
	});


	$('.insert').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		$('.page__main').slideToggle();

	});

	$('#mensaje').fadeOut(5000);

	function cargarFormulario(datos) {
		$('.action__form')[0].reset();
		$(".action__form input[name=encargadoId]").val(datos['idEncargado']);
		$(".action__form input[name=NombreEnc]").val(datos['encargadoName']);
		$(".action__form input[name=ApellidoEnc]").val(datos['encargadoApll']);
		$(".action__form input[name=CiEnc]").val(datos['encargadoCi']);
		$(".action__form input[name=CargoEnc]").val(datos['encargadoCargo']);
		$(".action__form input[name=FonoEnc]").val(datos['encargadoTlf']);
		$(".action__form input[name=emailEnc]").val(datos['idEmail']);
		$(".action__form input[name=DirEnc]").val(datos['idDirec']);
		

		console.log(datos);
	}
</script>
<script type="text/javascript">
	/*
	 * Funcion JavaScript para validar Cedula,Ruc y Pasaporte Ecuatorianos.
	 *
	 * La funcion devuelve verdadero (true) en caso de exito y falso cuando hay
	 * un error en la validacion.
	 *
	 */
	$("#target").blur(function() {



		numero = document.getElementById('target').value;
		/* alert(numero); */

		var suma = 0;
		var residuo = 0;
		var pri = false;
		var pub = false;
		var nat = false;
		var numeroProvincias = 22;
		var modulo = 11;
		/* Verifico que el campo no contenga letras */
		var ok = 1;
		for (i = 0; i < numero.length && ok == 1; i++) {
			var n = parseInt(numero.charAt(i));
			if (isNaN(n)) ok = 0;
		}
		if (ok == 0) {
			alert("No puede ingresar caracteres en el número");
			document.getElementById('target').value = "";
			return false;
		}

		if (numero.length < 10) {
			alert('El número de ruc ingresado no es válido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Los primeros dos digitos corresponden al codigo de la provincia */
		provincia = numero.substr(0, 2);
		if (provincia < 1 || provincia > numeroProvincias) {
			alert('El código de la provincia (dos primeros dígitos) es inválido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Aqui almacenamos los digitos de la cedula en variables. */
		d1 = numero.substr(0, 1);
		d2 = numero.substr(1, 1);
		d3 = numero.substr(2, 1);
		d4 = numero.substr(3, 1);
		d5 = numero.substr(4, 1);
		d6 = numero.substr(5, 1);
		d7 = numero.substr(6, 1);
		d8 = numero.substr(7, 1);
		d9 = numero.substr(8, 1);
		d10 = numero.substr(9, 1);

		/* El tercer digito es: */
		/* 9 para sociedades privadas y extranjeros   */
		/* 6 para sociedades publicas */
		/* menor que 6 (0,1,2,3,4,5) para personas naturales */

		if (d3 == 7 || d3 == 8) {
			alert('El tercer dígito ingresado es inválido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Solo para personas naturales (modulo 10) */
		if (d3 < 6) {
			nat = true;
			p1 = d1 * 2;
			if (p1 >= 10) p1 -= 9;
			p2 = d2 * 1;
			if (p2 >= 10) p2 -= 9;
			p3 = d3 * 2;
			if (p3 >= 10) p3 -= 9;
			p4 = d4 * 1;
			if (p4 >= 10) p4 -= 9;
			p5 = d5 * 2;
			if (p5 >= 10) p5 -= 9;
			p6 = d6 * 1;
			if (p6 >= 10) p6 -= 9;
			p7 = d7 * 2;
			if (p7 >= 10) p7 -= 9;
			p8 = d8 * 1;
			if (p8 >= 10) p8 -= 9;
			p9 = d9 * 2;
			if (p9 >= 10) p9 -= 9;
			modulo = 10;
		}

		/* Solo para sociedades publicas (modulo 11) */
		/* Aqui el digito verficador esta en la posicion 9, en las otras 2 en la pos. 10 */
		else if (d3 == 6) {
			pub = true;
			p1 = d1 * 3;
			p2 = d2 * 2;
			p3 = d3 * 7;
			p4 = d4 * 6;
			p5 = d5 * 5;
			p6 = d6 * 4;
			p7 = d7 * 3;
			p8 = d8 * 2;
			p9 = 0;
		}

		/* Solo para entidades privadas (modulo 11) */
		else if (d3 == 9) {
			pri = true;
			p1 = d1 * 4;
			p2 = d2 * 3;
			p3 = d3 * 2;
			p4 = d4 * 7;
			p5 = d5 * 6;
			p6 = d6 * 5;
			p7 = d7 * 4;
			p8 = d8 * 3;
			p9 = d9 * 2;
		}

		suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
		residuo = suma % modulo;

		/* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
		digitoVerificador = residuo == 0 ? 0 : modulo - residuo;

		/* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
		if (pub == true) {
			if (digitoVerificador != d9) {
				alert('El ruc de la empresa del sector público es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			/* El ruc de las empresas del sector publico terminan con 0001*/
			if (numero.substr(9, 4) != '0001') {
				alert('El ruc de la empresa del sector público debe terminar con 0001');
				document.getElementById('target').value = "";
				return false;
			}
		} else if (pri == true) {
			if (digitoVerificador != d10) {
				alert('El ruc de la empresa del sector privado es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			if (numero.substr(10, 3) != '001') {
				alert('El ruc de la empresa del sector privado debe terminar con 001');
				document.getElementById('target').value = "";
				return false;
			}
		} else if (nat == true) {
			if (digitoVerificador != d10) {
				alert('El número de cédula de la persona natural es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			if (numero.length > 10 && numero.substr(10, 3) != '001') {
				alert('El ruc de la persona natural debe terminar con 001');
				document.getElementById('target').value = "";
				return false;
			}
		}
		return true;

	});
	$(document).ready(function() {
		$('#user').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});
</script>


@stop