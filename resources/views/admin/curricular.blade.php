@extends('layouts.admin')

@section('title') Programas Curriculares @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<form method="POST" action="{{route('updateCurricular')}}" class="action__form" enctype= multipart/form-data>
			<div class="form__title">
				<h1>Ingreso de Programas Curriculares</h1>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="categoryId" value="">
			<div class="form__container">
				<div class="container__label">
					<label for="">Título: </label>
				</div>
				<div class="container__item">
					<input type="text" name="curricularTitle"  >
				</div>					
			</div>
			
			<div class="form__container">
				<div class="container__label">
					<label for="">Módulo:</label>
				</div>
				<div class="container__item">
					<select  name="curricularModule" id="curricularModule" >
						<option value="" >

						</option>
						@forelse($option as $curricularModule)
						<option value="{{$curricularModule->module_curricular_pensum_id}}">
							{{$curricularModule->module_curricular_pensum_name}}
						</option>
						@empty
						<option value="">No existen módulos</option>
						@endforelse
					</select>
				</div>
			</div>


			<div class="form__button">
				<div class="button__save">						
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
	<div class="page__main">
		<div class="main__title">
			<h1>Gestión de la Información de Malla Curricular</h1>
		</div>

		<div class="main__insert">
			<div class="main__function">
				<h2>Agregar</h2>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('curricular')}}" class="action__form" id="form__insert">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

					<div class="form__container">
				<div class="container__label">
					<label for="">Título: </label>
				</div>
				<div class="container__item">
					<input type="text" name="curricularTitle"  >
				</div>					
			</div>
			
			<div class="form__container">
				<div class="container__label">
					<label for="">Módulo:</label>
				</div>
				<div class="container__item">
					<select  name="curricularModule" id="curricularModule" >
						<option value="" >

						</option>
						@forelse($option as $curricularModule)
						<option value="{{$curricularModule->module_curricular_pensum_id}}">
							{{$curricularModule->module_curricular_pensum_name}}
						</option>
						@empty
						<option value="">No existen módulos</option>
						@endforelse
					</select>
				</div>
		
			</div>


				
				@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje" >
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">						
							<input type="submit" value="Agregar">
						</div>
						<div class="button__cancel">
							<input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="main__content">
			<h2>Programas Curriculares Existentes</h2>
			<table  class="content__table" id="category">
				<thead class="table__head">
					<tr>
						<th>Título</th>
						<th>Módulo</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@forelse($category as $categoryData )
					<tr class="data__info" 
					data-id="{{$categoryData->curriculum_program_id}}"
					data-nombre="{{$categoryData->curriculum_program_description}}"
					data-descripcion="{{$categoryData->curriculum_program_module}}"
					>
					<td>{{$categoryData->curriculum_program_description}}</td>
					<td>
					    @foreach($option as $parentName)
							@if($parentName->module_curricular_pensum_id == $categoryData->curriculum_program_module)
								{{$parentName->module_curricular_pensum_name}}
							@endif
						@endforeach
					</td>
					<td>
						<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					</td>
				</tr>
				@empty
				<tr >
					<td  colspan="5" class="table__msj"> ! No hay programas curriculares...</td>
				</tr>
				@endforelse

			</tbody>
		</table>
	</div>
</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	function cargarFormulario(datos){
		$('.action__form')[0].reset();
		$(".action__form input[name=categoryId]").val(datos['id']);
		$(".action__form input[name=curricularName]").val(datos['nombre']);
		$(".action__form input[name=curricularModule]").val(datos['descripcion']);
	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource').slideToggle();
		$('#form__insert')[0].reset();
	});

	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#category').DataTable({
			"ordering": false,
			"info":     false,
			"paging":   false
		});
	} );
	
</script>
@stop