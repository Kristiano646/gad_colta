<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="ed-item main-center cross-center base-1-3 web-10">
        
        <img id="fsp-blanco" src="img/logos/encabezado-colta.png" alt="logo" width="1030px" height="100px">
        
    </div>
    <main>
	    <br>
        <h5>Informe General de Proyectos </h5>
        <br>
        <table class="table table-striped">
            <thead class="table-primary">
                <tr style="font-size:10pt;">
                    <th>Nombre del Proyecto</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>Tiempo Estimado</th>
                    <th>Objetivo</th>
                    <th>Informes Totales</th>
                    <th>Presupuesto</th>
                    <th>Encargado</th>
                    
                </tr>
            </thead>
            <tbody>
            @foreach($presupuesto as $PresData)
					@endforeach
					@foreach($presupuesto as $presData)
					@endforeach
					@forelse($presu as $pr)
					@endforeach
					@forelse($proyec as $ProyData)
					@forelse($encargados as $enc)
					@endforeach
					@forelse($encproye as $encproy)
					@endforeach

					<tr style="font-size:9pt;">
						<td>{{$ProyData->proyectos_name}}</td>
						<td>{{$ProyData->proyectos_fechaini}}</td>
						<td>{{$ProyData->proyectos_fechafin}}</td>
						<td>{{$ProyData->proyectos_nmeses}} Meses</td>
						<td>{{$ProyData->proyectos_objetivo}}</td>
						<td>{{$ProyData->proyectos_numInf}} </td>
						@forelse($presu as $pr)
						@if($pr->proyecto_id==$ProyData->proyectos_id)
						<td>{{$pr->presupuesto_monto}}</td>
						@endif
						@endforeach
						@forelse($encproye as $encproy)
						@if($encproy->proyecto_id==$ProyData->proyectos_id)
						<td>{{$encproy->personal_name}} {{$encproy->personal_last_name}}</td>
						@endif
						@endforeach
						
					</tr>

					@empty
					<tr>
						<td></td>
						<td class="table__msj">! No se han ncontrado Datos...</td>
						<td></td>
					</tr>

					@endforelse
					<hr>
            </tbody>
        </table>
    </main>
</body>

</html>