<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="ed-item main-center cross-center base-1-3 web-10">
        <img id="fsp-blanco" src="img/logos/encabezado-colta.png" alt="logo" width="725px" height="100px" >  
    </div>
    <main>
	    <br>
        <h5>Informe general de directivas </h5>
        <br>
            @if($most != 1 )
			<table class="table table-striped">
				<thead class="table-primary">
					<tr style="font-size:10pt;">
						<th>Nombre Comunidad</th>
						<th>Nombre Dirigente</th>
						<th>Cargo</th>
						<th>Género</th>
						<th>Fecha de cargo</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($directivas as $dataDirectivas)

					@endforeach
					@foreach($comunidades as $datacomunidades)

					@endforeach
					@foreach($miembro as $dataMiembro)
					@endforeach
					@foreach($nombreComunidad as $datajoin)
					<tr class="data__info" data-miembroid="{{$datajoin->miembro_id}}" data-comunidad-name="{{$datajoin->comunidad_name}}" data-comunidad-id="{{$datajoin->comunidad_id}}" data-directiva-id="{{$datajoin->miembro_directiva_id}}" data-nombre="{{$datajoin->miembro_nombres}}" data-cargo="{{$datajoin->miembro_cargo}}" data-sexo="{{$datajoin->miembro_sexo}}" data-fechaini="{{$datajoin->miembro_directiva_fechaini}}" data-fechafin="{{$datajoin->miembro_directiva_fechafin}}" style="font-size:9pt;">
						{{$member}}
						<td>{{$datajoin->comunidad_name}}</td>
						<td>{{$datajoin->miembro_nombres}}</td>
						<td>{{$datajoin->miembro_cargo}}</td>
						<td>{{$datajoin->miembro_sexo}}</td>
						<td>{{date('Y-m-d', strtotime($datajoin->miembro_directiva_fechaini))}} hasta {{date('Y-m-d', strtotime($datajoin->miembro_directiva_fechafin))}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							@if (Auth::user()->user_type <= 3) <a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			@if($most == 1 )
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th>Nombre Comunidad</th>
						<th>Nombre Dirigente</th>
						<th>Cargo</th>
						<th>Género</th>
						<th>Fecha de cargo</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($miembro as $dataMiembro)
					@endforeach
					@foreach($nombreComunidad as $datajoin)
					@endforeach
					@foreach($dire as $direc)
					<tr class="data__info" data-miembroid="{{$datajoin->miembro_id}}" data-comunidad-name="{{$datajoin->comunidad_name}}" data-comunidad-id="{{$datajoin->comunidad_id}}" data-directiva-id="{{$datajoin->miembro_directiva_id}}" data-nombre="{{$datajoin->miembro_nombres}}" data-cargo="{{$datajoin->miembro_cargo}}" data-sexo="{{$datajoin->miembro_sexo}}" data-fechaini="{{$datajoin->miembro_directiva_fechaini}}" data-fechafin="{{$datajoin->miembro_directiva_fechafin}}">

						<td>{{$direc->comunidad_name}}</td>
						<td>{{$direc->miembro_nombres}}</td>
						<td>{{$direc->miembro_cargo}}</td>
						<td>{{$direc->miembro_sexo}}</td>
						<td>{{date('Y-m-d', strtotime($direc->miembro_directiva_fechaini))}} hasta {{date('Y-m-d', strtotime($direc->miembro_directiva_fechafin))}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
    </main>
</body>

</html>