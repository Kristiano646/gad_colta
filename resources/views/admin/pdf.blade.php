<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="ed-item main-center cross-center base-1-3 web-10">
        <img id="fsp-blanco" src="img/logos/encabezado-colta.png" alt="logo" width="725px" height="100px" >
    </div>
    <main>
        <br>
        <h5>Informe general de comunidades </h5>
        <br>
        <table class="table table-striped">
            <thead class="table-primary">
                <tr style="font-size:10pt;">
                    <th>Nombre Comunidad</th>
                    <th>Ubicación</th>
                    <th>Nombre Contacto</th>
                    <th>Teléfono</th>
                    <th>Email</th>
                    <th>Cédula</th>  
                </tr>
            </thead>
            <tbody>
            @forelse($category as $categoryData )
                <tr style="font-size:9pt;">
                    <td>{{$categoryData->comunidad_name}}</td>
                    <td>{{$categoryData->comunidad_ubicacion}}</td>
                    <td>{{$categoryData->comunidad_cont_name}}</td>
                    <td>{{$categoryData->comunidad_cont_tel}}</td>
                    <td>{{$categoryData->comunidad_cont_email}}</td>
                    <td>{{$categoryData->comunidad_ruc}}</td>
                </tr>
                @empty
					<tr>
						<td></td>
						<td class="table__msj"> ! No hay categorías registradas...</td>
						<td></td>
					</tr>
					@endforelse
            </tbody>
        </table>
    </main>
</body>

</html>