@extends('layouts.admin')

@section('title') Horarios @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<form method="POST" action="{{route('updateSchedule')}}" class="action__form" enctype= multipart/form-data>
			<div class="form__title">
				<h1>Ingreso de Horarios </h1>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="categoryId" value="">
			<div class="form__container">
				<div class="container__label">
					<label for="">Día: </label>
				</div>
				<div class="container__item">
					<input type="text" name="scheduleDay"  >
				</div>					
			</div>
			
            <div class="form__container">
				<div class="container__label">
					<label for="">Hora de Inicio: </label>
				</div>
				<div class="container__item">
					<input type="time" name="scheduleBegin"  >
				</div>					
			</div>
			
            
            <div class="form__container">
				<div class="container__label">
					<label for="">Hora de Fin: </label>
				</div>
				<div class="container__item">
					<input type="time" name="scheduleEnd"  >
				</div>					
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Programa:</label>
				</div>
				<div class="container__item">
					<select  name="scheduleProgram" id="scheduleProgram" >
						<option value="" >

						</option>
						@forelse($option as $curricularModule)
						<option value="{{$curricularModule->program_id}}">
							{{$curricularModule->program_description}}
						</option>
						@empty
						<option value="">No existen horarios</option>
						@endforelse
				</select>
				</div>
			</div>


			<div class="form__button">
				<div class="button__save">						
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
	<div class="page__main">
		<div class="main__title">
			<h1>Gestión de Horarios</h1>
		</div>

		<div class="main__insert">
			<div class="main__function">
				<h2>Agregar</h2>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('schedule')}}" class="action__form" id="form__insert">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="form__container">
				<div class="container__label">
					<label for="">Día: </label>
				</div>
				<div class="container__item">
					<input type="text" name="scheduleDay"  >
				</div>					
			</div>
			
            <div class="form__container">
				<div class="container__label">
					<label for="">Hora de Inicio: </label>
				</div>
				<div class="container__item">
					<input type="time" name="scheduleBegin"  >
				</div>					
			</div>
			
            
            <div class="form__container">
				<div class="container__label">
					<label for="">Hora de Fin: </label>
				</div>
				<div class="container__item">
					<input type="time" name="scheduleEnd"  >
				</div>					
			</div>

			<div class="form__container">
				<div class="container__label">
					<label for="">Programa:</label>
				</div>
				<div class="container__item">
					<select  name="scheduleProgram" id="scheduleProgram" >
						<option value="" >

						</option>
						@forelse($option as $curricularModule)
						<option value="{{$curricularModule->program_id}}">
							{{$curricularModule->program_description}}
						</option>
						@empty
						<option value="">No existen horarios</option>
						@endforelse
				</select>
				</div>
			</div>

		

				
				@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje" >
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">						
							<input type="submit" value="Agregar">
						</div>
						<div class="button__cancel">
							<input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="main__content">
			<h2>Horarios Existentes</h2>
			<table  class="content__table" id="category">
				<thead class="table__head">
					<tr>
						<th>Día</th>
						<th>Hora Inicio</th>
                        <th>Hora Fin</th>
                        <th>Programa</th>
					</tr>
				</thead>
				<tbody>
					@forelse($category as $categoryData )
					<tr class="data__info" 
					data-id="{{$categoryData->schedule_program_id}}"
					data-nombre="{{$categoryData->schedule_program_day}}"
					data-descripcion="{{$categoryData->schedule_program_hour_begin}}"
					data-descripcion2="{{$categoryData->schedule_program_hour_end}}"
                    data-descripcion3="{{$categoryData->schedule_program_program}}"
                    >
					<td>{{$categoryData->schedule_program_day}}</td>
					<td>{{$categoryData->schedule_program_hour_begin}}</td>
					<td>{{$categoryData->schedule_program_hour_end}}</td>
					<td>
                        @foreach($option as $parentName)
							@if($parentName->program_id == $categoryData->schedule_program_program)
								{{$parentName->program_description}}
							@endif
						@endforeach
					</td>
					<td>
						<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					</td>
				</tr>
				@empty
				<tr >
					<td  colspan="5" class="table__msj"> ! No hay horariosS...</td>
				</tr>
				@endforelse

			</tbody>
		</table>
	</div>
</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	function cargarFormulario(datos){
		$('.action__form')[0].reset();
		$(".action__form input[name=categoryId]").val(datos['id']);
		$(".action__form input[name=scheduleDay]").val(datos['nombre']);
		$(".action__form input[name=scheduleDayBegin]").val(datos['descripcion']);
        $(".action__form input[name=scheduleDayEnd]").val(datos['descripcion2']);
        $(".action__form input[name=scheduleProgram]").val(datos['descripcion3']);
	
    }

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource').slideToggle();
		$('#form__insert')[0].reset();
	});

	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#category').DataTable({
			"ordering": false,
			"info":     false,
			"paging":   false
		});
	} );
	
</script>
@stop