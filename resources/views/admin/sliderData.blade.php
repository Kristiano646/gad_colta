@extends('layouts.admin')
@section('title') Slider @stop
@section('styles')
@parent
<link rel="stylesheet" href="{{asset('/css/admin.css')}}">
@stop
@section('main')
<main>
	<div class="page__data">

		<div class="data__title">
			<h1>{{$social->slider_name}} </h1>
		</div>
		<div class="data__name">
			<p class="name__content">DESCRIPCIÓN</p>
		</div>
		<div class="data__content">
			<p class="content__descrip">{{$social->slider_description}}</p>
		</div>
		<div class="data__multi">
			<p class="name__content">IMAGEN</p>
		</div>	
		<div class="data__multimedia">
			<div class="multimedia__img">
				<div class="img__imagen"> 
				<img src="{{asset('/img/slider/'.$social->slider_image)}}" alt="Recurso no disponible" style="max-width:100%;width:auto;height:auto;">
				</div>	
			</div> 
		</div>
		<div class="data__name">
			<div class="name__link">
				<a class="fa fa-chevron-circle-left" href="{{route('slider')}}"> Atras</a>
			</div>
		</div>
	</div>

</main>
@stop