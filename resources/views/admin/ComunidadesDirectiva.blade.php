@extends('layouts.admin')

@section('title') Categorías @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__update">
		<div class="form__title">
			<h3>Actualizar Miembro</h3>
		</div>
		<form method="POST" action="{{route('directivasupd')}}" class="action__form" enctype=multipart/form-data>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="directivaId" value="">
			<input type="hidden" name="miembroId" value="">

			<div class="contenedor__categorias">
				<div class="form__container">
					<div class="container__label">
						<label for="">Comunidad: </label>
					</div>
					<div class="container__item">

						<select name="comunidadId">
							<option>Elija una Opción</option>
							@forelse($category as $NameComunidad)
							<option value="{{$NameComunidad->comunidad_id}}">{{$NameComunidad->comunidad_name}}</option>
							@empty
							<option>No existen Tipos </option>
							@endforelse
						</select>
					</div>

				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre de Miembro: </label>
					</div>
					<div class="container__item">
						<input type="text" name="NombreM" class="fecha" required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Cargo: </label>
					</div>
					<div class="container__item">
						<input type="text" name="CargoM" class="fecha" required>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Fecha Inicio actividades: </label>
					</div>
					<div class="container__item">
						<input type="date" name="fechaini" class="fecha" required>
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Fecha Fin actividades: </label>
					</div>
					<div class="container__item">
						<input type="date" name="fechafin" class="fecha" required>
					</div>
				</div>
			</div>
			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Actualizar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>
		</form>
	</div>
</section>

<section>
	<div class="page__main">
		<div class="form__title">
			<h3>Agregar Directiva</h3>
		</div>
		<div class="insert__form">

			<form method="POST" action="{{route('directivasing')}}" name="f1" id="f1" class="action__form" id="form__insert" enctype=multipart/form-data>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="directivaId" value="">

				<div class="contenedor__categorias">

					<div class="form__container">
						<div class="container__label">
							<label for="">Comunidad: </label>
						</div>

						<div class="container__item">
							<select name="comunidadId">
								<option value=""></option>
								@forelse($category as $NameComunidad)
								<option value="{{$NameComunidad->comunidad_id}}">{{$NameComunidad->comunidad_name}}</option>
								@empty
								<option>No existen Tipos </option>
								@endforelse
							</select>
						</div>

					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<input id="dirin" type="text" name="Desc" oninput="valor()" required>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Estado: </label>
						</div>
						<div class="container__item">
							<select name="EstadoD">
								<option value=""></option>
								<option value="1">Activo</option>
								<option value="2">Inactivo</option>
							</select>
						</div>
						<div class="form-msg"></div>
					</div>
				</div>
				
				<div class="form__button">
					<div class="button__save">
						<input type="submit" name="validar" value="Validar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>
			</form>

			<form method="POST" action="{{route('miembrosing')}}" name="f1" id="miembroform" class="action__form" id="form__insert" enctype=multipart/form-data >
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="directiva_descripcion" value="">
				<input type="hidden" name="comunidadId" value="">


				<div class="contenedor__categorias">
					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<input id="miemin" type="text" name="DescM" required readonly>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Nombres y Apellidos: </label>
						</div>
						<div class="container__item">
							<input type="text" name="NomApp" id="nom" required disabled>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Cargo: </label >
						</div>
						<div class="container__item">
							<input type="text" name="CargoM" id="carg" required disabled>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Genero: </label>
						</div>
						<div class="container__item">
							<select name="SexM" id="sex" disabled>
								<option value=""></option>
								<option value="Masculino">Masculino</option>
								<option value="Femenino">Femenino</option>
								<option value="No especificar">No Especificar</option>
							</select>
						</div>
					</div>

					<div class="form__container">
						<div class="container__label">
							<label for="">Fecha Inicio actividades: </label>
						</div>
						<div class="container__item">
							<input type="date" id="fini" name="fechaini" class="fecha" required disabled>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Fecha Fin actividades: </label>
						</div>
						<div class="container__item">
							<input type="date" id="ffin" name="fechafin" class="fecha" required disabled>
						</div>
					</div>

				</div>

				<div class="form__button">
					<div class="button__save">
						<input type="submit" value="Agregar">
					</div>
					<div class="button__cancel">
						<input type="button" class="cancel__btn" value="Cancelar">
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<section>
	<div class="page__delete">
		<form method="POST" action="{{route('directivasdel')}}" class="action__form" enctype="multipart/form-data">

			<div class="form__title">
				<h3>Eliminar Tipo</h3>
			</div>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="directivaId" value="">
			<input type="hidden" name="comunidadId" value="">
			<div class="contenedor__categorias">
				<div class="form__container">
					<div class="container__label">
						<label for="">Comunidad: </label>
					</div>
					<div class="container__item">

						<select name="comunidadId" disabled>
							<option>Elija una Opción</option>
							@forelse($category as $NameComunidad)
							<option value="{{$NameComunidad->comunidad_id}}">{{$NameComunidad->comunidad_name}}</option>
							@empty
							<option>No existen Tipos </option>
							@endforelse
						</select>
					</div>

				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Nombre de Miembro: </label>
					</div>
					<div class="container__item">
						<input type="text" name="NombreM"  required disabled>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Cargo: </label>
					</div>
					<div class="container__item">
						<input type="text" name="CargoM"  required disabled>
					</div>
				</div>
				<div class="form__container">
					<div class="container__label">
						<label for="">Fecha Inicio actividades: </label>
					</div>
					<div class="container__item">
						<input type="text" name="fechaini"  required disabled>
					</div>
				</div>

				<div class="form__container">
					<div class="container__label">
						<label for="">Fecha Fin actividades: </label>
					</div>
					<div class="container__item">
						<input type="text" name="fechafin"  required disabled>
					</div>
				</div>
			</div>




			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
	<div class="page__main1">
		<div class="main__link">
			<a href="{{route('directivas')}}">Directivas</a>
			<a href="{{route('infosenso1')}}">Información Censal I</a>
			<a href="{{route('infosenso2')}}">Información Censal II</a>
			<!-- <a href="{{route('bcodatos')}}">Banco de Datos</a> -->
		</div>
		<div class="main__title">
			<h3>Directiva</h3>
		</div>
		<hr><br>
		<div class="main__content">
			<h4>Directivas Existentes </h4>
			<div class="main__boton">
				<a class="insert"><i class="fa fa-plus"></i>Nuevo</a>
				&nbsp;
				<a class="pdf" href="{{route('downloaddirec')}}"><i class="fa fa-file-pdf-o"></i>PDF</a>
			</div>
			

			<div class="card-body">
				<form method="post" action="{{route('datatable')}}" name="frmfil" id="frmfil" class="action__form" enctype=multipart/form-data>
					<input type="hidden" name="mostar" value="1">
					<div class="row mb-3">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="col-md-3">
							<input type="date" name="start_date" id="start_date" class="form-control start_date" />
						</div>
						<div class="col-md-3">
							<input type="date" name="end_date" id="end_date" class="form-control end_date" />
						</div>
						<div class="col-md-3">
							<select class="form-control status_id" id="status_id" name="status_id">
								<option value="">Seleccione</option>
								@foreach($category as $NameComunidad)
								<option value="{{$NameComunidad->comunidad_id}}">{{$NameComunidad->comunidad_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-1">
							<input type="submit" id="filtrar" class="btn btn-danger" value="Filtrar" />
						</div>
						<form method="get" action="{{route('directivas')}}">
							<div class="col-md-1">
								<button type="submit" name="reloader" id="reloader" class="btn btn-danger">Recargar</button>
							</div>
						</form>
					</div>
				</form>
			</div>
			

			@if($most != 1 )
			<table class="content__table" id="user">
				<thead class="table__head">
					<tr>
						<th>Nombre Comunidad</th>
						<th>Nombre Dirigente</th>
						<th>Cargo</th>
						<th>Género</th>
						<th>Fecha de cargo</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($directivas as $dataDirectivas)

					@endforeach
					@foreach($category as $datacomunidades)

					@endforeach
					@foreach($miembro as $dataMiembro)
					@endforeach
					@foreach($data as $datajoin)
					<tr class="data__info" data-miembroid="{{$datajoin->miembro_id}}" data-comunidad-name="{{$datajoin->comunidad_name}}" data-comunidad-id="{{$datajoin->comunidad_id}}" data-directiva-id="{{$datajoin->miembro_directiva_id}}" data-nombre="{{$datajoin->miembro_nombres}}" data-cargo="{{$datajoin->miembro_cargo}}" data-sexo="{{$datajoin->miembro_sexo}}" data-fechaini="{{$datajoin->miembro_directiva_fechaini}}" data-fechafin="{{$datajoin->miembro_directiva_fechafin}}">
						{{$member}}
						<td>{{$datajoin->comunidad_name}}</td>
						<td>{{$datajoin->miembro_nombres}}</td>
						<td>{{$datajoin->miembro_cargo}}</td>
						<td>{{$datajoin->miembro_sexo}}</td>
						<td>{{date('Y-m-d', strtotime($datajoin->miembro_directiva_fechaini))}} hasta {{date('Y-m-d', strtotime($datajoin->miembro_directiva_fechafin))}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							@if (Auth::user()->user_type <= 3) <a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			@if($most == 1 )
			<table class="content__table" id="user1">
				<thead class="table__head">
					<tr>
						<th>Nombre Comunidad</th>
						<th>Nombre Dirigente</th>
						<th>Cargo</th>
						<th>Género</th>
						<th>Fecha de cargo</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($miembro as $dataMiembro)
					@endforeach
					@foreach($data as $datajoin)
					@endforeach
					@foreach($dire as $direc)
					<tr class="data__info" data-miembroid="{{$datajoin->miembro_id}}" data-comunidad-name="{{$datajoin->comunidad_name}}" data-comunidad-id="{{$datajoin->comunidad_id}}" data-directiva-id="{{$datajoin->miembro_directiva_id}}" data-nombre="{{$datajoin->miembro_nombres}}" data-cargo="{{$datajoin->miembro_cargo}}" data-sexo="{{$datajoin->miembro_sexo}}" data-fechaini="{{$datajoin->miembro_directiva_fechaini}}" data-fechafin="{{$datajoin->miembro_directiva_fechafin}}">

						<td>{{$direc->comunidad_name}}</td>
						<td>{{$direc->miembro_nombres}}</td>
						<td>{{$direc->miembro_cargo}}</td>
						<td>{{$direc->miembro_sexo}}</td>
						<td>{{date('Y-m-d', strtotime($direc->miembro_directiva_fechaini))}} hasta {{date('Y-m-d', strtotime($direc->miembro_directiva_fechafin))}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							@if (Auth::user()->user_type <= 3) <a class="delete" title="eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								@endif
						</td>

					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			<br><br>
		</div>
</main>
@stop


@section('scripts')
@parent



<script type="text/javascript">
	$('#mensaje').fadeOut(5000);

	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		console.log(datos);
		$('.page__update').slideToggle();
	});
	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		cargarFormulario(datos);
		console.log(datos)
		$('.page__delete').slideToggle();
	});

	$('.insert').on('click', function(event) {
		event.preventDefault();
		$('.page__main').slideToggle();

	});

	$('.validar').on('click', function(event) {
		event.preventDefault();

	});



	(function() {
		const forma = document.getElementById("f1");
		forma.addEventListener("submit", function(event) {
			event.preventDefault();
			var form = document.getElementById("f1"); //document.querySelector("f1");
			var datos = new FormData(form);
			console.log(form)
			var init = {
				method: form.method,
				body: datos
			};
			fetch(form.action, init)
				.then(function(response) {
					//document.getElementById("miembroform").style.display = "block";
					document.getElementById("ffin").disabled = false;
					document.getElementById("fini").disabled = false;
					document.getElementById("sex").disabled = false;
					document.getElementById("carg").disabled = false;
					document.getElementById("nom").disabled = false;
					

				})
				.catch(function(error) {
					//document.getElementById("miembroform").style.display = "none";
					console.log('Hubo un problema con la petición Fetch:' + error.message);
				});

		});

	})();





	function cargarFormulario(datos) {
		console.log(datos)
		$('.action__form')[0].reset();
		$(".action__form select[name=comuniname]").val(datos['comunidadName']);
		$(".action__form input[name=miembroId]").val(datos['miembroid']);
		$(".action__form select[name=comunidadId]").val(datos['comunidadId']);
		$(".action__form input[name=directivaId]").val(datos['directivaId']);
		$(".action__form input[name=NombreM]").val(datos['nombre']);
		$(".action__form input[name=CargoM]").val(datos['cargo']);
		$(".action__form input[name=fechafin]").val(datos['fechafin']);
		$(".action__form input[name=fechaini]").val(datos['fechaini']);

	}

	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource, .page__main').slideToggle();
		$('#form__insert')[0].reset();
		$('#action__form')[0].reset();
	});
	$('#mensaje').fadeOut(5000);
</script>
<script>
	$(document).ready(function() {
		$('#user').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});

	$(document).ready(function() {
		$('#user1').DataTable({
			"ordering": true,
			"info": false,
			"paging": false
		});
	});
</script>
<script>
	function valor() {
		let municipio = document.getElementById("dirin").value;
		//Se actualiza en municipio inm
		document.getElementById("miemin").value = municipio;
	}
</script>


@stop