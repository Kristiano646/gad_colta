@extends('layouts.admin')

@section('title') Funcionarios @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')
<section>
	<div class="page__delete">
		<form method="POST" action="{{route('deleteAuthority')}}" class="action__form" id="form__update" enctype=multipart/form-data>
			<div class="form__title">
				<h1>¿Eliminar?</h1>
			</div>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="autorityId" value="">
			@if (Auth::user()->user_type == 1 || Auth::user()->user_type == 2 )
			<div class="data__img">
				<div class="img__image">
					<img name="autorityphoto" width="100" height="100"></img>
				</div>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Nombre: </label>
				</div>
				<div class="container__item">
					<input type="text" name="autorityname" disabled>
				</div>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Apellido: </label>
				</div>
				<div class="container__item">
					<input type="text" name="autoritylastname" disabled>
				</div>
			</div>
			<div class="form__container">
				<div class="container__label">
					<label for="">Cédula: </label>
				</div>
				<div class="container__item">
					<input type="text" name="autorityci" disabled>
				</div>
			</div>

			@endif

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>
<main>
	<div class="page__main">
		<div class="main__insert">
			<div class="main__title">
				<h3>FUNCIONARIOS DEL {{$management->management_area_name}} </h3>
				<br><br>
			</div>
			<div class="main__boton">
				<a href="{{route('newAuthority')}}"><i class="fa fa-plus"></i>Nuevo  </a>
			</div>
			<br>
		</div>
		@if (count($errors) > 0 )
		<div class="main__msj">
			<ul id="mensaje">
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if(Session::has('mensaje'))
		<div class="main__msj">
			<ul id="mensaje">
				{{Session::get('mensaje')}}
			</ul>
		</div>
		@endIf
		<div class="main__data">
			@forelse($authority as $authorityData)
			<div class="data__info" data-id="{{$authorityData->user_id}}">
				<form method="POST" action="{{route('updateAuthority')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="authorityId" value="{{$authorityData->user_id}}">
					<div class="data__container" data-id="{{$authorityData->user_id}}" data-photo="{{asset('img/authority/'.$authorityData->user_photo)}}" data-ci="{{$authorityData->user_ci}}" data-name="{{$authorityData->user_name}}" data-lastname="{{$authorityData->user_last_name}}">
						<div class="data__img">
							<div class="img__image">
								@if($authorityData->user_photo !=null)
								<img src="{{asset('img/authority/'.$authorityData->user_photo)}}">
								@else
								<img src="{{asset('img/authority/IMAGEN-AUTORIDADES.png')}}">
								@endIf
							</div>
							<br>
							<div class="img__item">
								<input type="file" name="authorityPhoto" accept="image/*" class="custom-file-input">
							</div>
						</div>
						<div class="data__name">
							<div class="data__item">
								<label>Nombres:</label>
								<input type="text" name="authorityName" value="{{$authorityData->user_name}}">
							</div>

							<div class="data__item">
								<label>Apellidos:</label>
								<input type="text" name="authorityLastName" value="{{$authorityData->user_last_name}}">
							</div>
							<div class="data__item">
								<label id="ruc" for="">Ruc o CI: </label>
								<input id="target" type="text" name="authorityCi" value="{{$authorityData->user_ci}}" maxlength="15" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
							</div>
							<div class="data__item">
								<label>Direccion:</label>
								<input type="text" name="authorityDireccion" value="{{$authorityData->user_direccion}}">
							</div>
							<div class="data__item">
								<label>Teléfono:</label>
								<input type="tel" name="authorityTelefono" value="{{$authorityData->user_telefono}}" maxlength="15" placeholder="(Código de área) Número" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
							</div>
							<div class="data__item">
								<label>Email:</label>
								<input type="email" name="authorityEmail" value="{{$authorityData->user_mail}}">
							</div>
							<br>
							<br>

						</div>
						<div class="data__name">
							<div class="data__item">
								<label>Cargo:</label>
								<select name="authorityCargo">
									@forelse($cargo as $authorityCargo)
									@if($authorityData->user_cargo_a != $authorityCargo->user_cargo_id)
									<option value="{{$authorityCargo->user_cargo_id}}">{{$authorityCargo->user_cargo_description}}</option>

									@else
									<option selected value="{{$authorityCargo->user_cargo_id}}">{{$authorityCargo->user_cargo_description}}</option>
									@endIf

									@empty
									<option>No existen cargos </option>
									@endforelse
								</select>
							</div>
							@if (Auth::user()->user_type <= 2)
							<div class="data__item">
								<label>Tipo:</label>
								<select name="authorityType">
									@forelse($types as $authorityType)
									@if($authorityData->user_type != $authorityType->user_type_id)
									<option value="{{$authorityType->user_type_id}}">{{$authorityType->user_type_description}}</option>

									@else
									<option selected value="{{$authorityType->user_type_id}}">{{$authorityType->user_type_description}}</option>
									@endIf

									@empty
									<option>No existen Tipos </option>
									@endforelse
								</select>
							</div>
							@endif
							<div class="data__item">
								<label>Estado:</label>
								<select name="authorityEstado">
									@forelse($estado as $authorityEstado)
									@if($authorityData->user_estado != $authorityEstado->user_estado_id)
									<option value="{{$authorityEstado->user_estado_id}}">{{$authorityEstado->user_estado_description}}</option>
									@else
									<option selected value="{{$authorityEstado->user_estado_id}}">{{$authorityEstado->user_estado_description}}</option>
									@endIf

									@empty
									<option>No existen Estados </option>
									@endforelse
								</select>
							</div>
							<div class="data__item">
								<label>Hoja de vida actual:</label>
								@if($authorityData->user_cv !=null)
								<a target="_blank" href="{{asset('docs/authority/'.$authorityData->user_cv)}}"><i class="fa fa-file-pdf-o"></i></a>
								@endIf
							</div>
							<div class="data__item">
								<label>Nueva Hoja de vida:</label>
								<input type="file" name="authorityCv" accept=".doc, .docx, .pdf" class="custom-file-input">
							</div>
							<div class="data__item">
								<label>Observaciones:</label>
								<input type="text" name="authorityObservaciones" value="{{$authorityData->user_observaciones}}">
							</div>
						</div>

						<div class="data__button">
							<div>
								<input type="submit" name="btnGuardar" value="Modificar">
							</div>
							<div>
								<input type="button" required name="btnCancelar" class="cancel__btn" value="Cancelar">
							</div>

						</div>
						<div class="eliminar">
							@if (Auth::user()->user_type == 1)
							<input type="button" class="delete" required name="btnEliminar" value="Eliminar">
							@endif
						</div>
					</div>
				</form>
				@empty
				<div class="data__msj">! No hay directivos registrados... </div>
				@endforelse
			</div>
		</div>
	</div>
</main>
@stop

@section('scripts')
@parent

<script type="text/javascript">
	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__container').data();
		cargarFormulario(datos);
		$('.page__delete').slideToggle();
	});
	//$('input.fecha').datepicker({ dateFormat: 'yy-mm-dd' });

	$('.cancel__btn').click(function(event) {
		location.reload();
	});

	$('#mensaje').fadeOut(5000);

	function cargarFormulario(datos) {
		$('.action__form')[0].reset();
		$(".action__form input[name=autorityId]").val(datos['id']);
		$(".action__form input[name=autorityname]").val(datos['name']);
		$(".action__form input[name=autoritylastname]").val(datos['lastname']);
		$(".action__form input[name=autorityci]").val(datos['ci']);
		$(".action__form img[name=autorityphoto]").attr("src", datos['photo']);
		$('.action__form #newsState').val(datos['estado']);
		console.log(datos);
	}
</script>
<script type="text/javascript">
	/*
	 * Funcion JavaScript para validar Cedula,Ruc y Pasaporte Ecuatorianos.
	 *
	 * La funcion devuelve verdadero (true) en caso de exito y falso cuando hay
	 * un error en la validacion.
	 *
	 */
	$("#target").blur(function() {



		numero = document.getElementById('target').value;
		/* alert(numero); */

		var suma = 0;
		var residuo = 0;
		var pri = false;
		var pub = false;
		var nat = false;
		var numeroProvincias = 22;
		var modulo = 11;
		/* Verifico que el campo no contenga letras */
		var ok = 1;
		for (i = 0; i < numero.length && ok == 1; i++) {
			var n = parseInt(numero.charAt(i));
			if (isNaN(n)) ok = 0;
		}
		if (ok == 0) {
			alert("No puede ingresar caracteres en el número");
			document.getElementById('target').value = "";
			return false;
		}

		if (numero.length < 10) {
			alert('El número de ruc ingresado no es válido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Los primeros dos digitos corresponden al codigo de la provincia */
		provincia = numero.substr(0, 2);
		if (provincia < 1 || provincia > numeroProvincias) {
			alert('El código de la provincia (dos primeros dígitos) es inválido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Aqui almacenamos los digitos de la cedula en variables. */
		d1 = numero.substr(0, 1);
		d2 = numero.substr(1, 1);
		d3 = numero.substr(2, 1);
		d4 = numero.substr(3, 1);
		d5 = numero.substr(4, 1);
		d6 = numero.substr(5, 1);
		d7 = numero.substr(6, 1);
		d8 = numero.substr(7, 1);
		d9 = numero.substr(8, 1);
		d10 = numero.substr(9, 1);

		/* El tercer digito es: */
		/* 9 para sociedades privadas y extranjeros   */
		/* 6 para sociedades publicas */
		/* menor que 6 (0,1,2,3,4,5) para personas naturales */

		if (d3 == 7 || d3 == 8) {
			alert('El tercer dígito ingresado es inválido');
			document.getElementById('target').value = "";
			return false;
		}

		/* Solo para personas naturales (modulo 10) */
		if (d3 < 6) {
			nat = true;
			p1 = d1 * 2;
			if (p1 >= 10) p1 -= 9;
			p2 = d2 * 1;
			if (p2 >= 10) p2 -= 9;
			p3 = d3 * 2;
			if (p3 >= 10) p3 -= 9;
			p4 = d4 * 1;
			if (p4 >= 10) p4 -= 9;
			p5 = d5 * 2;
			if (p5 >= 10) p5 -= 9;
			p6 = d6 * 1;
			if (p6 >= 10) p6 -= 9;
			p7 = d7 * 2;
			if (p7 >= 10) p7 -= 9;
			p8 = d8 * 1;
			if (p8 >= 10) p8 -= 9;
			p9 = d9 * 2;
			if (p9 >= 10) p9 -= 9;
			modulo = 10;
		}

		/* Solo para sociedades publicas (modulo 11) */
		/* Aqui el digito verficador esta en la posicion 9, en las otras 2 en la pos. 10 */
		else if (d3 == 6) {
			pub = true;
			p1 = d1 * 3;
			p2 = d2 * 2;
			p3 = d3 * 7;
			p4 = d4 * 6;
			p5 = d5 * 5;
			p6 = d6 * 4;
			p7 = d7 * 3;
			p8 = d8 * 2;
			p9 = 0;
		}

		/* Solo para entidades privadas (modulo 11) */
		else if (d3 == 9) {
			pri = true;
			p1 = d1 * 4;
			p2 = d2 * 3;
			p3 = d3 * 2;
			p4 = d4 * 7;
			p5 = d5 * 6;
			p6 = d6 * 5;
			p7 = d7 * 4;
			p8 = d8 * 3;
			p9 = d9 * 2;
		}

		suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
		residuo = suma % modulo;

		/* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
		digitoVerificador = residuo == 0 ? 0 : modulo - residuo;

		/* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
		if (pub == true) {
			if (digitoVerificador != d9) {
				alert('El ruc de la empresa del sector público es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			/* El ruc de las empresas del sector publico terminan con 0001*/
			if (numero.substr(9, 4) != '0001') {
				alert('El ruc de la empresa del sector público debe terminar con 0001');
				document.getElementById('target').value = "";
				return false;
			}
		} else if (pri == true) {
			if (digitoVerificador != d10) {
				alert('El ruc de la empresa del sector privado es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			if (numero.substr(10, 3) != '001') {
				alert('El ruc de la empresa del sector privado debe terminar con 001');
				document.getElementById('target').value = "";
				return false;
			}
		} else if (nat == true) {
			if (digitoVerificador != d10) {
				alert('El número de cédula de la persona natural es incorrecto.');
				document.getElementById('target').value = "";
				return false;
			}
			if (numero.length > 10 && numero.substr(10, 3) != '001') {
				alert('El ruc de la persona natural debe terminar con 001');
				document.getElementById('target').value = "";
				return false;
			}
		}
		return true;

	});
</script>


@stop