@extends('layouts.admin')
@section('title') Noticias @stop
@section('styles')
@parent
<link rel="stylesheet" href="{{asset('/css/admin.css')}}">
@stop
@section('main')
<main>
	<div class="page__data">

		<div class="data__title">
			<h1>{{$news->news_title}}  {{$news->news_alias}}</h1>
		</div>
		<div class="data__name">
			<p class="name__content">CONTENIDO</p>
		</div>
		<div class="data__content">
		@if($news->news_content != null)
			<p class="content__descrip">{!!$news->news_content!!}</p>
			@else
			<p class="content__descrip">Sin descripción</p>
			@endif
		</div>
		<div class="data__date">
			<label class="date__label">Fecha de Publicación:</label> 
			<p class="date__info">{{$news->news_create->format('Y-M-d')}} </p>
			<label class="date__label">Fecha de Modificación:</label>
			<p class="date__info">{{$news->news_update->format('Y-M-d')}}</p>
		</div>
			
			
		</div>
		
		<div class="data__name">
			<div class="name__link">
				<a class="fa fa-chevron-circle-left" href="{{route('news')}}"> Atras</a>
			</div>
		</div>
	</div>
</main>
@stop