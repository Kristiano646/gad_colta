@extends('layouts.admin')

@section('title') Categorías @stop

@section('styles')
@parent
<link rel="stylesheet" href="{{ asset ('css/admin.css')}}">
@stop
@section('main')

<section>
	<div class="page__delete">
		<form method="POST" action="{{route('sensodlt1')}}" class="action__form" enctype=multipart/form-data>
			<div class="form__title">
				<h3>Eliminar Tipo</h3>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="SensoId" value="">
			
			<div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Parametro </label>
				</div>
				<div class="container__item">
					<select id="idtipo" name="idtipo" disabled>
						<option value="" >Escoja Categoria </option>
						@forelse($tiposen as $Datatiposen)
						<option value="{{$Datatiposen->Tiposinfocenso_id}}">{{$Datatiposen->Tiposinfocenso_description}}</option>
						@empty
						<option>No existen cargos </option>
						@endforelse
					</select>
				</div>
				<br>
				<div class="container__label">
					<label for="">Descripción: </label>
				</div>
				<div class="container__item">
					<input type="text" name="sensoDescription" disabled>
				</div>
			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Eliminar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<section>
	<div class="page__update">
		<form method="POST" action="{{route('sensoupd1')}}" class="action__form" enctype=multipart/form-data>
			<div class="form__title">
				<h3>Modificar Tipo</h3>
			</div>

			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="SensoId" value="">
			
			<div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Parametro </label>
				</div>
				<div class="container__item">
					<select id="idtipo" name="idtipo">
						<option value="" >Escoja Categoria </option>
						@forelse($tiposen as $Datatiposen)
						<option value="{{$Datatiposen->Tiposinfocenso_id}}">{{$Datatiposen->Tiposinfocenso_description}}</option>
						@empty
						<option>No existen cargos </option>
						@endforelse
					</select>
				</div>
				<br>
				<div class="container__label">
					<label for="">Descripción: </label>
				</div>
				<div class="container__item">
					<input type="text" name="sensoDescription">
				</div>
			</div>

			<div class="form__button">
				<div class="button__save">
					<input type="submit" value="Guardar">
				</div>
				<div class="button__cancel">
					<input type="button" class="cancel__btn" value="Cancelar">
				</div>
			</div>

		</form>
	</div>
</section>

<main>
	<div class="page__main">

		<div class="main__link">
			<a href="{{route('userType')}}">Usuarios</a>
			<a href="{{route('authorityType')}}">Autoridades</a>
			<a href="{{route('newsType')}}">Noticias</a>
			<a href="{{route('convenioType')}}">Convenios</a>
			<a href="{{route('senso1')}}">Informacion Sensal I</a>
			<a href="{{route('senso2')}}">Informacion Sensal II</a>
			<a href="{{route('proyectosType')}}">Proyectos</a>
		</div>

		<div class="main__title">
			<h3>Parametros Censo I</h3>
			<hr>
			<br> 
		</div>

		<div class="main__insert">
			<div class="main__function">
				<h4>Agregar</h4>
			</div>
			<div class="insert__form">
				<form method="POST" action="{{route('sensoing1')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">


					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<select id="idtipo" name="idtipo">
								<option value="" selected>Escoja Categoria </option>
								@forelse($tiposen as $Datatiposen)
								<option value="{{$Datatiposen->Tiposinfocenso_id}}">{{$Datatiposen->Tiposinfocenso_description}}</option>
								@empty
								<option>No existen cargos </option>
								@endforelse
							</select>
						</div>
					</div>
					<div class="form__container">
						<div class="container__label">
							<label for="">Descripción: </label>
						</div>
						<div class="container__item">
							<input type="text" name="sensoDescription">
						</div>
					</div>
					@if(Session::has('mensaje'))
					<div class="form__container">
						<div id="mensaje">
							{{Session::get('mensaje')}}
						</div>
					</div>
					@endIf
					@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="form__button">
						<div class="button__save">
							<input type="submit" value="Agregar">
						</div>
						<div class="button__cancel">
							<input type="button" class="cancel__btn" id="cancel__btn" value="Cancelar">
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="main__content">
			<h3>Parametros Censo I </h3>

			<div class="data__item">
				<label>Tipo de Parametro:</label>

				<form method="POST" action="{{route('tipo1')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

					<select id="Datatiposen" name="Datatiposen" onchange="this.form.submit()">
						<option value="" selected>Escoja Categoria </option>
						@forelse($tiposen as $Datatiposen)
						<option value="{{$Datatiposen->Tiposinfocenso_id}}">{{$Datatiposen->Tiposinfocenso_description}}</option>
						@empty
						<option>No existen cargos </option>
						@endforelse
					</select>
				</form>
			</div>

			<table class="content__table" id="user">
				<thead class="table__head">
					<tr>
						<th>Descripción</th>
						<th>Tipo</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>

					@foreach($tipsenso as $tipS)
					<tr class="data__info" data-id="{{$tipS->infocenso_id}}" data-id-tipo="{{$tipS->infocenso_id_Tipos}}" data-descripcion="{{$tipS->infocenso_description}}">
						<td>{{$tipS->infocenso_description}}</td>
						<td>{{$tipS->Tiposinfocenso_description}}</td>
						<td>
							<a class="update" title="modificar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							<a class="delete" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
						</td>
					</tr>

					@endforeach

				</tbody>
			</table>
			<br><br>



			





		</div>
	</div>
</main>
@stop


@section('scripts')
@parent
<script type="text/javascript">
	$('.update').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		console.log(datos);
		cargarFormulario(datos);
		$('.page__update').slideToggle();
	});

	$('.delete').on('click', function(event) {
		event.preventDefault();
		var datos = $(this).closest('.data__info').data();
		console.log(datos);
		cargarFormulario(datos);
		$('.page__delete').slideToggle();
	});

	function cargarFormulario(datos) {
		$('.action__form')[0].reset();
		$(".action__form input[name=SensoId]").val(datos['id']);
		$(".action__form select[name=idtipo]").val(datos['idTipo']);
		$(".action__form input[name=sensoDescription]").val(datos['descripcion']);
	}
	$('input.cancel__btn').click(function(event) {
		$(this).closest('.page__delete , .page__update , .page__resource').slideToggle();
		$('#form__insert')[0].reset();
		$('#action__form')[0].reset();
	});
	$('#mensaje').fadeOut(5000);

	$(document).ready(function() {
		$('#user').DataTable({
			"ordering": false,
			"info": false,
			"paging": false
		});
	});
</script>

@stop