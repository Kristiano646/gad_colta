@extends('layouts.plantilla')

@section('title') Vinculación @stop

@section('estilos')
@parent
<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')
<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="mision">
			<div class="ed-item main-center">
				
			<h3>Misión y Visión </h3>
			</div>
			<div class="ed-item cross-center web-50">
				<img src="{{ asset('img/vinculacion/'.$management->management_area_image_mission) }}" alt="">
			</div>
			<div class="ed-item cross-center web-50">
				<h2>Misión</h2>
				<p>{!!$management->management_area_mission  !!}</p>
				<h2>Visión</h2>
				<p>{!!$management->management_area_vision  !!}</p>
			</div>
		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">

		$('nav').addClass('sticky');

		// $('body').on({
		// 	'mousewheel': function(e) {
		// 		if (e.target.id == 'el') return;
		// 		e.preventDefault();
		// 		e.stopPropagation();
		// 	}
		// })

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});

	</script>
	@stop
</body>
@stop
