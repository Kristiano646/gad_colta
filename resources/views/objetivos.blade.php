@extends('layouts.plantilla')

@section('title') Objetivos @stop

@section('estilos')
	@parent
	<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')
	<body class="">
		@parent
		@section('menu')
			@parent
		@stop
		@section('main')
			<main>
				<article class="ed-container full" id="objetivos">

					<div class="ed-item main-center">
						<h3>Objetivos</h3>
					</div>
					<div class="ed-item cross-center web-50">
						<img src="{{ asset('img/vinculacion/'.$management->management_area_image_objective) }}" alt="">
					</div>
					<div class="ed-item cross-center web-50">
					<h4></h4>
						<p>{!!$management->management_area_objective!!}</p>
					</div>

				</article>
			</main>
		@stop
		@section('footer')
			@parent
		@stop
		@section('scripts')
			@parent
			<script type="text/javascript">

				$('nav').addClass('sticky');

				$('a[href^="#"]').on('click', function(event) {
				    var target = $(this.getAttribute('href'));
				    if( target.length ) {
				        event.preventDefault();
				        $('html, body').stop().animate({
				            scrollTop: target.offset().top
				        }, 1000);
				    }
				});

			</script>
		@stop
	</body>
@stop
