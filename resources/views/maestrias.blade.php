@extends('layouts.plantilla')

@section('title') Maestrías @stop

@section('estilos')
@parent
<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')
<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="contactos">

			<div class="ed-item main-center">
				<h3>Maestrías</h3>
			</div>
				
		<div class="main__content">
			<table  class="content__table" id="coordintab">
				<thead class="table__head">
					<tr>
				    	<th>Descripción</th>
						<th>Tipo de Programa</th>
						<th>Categoría de Programa</th>
                        <th>Objetivo</th>
						<th>Versión</th>
						<th>Duración</th>
                        <th>Fecha Inicio</th>
						<th>Fecha Fin</th>
						<th>Coordinador</th>
                        <th>Programa Curricular</th>
                        <th>Estado</th>
					</tr>
				</thead>
				<tbody class="data__info">
					@foreach($program as $programs )
					<tr>
					<td>{{$programs->program_description}}</td>
					<td>
					    @foreach($type as $types)
							@if($types->type_program_id == $programs->program_type_program)
								{{$types->type_program_description}}
							@endif
						@endforeach
					</td>
                    <td>
                       @foreach($cat as $catpros)
							@if($catpros->category_program_id == $programs->program_curriculum_program)
								{{$catpros->category_program_description}}
							@endif
						@endforeach
					</td>
                    <td>{{$programs->program_objective}}</td>
					<td>{{$programs->program_version}}</td>
					<td>{{$programs->program_duration}}</td>
					<td>{{$programs->program_date_begin}}</td>
					<td>{{$programs->program_date_end}}</td>
                    <td>
					    @foreach($coordinator as $coordinators)
							@if($coordinators->coordinator_program_id == $programs->program_coordinator_program)
								{{$coordinators->coordinator_program_name}}
							@endif
						@endforeach
					</td>
                    <td>
                       @foreach($curricular as $curriculars)
							@if($curriculars->curriculum_program_id == $programs->program_curriculum_program)
								{{$curriculars->curriculum_program_description}}
							@endif
						@endforeach
						<br></br>
					</td>
					<td>{{$programs->program_state}}</td>
					 	</tr>
				   @endforeach
			</tbody>
		</table>
		</div>
		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">

		$('nav').addClass('sticky');


		// $('body').on({
		// 	'mousewheel': function(e) {
		// 		if (e.target.id == 'el') return;
		// 		e.preventDefault();
		// 		e.stopPropagation();
		// 	}
		// })

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});
	</script>
	@stop
</body>
@stop
