@extends('layouts.plantilla')


@section('title') Postula! @stop

@section('estilos')
@parent

@stop

@section('body')
<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="contactos">
        <div class="ed-item main-center">
			<h3>Postula</h3>
		</div>
        <div class="ed-item main-center">
        <div class="insert__form">
				<form method="POST" action="{{route('postula')}}" class="action__form" id="form__insert" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{csrf_token()}}">

            <div class="form__container">
				<div class="container__label">
					<label for="">Maestría/Master:</label>
				</div>
				<div class="container__item">
					<select  name="postulantProgram" id="postulantProgram" >
						<option value="" selected disabled>Selecciona una opción</option>
						@forelse($option2 as $program)
						<option value="{{$program->program_id}}">
                        @if($program->program_state == "Ofertado")
							{{$program->program_description}}
                        @endif
						</option>
						@empty
						<option value="">No existen programas de maestría</option>
						@endforelse
					</select>
				</div>
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Tipo de Documento/Document Type:</label>
				</div>
				<div class="container__item">
					<select  name="postulantDocumentNumber" id="postulantDocumentNumber" >
						<option value="" selected disabled>Selecciona una opción</option>
						<option value="PASAPORTE" >
                            PASAPORTE
						</option>
						<option value="CEDULA">
							CEDULA
						</option>
					</select>
				</div>
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Número de cédula o Pasaporte/Passport or ID</label>
				</div>
				<div class="container__item">
					<input type="text" name="postulantIdDocument" placeholder="Cédula sin guión" >
				</div>					
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Nombres/Name: </label>
				</div>
				<div class="container__item">
					<input type="text" name="postulantName" placeholder="Ingrese sus nombres " >
				</div>					
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Apellidos/Last Name: </label>
				</div>
				<div class="container__item">
					<input type="text" name="postulantLastName" placeholder="Ingrese sus apellidos" >
				</div>					
			</div>
			
            <div class="form__container">
				<div class="container__label">
					<label for="">Dirección/Address: </label>
				</div>
				<div class="container__item">
					<input type="text" name="postulantAddress" placeholder="Ingrese su sirección" >
				</div>					
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Teléfono/Telephone </label>
				</div>
				<div class="container__item">
					<input type="text" name="postulantPhone" placeholder="Ingrese su teléfono o celular " >
				</div>					
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Correo/E-Mail: </label>
				</div>
				<div class="container__item">
					<input type="text" name="postulantMail" placeholder="Ingrese su correo electrónico " >
				</div>					
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Nacionalidad/Nationality: </label>
				</div>
				<div class="container__item">
					<select  name="postulantNationality" id="postulantNationality" >
						<option value="" selected disabled>Selecciona una opción</option>
						<option value="ECUADOR" >
                        ECUADOR
						</option>
						<option value="COLOMBIA">
                        COLOMBIA
						</option>
                        <option value="PERÚ">
                        PERÚ
						</option>
                        <option value="ARGENTINA">
                        ARGENTINA
						</option>
                        <option value="BOLIVIA">
                        BOLIVIA
						</option>
                        <option value="BRASIL">
                        BRASIL
						</option>
                        <option value="CHILE">
                        CHILE
						</option>
                        <option value="PARAGUAY">
                        PARAGUAY
						</option>
                        <option value="URUGUAY">
                        URUGUAY
						</option>
                        <option value="VENEZUELA">
                        VENEZUELA
						</option>
                        <option value="MÉXICO">
                        MÉXICO
						</option>
                        <option value="CUBA">
                        CUBA
						</option>
                        <option value="ESPAÑA">
                        ESPAÑA
						</option>
                        <option value="CHINA">
                        CHINA
						</option>
					</select>
				</div>
			</div>

            <div class="form__container">
				<div class="container__label">
					<label for="">Genero/Gender: </label>
				</div>
				<div class="container__item">
					<select  name="postulantGenre" id="postulantGenre" >
						<option value="" selected disabled>Selecciona una opción</option>
						<option value="MASCULINO" >
                        MASCULINO
						</option>
						<option value="FEMENINO">
                        FEMENINO
						</option>
                        <option value="MALE">
                        MALE
						</option>
                        <option value="FEMALE">
                        FEMALE
						</option>
					</select>
				</div>
			</div>
			@if (count($errors) > 0)
					<div class="form__container">
						<ul id="mensaje">
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
			<div class="form__button">
				<div class="button__save">				
					<input type="submit"  value="POSTULAR">
				</div>
			</div>
        </div>
		</div>
		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">

		$('nav').addClass('sticky');


		// $('body').on({
		// 	'mousewheel': function(e) {
		// 		if (e.target.id == 'el') return;
		// 		e.preventDefault();
		// 		e.stopPropagation();
		// 	}
		// })

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});
	</script>
	@stop
</body>
@stop
