	@extends('layouts.plantilla')

@section('title') Noticia @stop

@section('estilos')
@parent
<meta property="og:url"           content="{{ URL('/noticia/'.$news->news_id)}}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{$news->news_title}}" />
<meta property="og:description"   content="{{substr(strip_tags($news->news_content),0,300)}}" />
<meta property="og:image" content="{{ asset('/img/noticia/'.$news->news_photo )}}"/>

<link rel="stylesheet" href="../css/inicio.css">
@stop

@section('body')
<body class="">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0&appId=191628568142851&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="noticia-facultad">
		<div class="ed-item main-center">
				<h3>NOTICIA</h3>
			</div>
			<div class="ed-item padding-2">
				<h3>{{$news->news_title}}</h3>
				@if($news->news_type == 2)
				<p>Documento: <a href="{{asset('docs/noticias/'.$news->news_name)}}" target="_blank">  {{$news->news_name}}</a> </p>
				@elseif($news->news_type == 3)
				<p>Enlace: <a href="{{$news->news_name}}" target="_blank">{{$news->news_name}}</a></p>
				@else
				<img src="{{asset('img/noticias/'.$news->news_photo)}}" >
				@endif
				
				<p>
					{!!$news->news_content!!}
				</p>
			</div>
			<div class="fb-share-button" data-href="https://www.facebook.com/gad.colta/{{$news->news_id}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
		</article>
	</main>

	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">

		$('nav').addClass('sticky');

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});
	</script>
	@stop
</body>
@stop
