@extends('layouts.plantilla')

@section('title') Inicio @stop

@section('estilos')
@parent

@stop

@section('body')
<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('header')
	<header >
		<div class="ed-container full" id="header">
			<div class="ed-item ed-container full main-center cross-center">
			<div class="ed-item slider">
					@forelse ($slider as $slider)
					<img class="mySlides" src="img/slider/{{$slider->slider_image}}" alt="{{$slider->slider_name}}">
					@empty
					<img class="mySlides" src="img/slider/ESPOCH_PORTADA.jpg" alt="Portada_Defecto">
					@endforelse
				</div>
				<div class="ed-item description" style="position:absolute">
					<h1>GAD Municipal</h1>
					<h2>Canton Colta</h2>
					<span>Visitas: {{Counter::showAndCount('inicio')}}</span>
	      	
				</div>
				<div class="ed-item flecha main-center">
					<a href="#autoridades" class="ancla"> <i class="fa fa-angle-down"></i></a>
				</div>
			</div>

		</div>
	</header>
	@stop
	@section('main')
	<main>
		<article class="ed-container full" id="autoridades">
			<div class="ed-item main-center cross-end">
				<a href="#header" class="ancla2"> <i class="fa fa-angle-up"></i></a>
			</div>
			<div class="ed-item main-center cross-center ">
				<h3>Directivos</h3>
				<br>
			</div>
			@forelse($authoritys as $authorityData)
			@if( $authorityData->user_id == 1 )
			<div class="ed-item main-center base-100  autoridad">
					<div class="ed-item base-100 main-center cross-center">
						<!-- <a href="{{asset('docs/authority/'.$authorityData->user_cv)}}" target="_blank"> -->
							<a class="desactivado" style="cursor: default; " href="">
							<!-- <span><i class="fa fa-download fa-4x" aria-hidden="true"></i></span> -->
							<img src="{{ asset('img/authority/'.$authorityData->user_photo) }}" alt="">
							</a>
						<!-- </a> -->
						<p>{{$authorityData->user_name}} {{$authorityData->user_last_name}}</p>
						<span> {{$authorityData->user_type_description}} </span>
					</div>
				</div>
			</div>
            @else
			<div class="ed-item ed-container main-center base-100 tablet-50 web-25 autoridad">
					<div class="ed-item main-center cross-center">
						<!-- <a href="{{asset('docs/authority/'.$authorityData->user_cv)}}" target="_blank"> -->
							<!-- <span><i class="fa fa-download fa-4x" aria-hidden="true"></i></span> -->
							<a class="desactivado" style="cursor: default; " href="">
							<img src="{{ asset('img/authority/'.$authorityData->user_photo) }}" alt="">
							</a>
						<!-- </a> -->
						<p>{{$authorityData->user_name}} {{$authorityData->user_last_name}}</p>
						<span> {{$authorityData->user_type_description}} </span>
					</div>
			</div>
			@endif
			@empty
			No existen directivos a visualizar
			@endforelse

		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">
	/*	$('body').on({
			'mousewheel': function(e) {
				if (e.target.id == 'el') return;
				e.preventDefault();
				e.stopPropagation();
			}
		})
		*/
		$('.desactivado').on('click',function(e){
			event.preventDefault();
		})


       $('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});


		$(function () {
			var $win = $(window);
			var $pos = 100;
			$win.scroll(function () {
				if ($win.scrollTop() <= $pos)
					$('nav').removeClass('sticky');
				else {
					$('nav').addClass('sticky');
				}
			});
		});

		var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 5000); // Change image every 2 seconds
}
	</script>
	@stop
</body>
@stop
