@extends('layouts.plantilla')

@section('title') GAD-Colta @stop

@section('estilos')
	@parent
	<link rel="stylesheet" href="css/inicio.css">
@stop

@section('body')
	<body class="">
		@parent
		@section('menu')
			@parent
		@stop
		@section('main')
			<main>
				<article class="ed-container full" id="funciones">
					<div class="ed-item main-center">
						<h3>Responsabilidades</h3>
					</div>
					<div class="ed-item cross-center web-50">
						{!!$management->management_area_functions!!}
					</div>
					<div class="ed-item cross-center web-50">
						<img height="100" src="https://www.eltelegrafo.com.ec/media/k2/items/cache/531e4b1e02fe0ca2067e5d711460b2b5_XL.jpg" alt="">
					</div>
				</article>
			</main>
		@stop
		@section('footer')
			@parent
		@stop
		@section('scripts')
			@parent
			<script type="text/javascript">

						$('nav').addClass('sticky');


				// $('body').on({
				// 	'mousewheel': function(e) {
				// 		if (e.target.id == 'el') return;
				// 			e.preventDefault();
				// 			e.stopPropagation();
				// 	}
				// })

				$('a[href^="#"]').on('click', function(event) {
				    var target = $(this.getAttribute('href'));
				    if( target.length ) {
				        event.preventDefault();
				        $('html, body').stop().animate({
				            scrollTop: target.offset().top
				        }, 1000);
				    }
				});

			</script>
		@stop
	</body>
@stop
