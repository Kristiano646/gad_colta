@extends('layouts.plantilla')

@section('title') Coordinadores @stop

@section('estilos')
@parent

@stop

@section('body')
<body class="">
	@parent
	@section('menu')
	@parent
	@stop
	@section('main')
	<main>
		<article class="ed-container main-center full" id="contactos">

			<div class="ed-item main-center">
				<h3>Coordinadores</h3>
			</div>
				
		<div class="main__content" >
			<table  class="content__table" id="coordintab" >
				<thead class="table__head">
					<tr>
						<th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Título</th>
                        <th>E-mail</th>
                        <th>Número de Contacto</th>
					</tr>
				</thead>
				<tbody class="data__info">
					@foreach($coordinator as $coordinators )
					<tr>
						<td>{{$coordinators->coordinator_program_name}}</td>
                    	<td>{{$coordinators->coordinator_program_last_name}}</td>
                   	 	<td>{{$coordinators->coordinator_program_title}}</td>
                    	<td>{{$coordinators->coordinator_program_mail}}</td>
                    	<td>{{$coordinators->coordinator_program_cellphone}}</td>
						</tr>
				   @endforeach
			</tbody>
		</table>
		</div>
		</article>
	</main>
	@stop
	@section('footer')
	@parent
	@stop
	@section('scripts')
	@parent
	<script type="text/javascript">

		$('nav').addClass('sticky');


		// $('body').on({
		// 	'mousewheel': function(e) {
		// 		if (e.target.id == 'el') return;
		// 		e.preventDefault();
		// 		e.stopPropagation();
		// 	}
		// })

		$('a[href^="#"]').on('click', function(event) {
			var target = $(this.getAttribute('href'));
			if( target.length ) {
				event.preventDefault();
				$('html, body').stop().animate({
					scrollTop: target.offset().top
				}, 1000);
			}
		});
	</script>
	@stop
</body>
@stop
